//
// Created by root on 8/12/21.
//

#include "PrimalPhase2PreProcess.h"
#include "../KernelRegistry.h"

int PrimalPhase2PreProcess::Run() {
  dgInfo_.phase2UpdatePrimal(true);
  return OP_PRIMAL_PHASE2_REBUILD;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE2_PREPROCESS, KernelCreator<PrimalPhase2PreProcess>)
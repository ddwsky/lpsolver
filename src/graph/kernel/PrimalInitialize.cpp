//
// Created by root on 8/12/21.
//

#include "PrimalInitialize.h"
#include "../KernelRegistry.h"

int PrimalInitialize::Run() {
  // info initialize
  dgInfo_.num_col = dgInfo_.ekk_instance_.lp_.numCol_;
  dgInfo_.num_row = dgInfo_.ekk_instance_.lp_.numRow_;
  dgInfo_.num_tot = dgInfo_.num_col + dgInfo_.num_row;

  // Setup local vectors
  dgInfo_.col_aq.setup(dgInfo_.num_row);
  dgInfo_.row_ep.setup(dgInfo_.num_row);
  dgInfo_.row_ap.setup(dgInfo_.num_col);
  dgInfo_.col_basic_feasibility_change.setup(dgInfo_.num_row);
  dgInfo_.row_basic_feasibility_change.setup(dgInfo_.num_col);

  dgInfo_.ph1SorterR.reserve(dgInfo_.num_row);
  dgInfo_.ph1SorterT.reserve(dgInfo_.num_row);

  dgInfo_.num_free_col = 0;
  for (HighsInt iCol = 0; iCol < dgInfo_.num_tot; iCol++) {
    if (dgInfo_.ekk_instance_.simplexinfo_.workLower_[iCol] == -kHighsInf &&
            dgInfo_.ekk_instance_.simplexinfo_.workUpper_[iCol] == kHighsInf) {
      // Free column
      dgInfo_.num_free_col++;
    }
  }
  // Set up the HSet instances, possibly using the internal error reporting and
  // debug option
  const bool debug = dgInfo_.ekk_instance_.options_.highs_debug_level > kHighsDebugLevelCheap;
  if (dgInfo_.num_free_col) {
    highsLogUser(dgInfo_.ekk_instance_.options_.log_options, HighsLogType::kInfo,
                 "DGPrimal:: LP has %" HIGHSINT_FORMAT " free columns\n",
                 dgInfo_.num_free_col);
    dgInfo_.nonbasic_free_col_set.setup(dgInfo_.num_free_col, dgInfo_.num_tot,
                                        dgInfo_.ekk_instance_.options_.output_flag,
                                        dgInfo_.ekk_instance_.options_.log_file_stream, debug);
  }
  // Copy values of simplex solver options to dual simplex options
  dgInfo_.primal_feasibility_tolerance = dgInfo_.ekk_instance_.options_.primal_feasibility_tolerance;
  dgInfo_.dual_feasibility_tolerance = dgInfo_.ekk_instance_.options_.dual_feasibility_tolerance;
  dgInfo_.objective_target = dgInfo_.ekk_instance_.options_.objective_target;

  dgInfo_.ekk_instance_.status_.has_primal_objective_value = false;
  dgInfo_.ekk_instance_.status_.has_dual_objective_value = false;
  dgInfo_.ekk_instance_.model_status_ = HighsModelStatus::kNotset;
  dgInfo_.ekk_instance_.solve_bailout_ = false;
  dgInfo_.ekk_instance_.called_return_from_solve_ = false;
  dgInfo_.ekk_instance_.exit_algorithm = SimplexAlgorithm::kPrimal;

  dgInfo_.rebuild_reason = kRebuildReasonNo;

  dgInfo_.resetDevex();

  // run initialize
  HighsOptions &options = dgInfo_.ekk_instance_.options_;
  HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  HighsSimplexStatus &status = dgInfo_.ekk_instance_.status_;

  // Get the nonabsic free column set
  dgInfo_.getNonbasicFreeColumnSet();

  // Determine whether the solution is near-optimal. Value 1 is
  // unimportant, as the sum of dual infeasiblilities for near-optimal
  // solutions is typically many orders of magnitude smaller than 1,
  // and the sum of dual infeasiblilities will be very much larger for
  // non-trivial LPs that are primal feasible for a logical or crash
  // basis.
  const bool near_optimal = info.num_primal_infeasibility == 0 && info.sum_dual_infeasibility < 1;
  // Perturb bounds according to whether the solution is near-optimnal
  const bool perturb_bounds = !near_optimal;
  if (!perturb_bounds)
    highsLogDev(options.log_options, HighsLogType::kDetailed, "Near-optimal, so don't use bound perturbation\n");
  if (perturb_bounds && info.primal_simplex_bound_perturbation_multiplier) {
    dgInfo_.ekk_instance_.initialiseBound(SimplexAlgorithm::kPrimal, kSolvePhaseUnknown, perturb_bounds);
    dgInfo_.ekk_instance_.initialiseNonbasicValueAndMove();
    dgInfo_.ekk_instance_.computePrimal();
    dgInfo_.ekk_instance_.computeSimplexPrimalInfeasible();
  }

  // Now to do some iterations!
  HighsInt num_primal_infeasibility = dgInfo_.ekk_instance_.simplexinfo_.num_primal_infeasibility;
  dgInfo_.solve_phase = num_primal_infeasibility > 0 ? kSolvePhase1 : kSolvePhase2;

  // The major solving loop
  dgInfo_.correctPrimal(true);
  std::cout << "solve_phase: " << dgInfo_.solve_phase << ", primal objective: " << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value << std::endl;

  return OP_PRIMAL_PHASE1_REBUILD;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_INITIALIZE, KernelCreator<PrimalInitialize>)
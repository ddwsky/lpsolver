//
// Created by root on 8/12/21.
//

#ifndef HIGHS_STATIC_GRAPH_H
#define HIGHS_STATIC_GRAPH_H

#include "DynamicGraph.h"
template <class T>
DynamicGraph1* DynamicGraphCreator(GraphType type){
  DynamicGraph1* graph = new T(type);
  return graph;
}

class StaticGraph {
public:
    explicit StaticGraph(LPOptions options, std::string modelFile) : options_(options),modelFile_(modelFile) {};

    ~StaticGraph();

    HighsStatus Solve(){Run();};

    void Initialise(){
      LoadModel();
      nodes_={GRAPH_PRESOLVE, GRAPH_CRASH, GRAPH_PRIMAL};
      for(auto iv : nodes_){
        auto creator = KernelRegistry::GetInstance()->GetKernel(iv);
        if (creator == nullptr) {
          break;
        }
        DynamicGraph1 * graph = creator(iv);
        subGraphs_.emplace_back(graph);
      }
    };

protected:
    void LoadModel();
    void Run() {
      for (auto iv : subGraphs_) {
        iv->solve();
      }
    }

protected:
    // References:
    std::vector<GraphType> nodes_;
    std::vector<DynamicGraph1*> subGraphs_;
    const LPOptions options_;
    const std::string modelFile_;
};

#endif  // HIGHS_STATIC_GRAPH_H

//
// Created by root on 8/9/21.
//

#include "PrimalPhase1Rebuild.h"
#include "../KernelRegistry.h"

int PrimalPhase1Rebuild::Run() {
  HighsSimplexInfo &info = dgInfo_.simplexinfo_;
  HighsSimplexStatus &status = dgInfo_.status_;
//  std::cout << "rebuild : solve_phase: " << dgInfo_.solve_phase << ", primal objective: " << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value
//            << "," << info.updated_primal_objective_value << "," << info.update_count << "," << info.num_primal_infeasibility
//            << "," << info.dual_objective_value << "," << info.num_dual_infeasibility << "," << info.updated_primal_objective_value << std::endl;

  // Record whether the update objective value should be tested. If
  // the objective value is known, then the updated objective value
  // should be correct - once the correction due to recomputing the
  // dual values has been applied.
  //
  // Note that computePrimalObjectiveValue sets
  // has_primal_objective_value
  //
  // Have to do this before INVERT, as this permutes the indices of
  // basic variables, and baseValue only corresponds to the new
  // ordering once computePrimal has been called
  const bool check_updated_objective_value = status.has_primal_objective_value;
  double previous_primal_objective_value;
  if (check_updated_objective_value) {
    previous_primal_objective_value = info.updated_primal_objective_value;
  }

  // Rebuild ekk_instance_.factor_ - only if we got updates
  HighsInt reason_for_rebuild = dgInfo_.rebuild_reason;
  dgInfo_.rebuild_reason = kRebuildReasonNo;
  // Possibly Rebuild factor
  bool reInvert = info.update_count > 0;
  if (reInvert) {
    // Get a nonsingular inverse if possible. One of three things
    // happens: Current basis is nonsingular; Current basis is
    // singular and last nonsingular basis is refactorized as
    // nonsingular - or found singular. Latter is code failure.
    if (!dgInfo_.ekk_instance_.getNonsingularInverse(dgInfo_.solve_phase)) {
      dgInfo_.solve_phase = kSolvePhaseError;
      return -1;
    }
  }

  dgInfo_.ekk_instance_.computePrimal();
  dgInfo_.getBasicPrimalInfeasibility();
  if (info.num_primal_infeasibility > 0) {
    // Primal infeasibilities so should be in phase 1
    dgInfo_.phase1ComputeDual();
  } else {
    // No primal infeasibilities so in phase 2. Reset costs if was
    // previously in phase 1
    dgInfo_.ekk_instance_.initialiseCost(SimplexAlgorithm::kPrimal, dgInfo_.solve_phase);
    dgInfo_.solve_phase = kSolvePhase2;
    dgInfo_.ekk_instance_.computeDual();
  }
  dgInfo_.ekk_instance_.computeSimplexDualInfeasible();
  dgInfo_.ekk_instance_.computePrimalObjectiveValue();
  if (check_updated_objective_value) {
    // Apply the objective value correction due to computing primal
    // values from scratch.
    const double primal_objective_value_correction = info.primal_objective_value - previous_primal_objective_value;
    info.updated_primal_objective_value += primal_objective_value_correction;
  }
  // Now that there's a new dual_objective_value, reset the updated
  // value
  info.updated_primal_objective_value = info.primal_objective_value;

  dgInfo_.reportRebuild(reason_for_rebuild);

  // Record the synthetic clock for INVERT, and zero it for UPDATE
  dgInfo_.build_synthetic_tick_ = dgInfo_.factor_.build_synthetic_tick;
  dgInfo_.total_synthetic_tick_ = 0;

  dgInfo_.num_flip_since_rebuild = 0;
  // Data are fresh from rebuild
  status.has_fresh_rebuild = true;
//  std::cout << "rebuild : solve_phase: " << dgInfo_.solve_phase << ", primal objective: " << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value << std::endl;

  return (dgInfo_.solve_phase == kSolvePhase2) ? OP_PRIMAL_PHASE2_REBUILD : OP_PRIMAL_PHASE1_PRICING;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_REBUILD, KernelCreator<PrimalPhase1Rebuild>)

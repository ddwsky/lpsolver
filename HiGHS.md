# HiGHS数据结构、函数分析

## 数据结构

### HighsSimplexInfo

```
struct HighsSimplexInfo {
  std::vector<double> workCost_;//目标系数,前col是原始变量对应的系数,后row个是对偶变量对应的系数
  std::vector<double> workDual_;//前col是reduce cost，后row个是对偶变量取负
  std::vector<double> workShift_;//
  std::vector<double> workLower_;//前col个是原始变量的下界,后row个是每行的约束下界
  std::vector<double> workUpper_;//前col个是原始变量的上界,后row个是每行的约束上界
  std::vector<double> workRange_;//
  std::vector<double> workValue_;//前col个是原始变量(非基变量)的值
  std::vector<double> workLowerShift_;
  std::vector<double> workUpperShift_;
  std::vector<double> baseLower_;//row个基变量的下界
  std::vector<double> baseUpper_;//row个基变量的上界
  std::vector<double> baseValue_;//row个基变量的值
  std::vector<double> numTotRandomValue_;
  std::vector<HighsInt> devex_index_;
  HighsInt phase1_backtracking_test_done = false;
  HighsInt phase2_backtracking_test_done = false;
  bool backtracking_ = false;
  bool valid_backtracking_basis_ = false;
  SimplexBasis backtracking_basis_;
  HighsInt backtracking_basis_costs_perturbed_;
  HighsInt backtracking_basis_bounds_perturbed_;
  std::vector<double> backtracking_basis_workShift_;
  std::vector<double> backtracking_basis_workLowerShift_;
  std::vector<double> backtracking_basis_workUpperShift_;
  std::vector<double> backtracking_basis_edge_weights_;
  HighsInt dual_ray_row_;
  HighsInt dual_ray_sign_;
  HighsInt primal_ray_col_;
  HighsInt primal_ray_sign_;
  HighsInt simplex_strategy;
  HighsInt dual_edge_weight_strategy;
  HighsInt price_strategy;
  double dual_simplex_cost_perturbation_multiplier;
  double primal_simplex_phase1_cost_perturbation_multiplier = 1;
  double primal_simplex_bound_perturbation_multiplier;
  double factor_pivot_threshold;
  HighsInt update_limit;

  // Simplex control parameters from HSA
  HighsInt control_iteration_count0;
  double col_aq_density;
  double row_ep_density;
  double row_ap_density;
  double row_DSE_density;
  double col_basic_feasibility_change_density;
  double row_basic_feasibility_change_density;
  double col_BFRT_density;
  double primal_col_density;
  double dual_col_density;
  // For control of switch from DSE to Devex in dual simplex
  bool allow_dual_steepest_edge_to_devex_switch;
  double dual_steepest_edge_weight_log_error_threshold;
  double costly_DSE_frequency;
  HighsInt num_costly_DSE_iteration;
  double average_log_low_DSE_weight_error;
  double average_log_high_DSE_weight_error;
  // Internal options - can't be changed externally
  bool run_quiet = false;
  bool store_squared_primal_infeasibility = false;
  // Options for reporting timing
  bool report_simplex_inner_clock = false;
  bool report_simplex_outer_clock = false;
  bool report_simplex_phases_clock = false;
  bool report_HFactor_clock = false;
  // Option for analysing the LP simplex iterations, INVERT time and rebuild time
  bool analyse_lp = false;
  bool analyse_iterations = false;
  bool analyse_invert_form = false;
  // Simplex runtime information
  bool allow_cost_perturbation = true;
  bool allow_bound_perturbation = true;
  bool costs_perturbed = false;
  bool bounds_perturbed = false;

  HighsInt num_primal_infeasibility = -1;
  double max_primal_infeasibility;
  double sum_primal_infeasibility;
  HighsInt num_dual_infeasibility = -1;
  double max_dual_infeasibility;
  double sum_dual_infeasibility;

  // Records of cumulative iteration counts - updated at the end of a phase
  HighsInt dual_phase1_iteration_count = 0;
  HighsInt dual_phase2_iteration_count = 0;
  HighsInt primal_phase1_iteration_count = 0;
  HighsInt primal_phase2_iteration_count = 0;
  HighsInt primal_bound_swap = 0;

  HighsInt min_threads = 1;
  HighsInt num_threads = 1;
  HighsInt max_threads = kHighsThreadLimit;

  // Info on PAMI iterations
  HighsInt multi_iteration = 0;

  // Number of UPDATE operations performed - should be zeroed when INVERT is performed
  HighsInt update_count;
  // Value of dual objective - only set when computed from scratch in dual rebuild()
  double dual_objective_value;
  // Value of primal objective - only set when computed from scratch in primal rebuild()
  double primal_objective_value;

  // Value of dual objective that is updated in dual simplex solver
  double updated_dual_objective_value;
  // Value of primal objective that is updated in primal simplex solver
  double updated_primal_objective_value;
  // Number of logical variables in the basis
  HighsInt num_basic_logicals;
};

```



### SimplexBasis

```
struct SimplexBasis {
  std::vector<HighsInt> basicIndex_;
  std::vector<int8_t> nonbasicFlag_;
  std::vector<int8_t> nonbasicMove_;//可移动方向,0:点或自由移动,1:可以往上移,-1:可以往下移
};
```



### HMatrix

```
 private:
  HighsInt numCol;
  HighsInt numRow;
  std::vector<HighsInt> Astart;
  std::vector<HighsInt> Aindex;
  std::vector<double> Avalue;

  std::vector<HighsInt> ARstart;//行起始元素在nz向量(按行排)的索引
  std::vector<HighsInt> AR_Nend;//非基子矩阵N每行的nz个数
  std::vector<HighsInt> ARindex;//col列索引
  std::vector<double> ARvalue;//矩阵nz点的值
```



## 函数

### InitializeForSolve

#### allocateWorkAndBaseArrays

为HighsSimplexInfo的work分配空间

#### initialiseForNewLp

设置HighsSimplexInfo：

dual_edge_weight_strategy

price_strategy

primal_simplex_bound_perturbation_multiplier/dual_simplex_cost_perturbation_multiplier

factor_pivot_threshold:

update_limit:

allow_dual_steepest_edge_to_devex_switch

col_aq_density/row_ep_density/row_ap_density/row_DSE_density/col_basic_feasibility_change_density/row_basic_feasibility_change_density/col_BFRT_density/primal_col_density/dual_col_density/

numTotRandomValue_:perturbation



#### initialiseSimplexLpBasisAndFactor

实现功能:

1、setBasis,增加松弛变量(row),设置基变量和非基变量标志，可移动方向标记

2、computeFactor:buildSimple,buildKernel,buildFinish

buildSimple: permute/Lstart/

buildKernel:

buildFinish:UpivotLookup,LpivotIndex,LpivotLookup



#### initialiseMatrix

拷贝模型的csc矩阵,并生成csr矩阵

#### initialiseCost

initialiseLpColCost：

```
  for (HighsInt iCol = 0; iCol < lp_.numCol_; iCol++) {
    simplexinfo_.workCost_[iCol] = (HighsInt) lp_.sense_ * lp_.colCost_[iCol];
    simplexinfo_.workShift_[iCol] = 0;
  }
```

initialiseLpRowCost

```
  for (HighsInt iCol = lp_.numCol_; iCol < lp_.numCol_ + lp_.numRow_; iCol++) {
    simplexinfo_.workCost_[iCol] = 0;
    simplexinfo_.workShift_[iCol] = 0;
  }
```



#### initialiseBound

initialiseLpColBound

```
  for (HighsInt iCol = 0; iCol < lp_.numCol_; iCol++) {
    simplexinfo_.workLower_[iCol] = lp_.colLower_[iCol];
    simplexinfo_.workUpper_[iCol] = lp_.colUpper_[iCol];
    simplexinfo_.workRange_[iCol] = simplexinfo_.workUpper_[iCol] - simplexinfo_.workLower_[iCol];
    simplexinfo_.workLowerShift_[iCol] = 0;
    simplexinfo_.workUpperShift_[iCol] = 0;
  }
```

initialiseLpRowBound

```
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++) {
    HighsInt iCol = lp_.numCol_ + iRow;
    simplexinfo_.workLower_[iCol] = -lp_.rowUpper_[iRow];
    simplexinfo_.workUpper_[iCol] = -lp_.rowLower_[iRow];
    simplexinfo_.workRange_[iCol] = simplexinfo_.workUpper_[iCol] - simplexinfo_.workLower_[iCol];
    simplexinfo_.workLowerShift_[iCol] = 0;
    simplexinfo_.workUpperShift_[iCol] = 0;
  }
```



#### computePrimal

step1:计算b=Nxn

step2:FTran,计算Bxb'=b

step3:求出的基变量取反，基变量范围用workLower_/Upper



#### computeDual

求对偶变量

step1:设置对偶的rhs

step2:BTran，计算Bty=cb

step3:计算ANTy,

step4:workDual_前col-=ANTy，得reduce cost；workDual_后row-=y

#### computeSimplexPrimalInfeasible

计算基变量和非基变量的不可行性



#### computeSimplexDualInfeasible

计算对偶非基变量的不可行性

#### computePrimalObjectiveValue

计算Ctx

#### computeDualObjectiveValue

计算Z=Ctx

### rebuild

step1:调用computePrimal计算基变量

step2:如果在phase2，如果有变量值超出bound的情况，对bound根据purturb值进行shift

step3:计算原始基变量的不可行性

step4:计算对偶变量（phase1和phase2的计算不同，区别是phase1原始不可行，对偶可行，对偶变量bound越界要correct？？？）

step5:计算对偶不可行性

step6:计算原始目标值



```
  if (solve_phase == kSolvePhase1) {
    use_hyper_chuzc = false;
  } else {
    use_hyper_chuzc = true;
  }
```

### chuzc ->chooseColumn

step1:在非基自由变量中选择reduce_cost/devex_weight最大的

step2:在其他非基自由变量选择reduce_cost/devex_weight最大的？？？

如果选不出col，可能已经最优了

### useVariableIn-> FTRAN

step1:取aq

step2：FTran求aq’

step3:计算pivot col的workDual_

该步骤可能出现基矩阵奇异

### phase1ChooseRow

算法挺复杂，？？？

### chooseRow

ratio test

### considerBoundSwap

### update

step1:如果flip，处理？？；否则对出基变量是fix的进行处理

step2:如果是phase1，执行step3-4；否则执行step5

step3:phase1UpdatePrimal更新reduce_cost，和原始不可行性

step4:basicFeasibilityChangeUpdateDual更新,Btran,price,

step5:phase2UpdatePrimal,更新基变量，可能会有原始不可行的结果

step6:considerInfeasibleValueIn,检查不可行，可能有原始不可行

step7::updateDual,更新workDual_

step8:updateDevex,更新pricing weight(devex)

step9:removeNonbasicFreeColumn,对入基变量是free(fix)的col处理

step10:updatePivots，入基出基变量交换

step11:updateFactor,forrest-tomlin-update

step12:updateMatrix,ARvalue,ARindex交换








//
// Created by root on 8/12/21.
//

#include "PrimalPhase1PostProcess.h"
#include "../KernelRegistry.h"

int PrimalPhase1PostProcess::Run() {
  HighsSimplexStatus &status = dgInfo_.ekk_instance_.status_;
  if (!dgInfo_.ekk_instance_.simplexinfo_.num_primal_infeasibility && dgInfo_.solve_phase == kSolvePhase1)
    dgInfo_.rebuild_reason = kRebuildReasonUpdateLimitReached;
  std::cout << "solve_phase: " << dgInfo_.solve_phase << ", primal objective: "
            << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value << ", " << dgInfo_.rebuild_reason << std::endl;
  if (!dgInfo_.rebuild_reason) {
    return OP_PRIMAL_PHASE1_PRICING;
  } else if (!(status.has_fresh_rebuild && dgInfo_.num_flip_since_rebuild == 0)) {
    return OP_PRIMAL_PHASE1_REBUILD;
  }
  if (dgInfo_.solve_phase == kSolvePhase1) {
    // Determine whether primal infeasiblility has been identified
    if (dgInfo_.variable_in < 0) {
      // Optimal in phase 1, so should have primal infeasiblilities
      assert(dgInfo_.simplexinfo_.num_primal_infeasibility > 0);
      if (dgInfo_.ekk_instance_.simplexinfo_.bounds_perturbed) {
        // Remove any bound perturbations and return to phase 1
        dgInfo_.cleanup();
      } else {
        dgInfo_.ekk_instance_.model_status_ = HighsModelStatus::kInfeasible;
        dgInfo_.solve_phase = kSolvePhaseExit;
      }
    }
    return OP_PRIMAL_PHASE1_REBUILD;
  }
  return OP_PRIMAL_PHASE2_PREPROCESS;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_POSTPROCESS, KernelCreator<PrimalPhase1PostProcess>)
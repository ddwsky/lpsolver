//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE1_UPDATE_H
#define HIGHS_PRIMAL_PHASE1_UPDATE_H

#include "../KernelBase.h"

class PrimalPhase1Update : public KernelBase {
public:
    PrimalPhase1Update(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase1Update() = default;

    int Run() override;
};


#endif //HIGHS_PRIMAL_PHASE1_UPDATE_H

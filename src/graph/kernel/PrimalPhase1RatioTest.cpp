//
// Created by root on 8/9/21.
//

#include "PrimalPhase1RatioTest.h"
#include "../KernelRegistry.h"

int PrimalPhase1RatioTest::Run() {
  RatioTest();
  return BoundFlip();
}

void PrimalPhase1RatioTest::RatioTest() {
  const HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  const vector<double> &baseLower = info.baseLower_;
  const vector<double> &baseUpper = info.baseUpper_;
  const vector<double> &baseValue = info.baseValue_;
  // Collect phase 1 theta lists
  //

  const double dPivotTol = info.update_count < 10 ? 1e-9 : info.update_count < 20 ? 1e-8 : 1e-7;
  dgInfo_.ph1SorterR.clear();
  dgInfo_.ph1SorterT.clear();
  for (HighsInt i = 0; i < dgInfo_.col_aq.count; i++) {
    HighsInt iRow = dgInfo_.col_aq.index[i];
    double dAlpha = dgInfo_.col_aq.array[iRow] * dgInfo_.move_in;

    // When the basic variable x[i] decrease
    if (dAlpha > +dPivotTol) {
      // Whether it can become feasible by going below its upper bound
      if (baseValue[iRow] > baseUpper[iRow] + dgInfo_.primal_feasibility_tolerance) {
        double dFeasTheta = (baseValue[iRow] - baseUpper[iRow] - dgInfo_.primal_feasibility_tolerance) / dAlpha;
        dgInfo_.ph1SorterR.push_back(std::make_pair(dFeasTheta, iRow));
        dgInfo_.ph1SorterT.push_back(std::make_pair(dFeasTheta, iRow));
      }
      // Whether it can become infeasible (again) by going below its
      // lower bound
      if (baseValue[iRow] > baseLower[iRow] - dgInfo_.primal_feasibility_tolerance &&
          baseLower[iRow] > -kHighsInf) {
        double dRelaxTheta = (baseValue[iRow] - baseLower[iRow] + dgInfo_.primal_feasibility_tolerance) / dAlpha;
        double dTightTheta = (baseValue[iRow] - baseLower[iRow]) / dAlpha;
        dgInfo_.ph1SorterR.push_back(std::make_pair(dRelaxTheta, iRow - dgInfo_.num_row));
        dgInfo_.ph1SorterT.push_back(std::make_pair(dTightTheta, iRow - dgInfo_.num_row));
      }
    }

    // When the basic variable x[i] increase
    if (dAlpha < -dPivotTol) {
      // Whether it can become feasible by going above its lower bound
      if (baseValue[iRow] < baseLower[iRow] - dgInfo_.primal_feasibility_tolerance) {
        double dFeasTheta = (baseValue[iRow] - baseLower[iRow] + dgInfo_.primal_feasibility_tolerance) / dAlpha;
        dgInfo_.ph1SorterR.push_back(std::make_pair(dFeasTheta, iRow - dgInfo_.num_row));
        dgInfo_.ph1SorterT.push_back(std::make_pair(dFeasTheta, iRow - dgInfo_.num_row));
      }
      // Whether it can become infeasible (again) by going above its
      // upper bound
      if (baseValue[iRow] < baseUpper[iRow] + dgInfo_.primal_feasibility_tolerance &&
          baseUpper[iRow] < +kHighsInf) {
        double dRelaxTheta = (baseValue[iRow] - baseUpper[iRow] - dgInfo_.primal_feasibility_tolerance) / dAlpha;
        double dTightTheta = (baseValue[iRow] - baseUpper[iRow]) / dAlpha;
        dgInfo_.ph1SorterR.push_back(std::make_pair(dRelaxTheta, iRow));
        dgInfo_.ph1SorterT.push_back(std::make_pair(dTightTheta, iRow));
      }
    }
  }

  // When there are no candidates at all, we can leave it here
  if (dgInfo_.ph1SorterR.empty()) {
    dgInfo_.row_out = -1;
    dgInfo_.variable_out = -1;
    return;
  }

  // Now sort the relaxed theta to find the final break point. TODO:
  // Consider partial sort. Or heapify [O(n)] and then pop k points
  // [kO(log(n))].

  std::sort(dgInfo_.ph1SorterR.begin(), dgInfo_.ph1SorterR.end());
  double dMaxTheta = dgInfo_.ph1SorterR.at(0).first;
  double dGradient = fabs(dgInfo_.theta_dual);
  for (HighsUInt i = 0; i < dgInfo_.ph1SorterR.size(); i++) {
    double dMyTheta = dgInfo_.ph1SorterR.at(i).first;
    HighsInt index = dgInfo_.ph1SorterR.at(i).second;
    HighsInt iRow = index >= 0 ? index : index + dgInfo_.num_row;
    dGradient -= fabs(dgInfo_.col_aq.array[iRow]);
    // Stop when the gradient start to decrease
    if (dGradient <= 0) {
      break;
    }
    dMaxTheta = dMyTheta;
  }

  // Find out the biggest possible alpha for pivot
  std::sort(dgInfo_.ph1SorterT.begin(), dgInfo_.ph1SorterT.end());
  double dMaxAlpha = 0.0;
  HighsUInt iLast = dgInfo_.ph1SorterT.size();
  for (HighsUInt i = 0; i < dgInfo_.ph1SorterT.size(); i++) {
    double dMyTheta = dgInfo_.ph1SorterT.at(i).first;
    HighsInt index = dgInfo_.ph1SorterT.at(i).second;
    HighsInt iRow = index >= 0 ? index : index + dgInfo_.num_row;
    double dAbsAlpha = fabs(dgInfo_.col_aq.array[iRow]);
    // Stop when the theta is too large
    if (dMyTheta > dMaxTheta) {
      iLast = i;
      break;
    }
    // Update the maximal possible alpha
    if (dMaxAlpha < dAbsAlpha) {
      dMaxAlpha = dAbsAlpha;
    }
  }

  // Finally choose a pivot with good enough alpha, working backwards
  dgInfo_.row_out = -1;
  dgInfo_.variable_out = -1;
  dgInfo_.move_out = 0;
  for (HighsInt i = iLast - 1; i >= 0; i--) {
    HighsInt index = dgInfo_.ph1SorterT.at(i).second;
    HighsInt iRow = index >= 0 ? index : index + dgInfo_.num_row;
    double dAbsAlpha = fabs(dgInfo_.col_aq.array[iRow]);
    if (dAbsAlpha > dMaxAlpha * 0.1) {
      dgInfo_.row_out = iRow;
      dgInfo_.move_out = index >= 0 ? 1 : -1;
      break;
    }
  }
//  std::cout << "ratio test : solve_phase: " << dgInfo_.solve_phase << ", primal objective: " << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value
//            << "," << info.updated_primal_objective_value << "," << info.update_count << "," << info.num_primal_infeasibility
//            << "," << info.dual_objective_value << "," << info.num_dual_infeasibility << "," << info.updated_primal_objective_value
//            << "," << dgInfo_.value_in << "," << dgInfo_.row_out << "," << dgInfo_.theta_primal << "," << dgInfo_.alpha_col
//            << "," << dgInfo_.alpha_row << "," << dgInfo_.move_in << "," << dgInfo_.move_out << "," << dgInfo_.theta_dual
//            << "," << dgInfo_.variable_in << "," << dgInfo_.variable_out << std::endl;

}

int PrimalPhase1RatioTest::BoundFlip() {
  const HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  const vector<double> &baseLower = info.baseLower_;
  const vector<double> &baseUpper = info.baseUpper_;
  const vector<double> &baseValue = info.baseValue_;
////////////////////////////BF
  const vector<double> &workLower = info.workLower_;
  const vector<double> &workUpper = info.workUpper_;
  const vector<double> &workValue = info.workValue_;

  // Compute the primal theta and see if we should have done a bound
  // flip instead
  // Determine the step to the leaving bound
  //
  dgInfo_.alpha_col = dgInfo_.col_aq.array[dgInfo_.row_out];
  // In Phase 1, move_out depends on whether the leaving variable is
  // becoming feasible - moves up to lower (down to upper) - or
  // remaining feasible - moves down to lower (up to upper) - so
  // can't be set so easily as in phase 2
  dgInfo_.theta_primal = 0;
  if (dgInfo_.move_out == 1) {
    dgInfo_.theta_primal = (baseValue[dgInfo_.row_out] - baseUpper[dgInfo_.row_out]) / dgInfo_.alpha_col;
  } else {
    dgInfo_.theta_primal = (baseValue[dgInfo_.row_out] - baseLower[dgInfo_.row_out]) / dgInfo_.alpha_col;
  }
  assert(dgInfo_.theta_primal > -kHighsInf && dgInfo_.theta_primal < kHighsInf);

  // Look to see if there is a bound flip
  bool flipped = false;
  double lower_in = workLower[dgInfo_.variable_in];
  double upper_in = workUpper[dgInfo_.variable_in];
  dgInfo_.value_in = workValue[dgInfo_.variable_in] + dgInfo_.theta_primal;
  if (dgInfo_.move_in > 0) {
    if (dgInfo_.value_in > upper_in + dgInfo_.primal_feasibility_tolerance) {
      flipped = true;
      dgInfo_.row_out = -1;
      dgInfo_.value_in = upper_in;
      dgInfo_.theta_primal = upper_in - lower_in;
    }
  } else {
    if (dgInfo_.value_in < lower_in - dgInfo_.primal_feasibility_tolerance) {
      flipped = true;
      dgInfo_.row_out = -1;
      dgInfo_.value_in = lower_in;
      dgInfo_.theta_primal = lower_in - upper_in;
    }
  }
  const bool pivot_or_flipped = dgInfo_.row_out >= 0 || flipped;
//  std::cout << "bound flip : solve_phase: " << dgInfo_.solve_phase << ", primal objective: " << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value
//            << "," << info.updated_primal_objective_value << "," << info.update_count << "," << info.num_primal_infeasibility
//            << "," << info.dual_objective_value << "," << info.num_dual_infeasibility << "," << info.updated_primal_objective_value
//            << "," << dgInfo_.value_in << "," << dgInfo_.row_out << "," << dgInfo_.theta_primal << "," << dgInfo_.alpha_col
//            << "," << dgInfo_.alpha_row << "," << dgInfo_.move_in << "," << dgInfo_.move_out << "," << dgInfo_.theta_dual
//            << "," << dgInfo_.variable_in << "," << dgInfo_.variable_out << std::endl;
  // Check for possible error
  assert(pivot_or_flipped);
  assert(flipped == (dgInfo_.row_out == -1));
  return OP_PRIMAL_PHASE1_BTRAN;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_RATIO_TEST, KernelCreator<PrimalPhase1RatioTest>)
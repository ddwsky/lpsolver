//
// Created by root on 8/9/21.
//

#include "PrimalPhase1Pricing.h"
#include "../KernelRegistry.h"
#include <iomanip>
using namespace std;
int PrimalPhase1Pricing::Run() {
  const vector<int8_t> &nonbasicMove = dgInfo_.simplexbasis_.nonbasicMove_;
  const vector<double> &workDual = dgInfo_.simplexinfo_.workDual_;
  double best_measure = 0;
  dgInfo_.variable_in = -1;
  // Consider nonbasic free columns first
  const HighsInt &num_nonbasic_free_col = dgInfo_.nonbasic_free_col_set.count();

  // Choose any attractive nonbasic free column
  if (num_nonbasic_free_col) {
    const vector<HighsInt> &nonbasic_free_col_set_entry = dgInfo_.nonbasic_free_col_set.entry();
    for (HighsInt ix = 0; ix < num_nonbasic_free_col; ix++) {
      HighsInt iCol = nonbasic_free_col_set_entry[ix];
      double dual_infeasibility = fabs(workDual[iCol]);
      if (dual_infeasibility > dgInfo_.dual_feasibility_tolerance &&
          dual_infeasibility > best_measure * dgInfo_.devex_weight[iCol]) {
        dgInfo_.variable_in = iCol;
        best_measure = dual_infeasibility / dgInfo_.devex_weight[iCol];
//        std::cout << "variable_in0:" << num_nonbasic_free_col << ", " << iCol << ", " << best_measure << std::endl;
      }
    }
  }
  // Now look at other columns
//  std::cout << ", :" << dgInfo_.num_tot << ", " << best_measure << ", " << dgInfo_.iteration_count_ << std::endl;
//  string a;
  for (HighsInt iCol = 0; iCol < dgInfo_.num_tot; iCol++) {
    double dual_infeasibility = -nonbasicMove[iCol] * workDual[iCol];
    double b=best_measure * dgInfo_.devex_weight[iCol];
    bool flag = dual_infeasibility > dgInfo_.dual_feasibility_tolerance;
    bool flag1 = dual_infeasibility > b && fabs(dual_infeasibility-b) > dgInfo_.dual_feasibility_tolerance;//best_measure * dgInfo_.devex_weight[iCol];
//    if (dgInfo_.iteration_count_ == 459)
//      std::cout << setiosflags(std::ios::fixed)<<setprecision(9) << iCol << "," << dual_infeasibility << "," << dgInfo_.dual_feasibility_tolerance << ","
//                << dual_infeasibility << "," << best_measure << "," << dgInfo_.devex_weight[iCol] << ", " << flag << ", " << flag1 << ", " << b
//              << std::endl;
    if (flag && flag1) {
      dgInfo_.variable_in = iCol;
//      std::cout << "variable_in1:" << iCol << ", " << best_measure << ", " << workDual[iCol] << ", "
//                << dual_infeasibility << ", " << dgInfo_.devex_weight[iCol];
      best_measure = dual_infeasibility / dgInfo_.devex_weight[iCol];
//      std::cout << ", :" << iCol << ", " << best_measure << std::endl;
    }
  }
//  if(dgInfo_.iteration_count_ == 459)
//  std::cout << "variable_in1: a=" << a << std::endl;
//  printf("ChooseColumn: Iteration %" HIGHSINT_FORMAT ", choose column %"
//         HIGHSINT_FORMAT " with measure %g\n",
//         dgInfo_.ekk_instance_.iteration_count_, dgInfo_.variable_in, best_measure);

  HighsSimplexStatus &status = dgInfo_.ekk_instance_.status_;
  if (dgInfo_.variable_in == -1) {
    dgInfo_.rebuild_reason = kRebuildReasonPossiblyOptimal;
    if ((status.has_fresh_rebuild && dgInfo_.num_flip_since_rebuild == 0)) {
      return OP_PRIMAL_PHASE1_POSTPROCESS;
    }
    return OP_PRIMAL_PHASE1_REBUILD;
  } else {
    return OP_PRIMAL_PHASE1_FTRAN;
  }
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_PRICING, KernelCreator<PrimalPhase1Pricing>)
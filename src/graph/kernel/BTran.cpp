//
// Created by root on 8/9/21.
//

#include "BTran.h"
#include "../KernelRegistry.h"

int BTran::Run() {
  if (dgInfo_.row_out < 0)
    return dgInfo_.solve_phase == kSolvePhase1 ? OP_PRIMAL_PHASE1_UPDATE : OP_PRIMAL_PHASE2_UPDATE;
  // Record the pivot entry
  dgInfo_.alpha_col = dgInfo_.col_aq.array[dgInfo_.row_out];
  dgInfo_.variable_out = dgInfo_.ekk_instance_.simplexbasis_.basicIndex_[dgInfo_.row_out];

  // Compute the tableau row
  //
  // BTRAN
  //
  // Compute unit BTran for tableau row and FT update
  dgInfo_.ekk_instance_.unitBtran(dgInfo_.row_out, dgInfo_.row_ep);
  //
  // PRICE
  //
  dgInfo_.ekk_instance_.tableauRowPrice(dgInfo_.row_ep, dgInfo_.row_ap);

  // Checks row-wise pivot against column-wise pivot for
  // numerical trouble
  //
  // rebuild_reason = kRebuildReasonPossiblySingularBasis is set if
  // numerical trouble is detected
  dgInfo_.updateVerify();
  if (dgInfo_.rebuild_reason)
    return dgInfo_.solve_phase == kSolvePhase1 ? OP_PRIMAL_PHASE1_REBUILD : OP_PRIMAL_PHASE2_REBUILD;
  return dgInfo_.solve_phase == kSolvePhase1 ? OP_PRIMAL_PHASE1_UPDATE : OP_PRIMAL_PHASE2_UPDATE;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_BTRAN, KernelCreator<BTran>)
REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE2_BTRAN, KernelCreator<BTran>)

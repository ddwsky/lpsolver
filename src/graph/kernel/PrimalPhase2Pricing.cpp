//
// Created by root on 8/9/21.
//

#include "PrimalPhase2Pricing.h"
#include "../KernelRegistry.h"

int PrimalPhase2Pricing::Run() {
  std::cout << "bt num trub:" << dgInfo_.alpha_col << ", " << dgInfo_.alpha_row << ", " << dgInfo_.variable_in
            << std::endl;
  const vector<int8_t> &nonbasicMove = dgInfo_.simplexbasis_.nonbasicMove_;
  const vector<double> &workDual = dgInfo_.simplexinfo_.workDual_;
  double best_measure = 0;
  dgInfo_.variable_in = -1;
  // Consider nonbasic free columns first
  const HighsInt &num_nonbasic_free_col = dgInfo_.nonbasic_free_col_set.count();

  // Choose any attractive nonbasic free column
  if (num_nonbasic_free_col) {
    const vector<HighsInt> &nonbasic_free_col_set_entry = dgInfo_.nonbasic_free_col_set.entry();
    for (HighsInt ix = 0; ix < num_nonbasic_free_col; ix++) {
      HighsInt iCol = nonbasic_free_col_set_entry[ix];
      double dual_infeasibility = fabs(workDual[iCol]);
      if (dual_infeasibility > dgInfo_.dual_feasibility_tolerance &&
          dual_infeasibility > best_measure * dgInfo_.devex_weight[iCol]) {
        dgInfo_.variable_in = iCol;
        best_measure = dual_infeasibility / dgInfo_.devex_weight[iCol];
      }
    }
  }
  // Now look at other columns
  for (HighsInt iCol = 0; iCol < dgInfo_.num_tot; iCol++) {
    double dual_infeasibility = -nonbasicMove[iCol] * workDual[iCol];
    double b=best_measure * dgInfo_.devex_weight[iCol];
    bool flag = dual_infeasibility > dgInfo_.dual_feasibility_tolerance;
    bool flag1 = dual_infeasibility > b && fabs(dual_infeasibility-b) > dgInfo_.dual_feasibility_tolerance;//best_measure * dgInfo_.devex_weight[iCol];
    if (flag && flag1) {
      dgInfo_.variable_in = iCol;
      best_measure = dual_infeasibility / dgInfo_.devex_weight[iCol];
    }
  }
//  printf("ChooseColumn: Iteration %" HIGHSINT_FORMAT ", choose column %"
//         HIGHSINT_FORMAT " with measure %g\n",
//         dgInfo_.ekk_instance_.iteration_count_, dgInfo_.variable_in, best_measure);
  std::cout << "bt num trub:" << dgInfo_.alpha_col << ", " << dgInfo_.alpha_row << ", " << dgInfo_.variable_in
            << std::endl;
  HighsSimplexStatus &status = dgInfo_.ekk_instance_.status_;
  if (dgInfo_.variable_in == -1) {
    dgInfo_.rebuild_reason = kRebuildReasonPossiblyOptimal;
    if ((status.has_fresh_rebuild && dgInfo_.num_flip_since_rebuild == 0)) {
      return OP_PRIMAL_PHASE2_POSTPROCESS;
    }
    return OP_PRIMAL_PHASE2_REBUILD;
  } else {
    return OP_PRIMAL_PHASE2_FTRAN;
  }
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE2_PRICING, KernelCreator<PrimalPhase2Pricing>)
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                       */
/*    This file is part of the HiGHS linear optimization suite           */
/*                                                                       */
/*    Written and engineered 2008-2021 at the University of Edinburgh    */
/*                                                                       */
/*    Available as open-source under the MIT License                     */
/*                                                                       */
/*    Authors: Julian Hall, Ivet Galabova, Qi Huangfu, Leona Gottwald    */
/*    and Michael Feldmeier                                              */
/*                                                                       */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**@file Highs.h
 * @brief The HiGHS class
 */
#ifndef _XX_PRIMAL_H_
#define _XX_PRIMAL_H_

#include <sstream>

#include "lp_data/HighsLpUtils.h"
#include "lp_data/HighsRanging.h"
#include "lp_data/HighsInfo.h"
//#include "lp_data/HighsSolutionDebug.h"
#include "model/HighsModel.h"

/**
 * @brief Class to set parameters and run HiGHS
 */
class Primal {
public:
    Primal(HighsOptions &options, HighsTimer &timer);

    virtual ~Primal() {
      FILE *log_file_stream = options_.log_options.log_file_stream;
      if (log_file_stream != NULL) {
        assert(log_file_stream != stdout);
        fclose(log_file_stream);
      }
    }

    /**
     * @brief Resets options and then calls clearModel()
     */
    HighsStatus clear();

    /**
     * @brief Clears model and then calls clearSolver()
     */
    HighsStatus clearModel();

    /**
     * @brief Clears solver, and creates a HiGHS model object for the LP
     * in HiGHS
     */
    HighsStatus clearSolver();

    /**
     * Methods for model input
     */

    /**
     * @brief Every model loading module eventually uses
     * passModel(HighsModel model) to communicate the model to HiGHS. It clears
     * the vector of HighsModelObjects (hmos), creates a HighsModelObject for the
     * LP and makes it the first of the vector of HighsModelObjects
     */
    HighsStatus passModel(
            HighsModel model  //!< The HighsModel instance for this model
    );

    /**
     * @brief reads in a model and initializes the HighsModelObject
     */
    HighsStatus readModel(const std::string filename  //!< the filename
    );


    HighsStatus run();

    HighsStatus passOptions(const HighsOptions &options  //!< The options
    );

    const HighsInfo &getInfo() const { return info_; }

    HighsStatus getInfoValue(const std::string &info,  //!< The info name
                             double &value) const;     //!< The info value

    const HighsModel &getModel() const { return model_; }

    /**
     * @brief Returns the current model status
     */
    const HighsModelStatus &getModelStatus(
            const bool scaled_model = false) const {
      return scaled_model ? scaled_model_status_ : model_status_;
    }


    /**
     * @brief Gets the run time of HiGHS
     */
    double getRunTime() { return timer_.readRunHighsClock(); }


    std::string modelStatusToString(const HighsModelStatus model_status) const;

    std::string solutionStatusToString(const HighsInt solution_status) const;

//    std::string basisStatusToString(const HighsBasisStatus basis_status) const;


    HighsStatus returnFromSolve(const HighsStatus return_status);

    HighsInt initialiseSimplexLpBasisAndFactor(const bool only_from_known_basis = false);

    void updateSimplexOptions();

    void initialiseMatrix();

    void allocateWorkAndBaseArrays();

    void initialiseCost(const SimplexAlgorithm algorithm,
                        const HighsInt solve_phase, const bool perturb = false);

    void initialiseBound(const SimplexAlgorithm algorithm,
                         const HighsInt solve_phase, const bool perturb = false);

    void initialiseLpColCost();

    void initialiseLpRowCost();

    void initialiseNonbasicValueAndMove();

    void initialiseLpColBound();

    void initialiseLpRowBound();

    void computePrimal();

    void computeDual();

    void computeSimplexPrimalInfeasible();

    void computeSimplexDualInfeasible();

    void computePrimalObjectiveValue();

    void computeDualObjectiveValue(const HighsInt phase = 2);

    HighsStatus setBasis();

    HighsInt computeFactor();

    void setNonbasicMove();

    void fullBtran(HVector &buffer);

    void fullPrice(const HVector &full_col, HVector &full_row);

    bool isUnconstrainedLp();

    bool bailoutOnTimeIterations();

    void pivotColumnFtran(const HighsInt iCol, HVector &col_aq);

    double computeDualForTableauColumn(const HighsInt iVar, const HVector &tableau_column);

    void unitBtran(const HighsInt iRow, HVector &row_ep);

    void tableauRowPrice(const HVector &row_ep, HVector &row_ap);

    void invalidateDualMaxSumInfeasibilityRecord();

    void invalidateDualInfeasibilityRecord();

    void invalidatePrimalMaxSumInfeasibilityRecord();

    void invalidatePrimalInfeasibilityRecord();

    bool sparseLoopStyle(const HighsInt count, const HighsInt dim, HighsInt &to_entry);

    void updateFactor(HVector *column, HVector *row_ep, HighsInt *iRow, HighsInt *hint);

    void updatePivots(const HighsInt variable_in, const HighsInt row_out, const HighsInt move_out);

    void updateMatrix(const HighsInt variable_in, const HighsInt variable_out);

    void choosePriceTechnique(const HighsInt price_strategy, const double row_ep_density, bool &use_col_price,
                              bool &use_row_price_w_switch);

    void
    putBacktrackingBasis(const vector<HighsInt> &basicIndex_before_compute_factor, double *scattered_edge_weights);

    void putBacktrackingBasis();

    bool getNonsingularInverse(const HighsInt solve_phase);

    bool getBacktrackingBasis(double *scattered_edge_weights);

    HighsSolution getSolution();

    HighsBasis getHighsBasis();

    void initialiseForNewLp();

    void setSimplexOptions();

    void initialiseControl();

    void initialiseSimplexLpRandomVectors();

private:

    void forceHighsSolutionBasisSize();

    //
    // Sets unscaled and scaled model status, basis, solution and info
    // from the highs_model_object
    void setHighsModelStatusBasisSolutionAndInfo();


    //
    // Methods to clear solver data for users in Highs class members
    // before (possibly) updating them with data from trying to solve
    // the inumcumbent model.
    //
    // Clears all solver data in Highs class members by calling
    // clearModelStatus(), clearSolution(), clearBasis() and
    // clearInfo().
    void clearUserSolverData();

    //
    // Sets unscaled and scaled model status to HighsModelStatus::kNotset
    void clearModelStatus();

    //
    // Sets primal and dual solution status to
    // kSolutionStatusNone, and clears solution_ vectors
    void clearSolution();

    //
    // Invalidates basis and clears basis_ vectors
    void clearBasis();

    //
    // Invalidates info_ and resets the values of its members
    void clearInfo();

    HighsStatus returnFromRun(const HighsStatus return_status);

    HighsStatus returnFromHighs(const HighsStatus return_status);

    HighsStatus Solve();

    HighsStatus SolveInternal();

    HighsStatus initialiseForSolve();

public:
    HighsOptions options_;
    HighsBasis basis_;
    HighsModelStatus model_status_ = HighsModelStatus::kNotset;
    HighsSimplexAnalysis analysis_;
    double cost_scale_ = 1;
    HighsInt iteration_count_ = 0;

    bool solve_bailout_;
    bool called_return_from_solve_;
    SimplexAlgorithm exit_algorithm;
    HighsInt return_primal_solution_status;
    HighsInt return_dual_solution_status;

    HighsLp &lp_;
    std::string lp_name_;
    HighsSimplexStatus status_;
    HighsSimplexInfo simplexinfo_;
    SimplexBasis simplexbasis_;
    HighsRandom random_;

    double *workEdWt_ = NULL;      //!< DSE or Dvx weight
    double *workEdWtFull_ = NULL;  //!< Full-length std::vector where weights

    HMatrix matrix_;
    HFactor factor_;

    double build_synthetic_tick_;
    double total_synthetic_tick_;

//    ModelObject ekk_instance_;
    HighsSolutionParams solution_params_;
    HighsScale scale_;
private:
    HighsSolution solution_;
    HighsModel model_;
    HighsTimer timer_;

    HighsIterationCounts iteration_counts_;
    HighsInfo info_;

    HighsModelStatus scaled_model_status_ = HighsModelStatus::kNotset;
    HighsModelStatus unscaled_model_status_ = HighsModelStatus::kNotset;

    // This is strictly for debugging. It's used to check whether
    // returnFromRun() was called after the previous call to
    // Highs::run() and, assuming that this is always done, it checks
    // whether Highs::run() is called recursively.
    bool called_return_from_run = true;


};

#endif

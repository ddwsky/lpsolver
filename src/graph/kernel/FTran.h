//
// Created by root on 8/9/21.
//

#ifndef HIGHS_FTRAN_H
#define HIGHS_FTRAN_H

#include "../KernelBase.h"

class FTran : public KernelBase {
public:
    FTran(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~FTran() {};

    int Run() override;

};


#endif //HIGHS_FTRAN_H

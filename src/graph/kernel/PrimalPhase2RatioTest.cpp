//
// Created by root on 8/9/21.
//

#include "PrimalPhase2RatioTest.h"
#include "../KernelRegistry.h"

int PrimalPhase2RatioTest::Run() {
  std::cout << "bt num trub:" << dgInfo_.alpha_col << ", " << dgInfo_.alpha_row << ", " << dgInfo_.variable_in
            << std::endl;
  RatioTest();
  int ret = BoundFlip();
  std::cout << "bt num trub:" << dgInfo_.alpha_col << ", " << dgInfo_.alpha_row << ", " << dgInfo_.variable_in
            << std::endl;
  return ret;
}

void PrimalPhase2RatioTest::RatioTest() {
  HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  const vector<double> &baseLower = info.baseLower_;
  const vector<double> &baseUpper = info.baseUpper_;
  const vector<double> &baseValue = info.baseValue_;
  // Initialize
  dgInfo_.row_out = -1;

  // Choose row pass 1
  double alphaTol = info.update_count < 10 ? 1e-9 : info.update_count < 20 ? 1e-8 : 1e-7;

  double relaxTheta = 1e100;
  double relaxSpace;
  for (HighsInt i = 0; i < dgInfo_.col_aq.count; i++) {
    HighsInt iRow = dgInfo_.col_aq.index[i];
    double alpha = dgInfo_.col_aq.array[iRow] * dgInfo_.move_in;
    if (alpha > alphaTol) {
      relaxSpace = baseValue[iRow] - baseLower[iRow] + dgInfo_.primal_feasibility_tolerance;
      if (relaxSpace < relaxTheta * alpha) relaxTheta = relaxSpace / alpha;
    } else if (alpha < -alphaTol) {
      relaxSpace = baseValue[iRow] - baseUpper[iRow] - dgInfo_.primal_feasibility_tolerance;
      if (relaxSpace > relaxTheta * alpha) relaxTheta = relaxSpace / alpha;
    }
  }
  double bestAlpha = 0;
  for (HighsInt i = 0; i < dgInfo_.col_aq.count; i++) {
    HighsInt iRow = dgInfo_.col_aq.index[i];
    double alpha = dgInfo_.col_aq.array[iRow] * dgInfo_.move_in;
    if (alpha > alphaTol) {
      // Positive pivotal column entry
      double tightSpace = baseValue[iRow] - baseLower[iRow];
      if (tightSpace < relaxTheta * alpha) {
        if (bestAlpha < alpha) {
          bestAlpha = alpha;
          dgInfo_.row_out = iRow;
        }
      }
    } else if (alpha < -alphaTol) {
      // Negative pivotal column entry
      double tightSpace = baseValue[iRow] - baseUpper[iRow];
      if (tightSpace > relaxTheta * alpha) {
        if (bestAlpha < -alpha) {
          bestAlpha = -alpha;
          dgInfo_.row_out = iRow;
        }
      }
    }
  }
}

int PrimalPhase2RatioTest::BoundFlip() {
  const HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  const vector<double> &workLower = info.workLower_;
  const vector<double> &workUpper = info.workUpper_;
  const vector<double> &baseLower = info.baseLower_;
  const vector<double> &baseUpper = info.baseUpper_;
  const vector<double> &workValue = info.workValue_;
  const vector<double> &baseValue = info.baseValue_;

  // Compute the primal theta and see if we should have done a bound
  // flip instead
  if (dgInfo_.row_out < 0) {
    assert(dgInfo_.solve_phase == kSolvePhase2);
    // No binding ratio in CHUZR, so flip or unbounded
    dgInfo_.theta_primal = dgInfo_.move_in * kHighsInf;
    dgInfo_.move_out = 0;
  } else {
    // Determine the step to the leaving bound
    //
    dgInfo_.alpha_col = dgInfo_.col_aq.array[dgInfo_.row_out];
    // In Phase 1, move_out depends on whether the leaving variable is
    // becoming feasible - moves up to lower (down to upper) - or
    // remaining feasible - moves down to lower (up to upper) - so
    // can't be set so easily as in phase 2
    if (dgInfo_.solve_phase == kSolvePhase2)
      dgInfo_.move_out = dgInfo_.alpha_col * dgInfo_.move_in > 0 ? -1 : 1;
    dgInfo_.theta_primal = 0;
    if (dgInfo_.move_out == 1) {
      dgInfo_.theta_primal = (baseValue[dgInfo_.row_out] - baseUpper[dgInfo_.row_out]) / dgInfo_.alpha_col;
    } else {
      dgInfo_.theta_primal = (baseValue[dgInfo_.row_out] - baseLower[dgInfo_.row_out]) / dgInfo_.alpha_col;
    }
    assert(dgInfo_.theta_primal > -kHighsInf && dgInfo_.theta_primal < kHighsInf);
  }

  // Look to see if there is a bound flip
  bool flipped = false;
  double lower_in = workLower[dgInfo_.variable_in];
  double upper_in = workUpper[dgInfo_.variable_in];
  dgInfo_.value_in = workValue[dgInfo_.variable_in] + dgInfo_.theta_primal;
  if (dgInfo_.move_in > 0) {
    if (dgInfo_.value_in > upper_in + dgInfo_.primal_feasibility_tolerance) {
      flipped = true;
      dgInfo_.row_out = -1;
      dgInfo_.value_in = upper_in;
      dgInfo_.theta_primal = upper_in - lower_in;
    }
  } else {
    if (dgInfo_.value_in < lower_in - dgInfo_.primal_feasibility_tolerance) {
      flipped = true;
      dgInfo_.row_out = -1;
      dgInfo_.value_in = lower_in;
      dgInfo_.theta_primal = lower_in - upper_in;
    }
  }
  const bool pivot_or_flipped = dgInfo_.row_out >= 0 || flipped;
  // Check for possible unboundedness
  if (!pivot_or_flipped) {
    dgInfo_.rebuild_reason = kRebuildReasonPossiblyPrimalUnbounded;
    return OP_PRIMAL_PHASE2_REBUILD;
  }
  // Check for possible error
  assert(pivot_or_flipped);
  assert(flipped == (dgInfo_.row_out == -1));
  return OP_PRIMAL_PHASE2_BTRAN;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE2_RATIO_TEST, KernelCreator<PrimalPhase2RatioTest>)
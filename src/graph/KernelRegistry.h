//
// Created by root on 8/8/21.
//

#ifndef HIGHS_KERNEL_REGISTRY_H
#define HIGHS_KERNEL_REGISTRY_H

#include <mutex>
#include <vector>
#include "kernel_types.h"
#include "KernelBase.h"

class KernelRegistry {
public:
    KernelRegistry() { kernelList_.resize(MAX_KERNEL_NUM); };

    virtual ~KernelRegistry() { kernelList_.clear(); };

    static KernelRegistry *GetInstance();

    void RegKernel(KernelKey key, KernelFunc func);

    int GetFuncIndex(const KernelKey &desc);

    KernelFunc GetKernel(const KernelKey &desc);

    KernelFunc GetKernel(int index);

    KernelFunc GetKernel(KernelArch arch, DataType dtype, OpType opid);

protected:
    std::vector<KernelFunc> kernelList_;

private:
    std::mutex lock_;
};

class KernelRegister {
public:
    KernelRegister(const KernelKey &desc, KernelFunc func) {
      KernelRegistry::GetInstance()->RegKernel(desc, func);
    }

    KernelRegister(const KernelArch arch, const DataType dataType, const OpType opType, KernelFunc func) {
      KernelKey key{arch, dataType, opType};
      KernelRegistry::GetInstance()->RegKernel(key, func);
    }

    ~KernelRegister() = default;
};

#define REG_KERNEL(arch, data_type, op_type, kernel_func) \
  static KernelRegister g_##arch##data_type##op_type##kernelReg(arch, data_type, op_type, kernel_func);


#endif //HIGHS_KERNEL_REGISTRY_H

//
// Created by root on 8/12/21.
//

#ifndef HIGHS_DYNAMIC_GRAPH_H
#define HIGHS_DYNAMIC_GRAPH_H

#include <utility>

#include "KernelBase.h"
#include "kernel_types.h"
#include "../graph/KernelRegistry.h"

template<class T1, class T2>
class DynamicGraph {
public:
    explicit DynamicGraph(T1 &simplex, OpType initOpid) : ekk_instance_(simplex), dgInfo_(simplex),
                                                          initOpid_(initOpid) {
      dynKernels_.clear();
    }

    virtual ~DynamicGraph() {
      for (auto iv : dynKernels_) {
        delete iv.second;
      }
    };

    virtual HighsStatus solve() {
      // The major solving loop
      Run();
      return HighsStatus(0);
    };

protected:
    void Run() {
      int ret = 0;
      int id = initOpid_;
      for (;;) {
        std::cout << "kernel id: " << id << std::endl;
        auto kernelIter = dynKernels_.find(id);
        if (kernelIter != dynKernels_.end()) {
          id = kernelIter->second->Run();
          continue;
        }
        auto creator = KernelRegistry::GetInstance()->GetKernel(dgInfo_.rtDesc_.arch, dgInfo_.rtDesc_.dtype,
                                                                OpType(id));
        if (creator == nullptr) {
          break;
        }
        auto kernel = creator(dgInfo_, id, ret);
        dynKernels_[id] = kernel;
        id = kernel->Run();
      }
    }

protected:
    // References:
    T1 &ekk_instance_;
    T2 dgInfo_;
    std::map<int, KernelBase *> dynKernels_;
    const OpType initOpid_;
};

class DynamicGraph1 {
public:
    explicit DynamicGraph1(HighsModel model, OpType initOpid) : model_(model), initOpid_(initOpid) {
      dynKernels_.clear();
    }

    virtual ~DynamicGraph1() {
      for (auto iv : dynKernels_) {
        delete iv.second;
      }
    };

    virtual HighsStatus solve() {
      // The major solving loop
      Run();
      return HighsStatus(0);
    };

protected:
    void Run() {
      int ret = 0;
      int id = initOpid_;
      for (;;) {
        std::cout << "kernel id: " << id << std::endl;
        auto kernelIter = dynKernels_.find(id);
        if (kernelIter != dynKernels_.end()) {
          id = kernelIter->second->Run();
          continue;
        }
        auto creator = KernelRegistry::GetInstance()->GetKernel(dgInfo_.rtDesc_.arch, dgInfo_.rtDesc_.dtype,
                                                                OpType(id));
        if (creator == nullptr) {
          break;
        }
        auto kernel = creator(dgInfo_, id, ret);
        dynKernels_[id] = kernel;
        id = kernel->Run();
      }
    }

protected:
    // References:
    std::map<int, KernelBase *> dynKernels_;
    const OpType initOpid_;
    const HighsModel model_;

    DGInfo dgInfo_;
};
#endif  // HIGHS_DYNAMIC_GRAPH_H

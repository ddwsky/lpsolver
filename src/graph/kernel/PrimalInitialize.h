//
// Created by root on 8/12/21.
//

#ifndef HIGHS_PRIMAL_INITIALIZE_H
#define HIGHS_PRIMAL_INITIALIZE_H

#include "../KernelBase.h"

class PrimalInitialize : public KernelBase {
public:
    PrimalInitialize(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalInitialize() = default;

    int Run() override;

};


#endif // HIGHS_PRIMAL_INITIALIZE_H

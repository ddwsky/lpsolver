//
// Created by root on 8/12/21.
//

#ifndef HIGHS_PRIMAL_PHASE1_POSTPROCESS_H
#define HIGHS_PRIMAL_PHASE1_POSTPROCESS_H

#include "../KernelBase.h"

class PrimalPhase1PostProcess : public KernelBase {
public:
    PrimalPhase1PostProcess(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase1PostProcess() {};

    int Run() override;

};


#endif //HIGHS_PRIMAL_PHASE1_POSTPROCESS_H

//
// Created by root on 8/9/21.
//

#ifndef HIGHS_KERNELBASE_H
#define HIGHS_KERNELBASE_H

#include "kernel_types.h"

class KernelBase {
public:
    KernelBase(DGInfo &info, int id, int rval) : dgInfo_(info), lastId_(id), lastRet_(rval) {};

    ~KernelBase() {};

    virtual int Run() = 0;

protected:
    DGInfo &dgInfo_;
    int lastId_;
    int lastRet_;
};

template<class T>
KernelBase *KernelCreator(DGInfo &info, int id, int rval) {
  auto *kernel = new T(info, id, rval);
  return kernel;
}

using KernelFunc = KernelBase* (*)(DGInfo &cfg, int id, int rval);

#endif //HIGHS_KERNELBASE_H

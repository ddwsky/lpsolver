//
// Created by root on 8/9/21.
//

#ifndef HIGHS_BTRAN_H
#define HIGHS_BTRAN_H

#include "../KernelBase.h"

class BTran : public KernelBase {
public:
    BTran(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~BTran() {};

    int Run() override;
};


#endif //HIGHS_BTRAN_H

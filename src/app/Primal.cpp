/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                       */
/*    This file is part of the HiGHS linear optimization suite           */
/*                                                                       */
/*    Written and engineered 2008-2021 at the University of Edinburgh    */
/*                                                                       */
/*    Available as open-source under the MIT License                     */
/*                                                                       */
/*    Authors: Julian Hall, Ivet Galabova, Qi Huangfu, Leona Gottwald    */
/*    and Michael Feldmeier                                              */
/*                                                                       */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**@file lp_data/Highs.cpp
 * @brief
 */
#include "Primal.h"
#include "PrimalSimplex.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>
#include <sstream>

#include "HConfig.h"
#include "io/Filereader.h"
#include "io/HighsIO.h"
#include "io/LoadOptions.h"
#include "lp_data/HighsInfoDebug.h"
#include "lp_data/HighsLpUtils.h"
#include "lp_data/HighsModelUtils.h"
#include "lp_data/HighsSolution.h"
#include "lp_data/HighsSolve.h"
#include "presolve/ICrashX.h"
#include "simplex/HSimplexDebug.h"
#include "util/HighsMatrixPic.h"

#include "../graph/DGPrimal.h"


#ifdef OPENMP

#include "omp.h"

#endif

#include "simplex/HSimplex.h"
#include "util/HighsSort.h"
#include "simplex/SimplexTimer.h"
#include "simplex/HSimplexReport.h"
#include "simplex/HEkkPrimal.h"

Primal::Primal(HighsOptions &options, HighsTimer &timer)
        : options_(options), analysis_(timer), lp_(model_.lp_) {
}

HighsStatus Primal::clear() {
  resetLocalOptions(options_.records);
  return clearModel();
}

HighsStatus Primal::clearModel() {
  model_.clear();
  return clearSolver();
}

HighsStatus Primal::clearSolver() {
  HighsStatus return_status = HighsStatus::kOk;
  clearUserSolverData();
  return returnFromHighs(return_status);
}

HighsStatus Primal::passOptions(const HighsOptions &options) {
  if (passLocalOptions(options_.log_options, options, options_) ==
      OptionStatus::kOk)
    return HighsStatus::kOk;
  return HighsStatus::kError;
}

HighsStatus Primal::getInfoValue(const std::string &info, double &value) const {
  InfoStatus status =
          getLocalInfoValue(options_, info, info_.valid, info_.records, value);
  if (status == InfoStatus::kOk) {
    return HighsStatus::kOk;
  } else if (status == InfoStatus::kUnavailable) {
    return HighsStatus::kWarning;
  } else {
    return HighsStatus::kError;
  }
}

// Methods below change the incumbent model or solver infomation
// associated with it. Hence returnFromHighs is called at the end of
// each
HighsStatus Primal::passModel(HighsModel model) {
  HighsStatus return_status = HighsStatus::kOk;
  HighsLp &lp = model_.lp_;
  HighsHessian &hessian = model_.hessian_;
  // Move the model's LP and Hessian to the internal LP and Hessian
  lp = std::move(model.lp_);
  hessian = std::move(model.hessian_);
  // Ensure that the LP is column-wise
  return_status =
          interpretCallStatus(setOrientation(lp), return_status, "setOrientation");
  if (return_status == HighsStatus::kError) return return_status;
  // Check validity of the LP, normalising its values
  return_status =
          interpretCallStatus(assessLp(lp, options_), return_status, "assessLp");
  if (return_status == HighsStatus::kError) return return_status;

  // Clear solver status, solution, basis and info associated with any
  // previous model; clear any HiGHS model object; create a HiGHS
  // model object for this LP
  return_status =
          interpretCallStatus(clearSolver(), return_status, "clearSolver");
  return returnFromHighs(return_status);
}

HighsStatus Primal::readModel(const std::string filename) {
  HighsStatus return_status = HighsStatus::kOk;
  Filereader *reader = Filereader::getFilereader(filename);
  if (reader == NULL) {
    highsLogUser(options_.log_options, HighsLogType::kError,
                 "Model file %s not supported\n", filename.c_str());
    return HighsStatus::kError;
  }

  HighsModel model;
  FilereaderRetcode call_code =
          reader->readModelFromFile(options_, filename, model);
  delete reader;
  if (call_code != FilereaderRetcode::kOk) {
    interpretFilereaderRetcode(options_.log_options, filename.c_str(),
                               call_code);
    return_status = interpretCallStatus(HighsStatus::kError, return_status,
                                        "readModelFromFile");
    if (return_status == HighsStatus::kError) return return_status;
  }
  model.lp_.model_name_ = extractModelName(filename);
  return_status = interpretCallStatus(passModel(std::move(model)),
                                      return_status, "passModel");
  return returnFromHighs(return_status);
}

// Checks the options calls presolve and postsolve if needed. Solvers are called
// with callSolveLp(..)
HighsStatus Primal::run() {
  // Set this so that calls to returnFromRun() can be checked
  called_return_from_run = false;
  // From here all return statements execute returnFromRun()
  HighsStatus return_status = HighsStatus::kOk;
  // Initialise the HiGHS model status values
  scaled_model_status_ = HighsModelStatus::kNotset;
  unscaled_model_status_ = HighsModelStatus::kNotset;
  model_status_ = scaled_model_status_;
  scaled_model_status_ = unscaled_model_status_;
  // Clear the run info
  clearInfo();
  // Zero the HiGHS iteration counts
  zeroHighsIterationCounts(iteration_counts_);
  // Start the HiGHS run clock
  timer_.startRunHighsClock();
  highsSetLogCallback(options_);

  // Solve the model as an LP
  //
  // Record the initial time and set the component times and postsolve
  // iteration count to -1 to identify whether they are not required
  double initial_time = timer_.readRunHighsClock();
  double this_presolve_time = -1;
  double this_solve_presolved_lp_time = -1;
  double this_postsolve_time = -1;
  double this_solve_original_lp_time = -1;
  HighsInt postsolve_iteration_count = -1;

//  ekk_instance_.lp_name_ = "LP without presolve or with basis";
  this_solve_original_lp_time = -timer_.read(timer_.solve_clock);
  timer_.start(timer_.solve_clock);

  return_status = Solve();

  timer_.stop(timer_.solve_clock);
  this_solve_original_lp_time += timer_.read(timer_.solve_clock);
  return_status = interpretCallStatus(return_status, return_status, "callSolveLp");
  if (return_status == HighsStatus::kError)
    return returnFromRun(return_status);

  setHighsModelStatusBasisSolutionAndInfo();

  double lp_solve_final_time = timer_.readRunHighsClock();
  double this_solve_time = lp_solve_final_time - initial_time;
  if (postsolve_iteration_count < 0) {
    highsLogDev(options_.log_options, HighsLogType::kInfo, "Postsolve  : \n");
  } else {
    highsLogDev(options_.log_options, HighsLogType::kInfo,
                "Postsolve  : %" HIGHSINT_FORMAT "\n",
                postsolve_iteration_count);
  }
  highsLogDev(options_.log_options, HighsLogType::kInfo, "Time       : %8.2f\n",
              this_solve_time);
  highsLogDev(options_.log_options, HighsLogType::kInfo, "Time Pre   : %8.2f\n",
              this_presolve_time);
  highsLogDev(options_.log_options, HighsLogType::kInfo, "Time PreLP : %8.2f\n",
              this_solve_presolved_lp_time);
  highsLogDev(options_.log_options, HighsLogType::kInfo, "Time PostLP: %8.2f\n",
              this_solve_original_lp_time);
  if (this_solve_time > 0) {
    highsLogDev(options_.log_options, HighsLogType::kInfo, "For LP %16s",
                lp_.model_name_.c_str());
    double sum_time = 0;
    if (this_solve_original_lp_time > 0) {
      sum_time += this_solve_original_lp_time;
      HighsInt pct = (100 * this_solve_original_lp_time) / this_solve_time;
      highsLogDev(options_.log_options, HighsLogType::kInfo,
                  ": Solve original LP %8.2f (%3" HIGHSINT_FORMAT "%%)",
                  this_solve_original_lp_time, pct);
    }
    highsLogDev(options_.log_options, HighsLogType::kInfo, "\n");
    double rlv_time_difference =
            fabs(sum_time - this_solve_time) / this_solve_time;
    if (rlv_time_difference > 0.1)
      highsLogDev(options_.log_options, HighsLogType::kInfo,
                  "Strange: Solve time = %g; Sum times = %g: relative "
                  "difference = %g\n",
                  this_solve_time, sum_time, rlv_time_difference);
  }
  // Assess success according to the scaled model status, unless
  // something worse has happened earlier
  return_status = highsStatusFromHighsModelStatus(scaled_model_status_);
  return_status = interpretCallStatus(return_status, return_status);
  return returnFromRun(return_status);
}

std::string Primal::modelStatusToString(
        const HighsModelStatus model_status) const {
  return utilModelStatusToString(model_status);
}

std::string Primal::solutionStatusToString(
        const HighsInt solution_status) const {
  return utilSolutionStatusToString(solution_status);
}

//std::string Primal::basisStatusToString(
//        const HighsBasisStatus basis_status) const {
//  return utilBasisStatusToString(basis_status);
//}

void Primal::clearUserSolverData() {
  clearModelStatus();
  clearSolution();
  clearBasis();
  clearInfo();
}

void Primal::clearModelStatus() {
  model_status_ = HighsModelStatus::kNotset;
  scaled_model_status_ = HighsModelStatus::kNotset;
}

void Primal::clearSolution() {
  info_.primal_solution_status = kSolutionStatusNone;
  info_.dual_solution_status = kSolutionStatusNone;
  clearSolutionUtil(solution_);
}

void Primal::clearBasis() { clearBasisUtil(basis_); }

void Primal::clearInfo() { info_.clear(); }

// Ensure that the HiGHS solution and basis have the same size as the
// model, and that the HiGHS basis is kept up-to-date with any solved
// basis
void Primal::forceHighsSolutionBasisSize() {
  // Ensure that the HiGHS solution vectors are the right size
  solution_.col_value.resize(model_.lp_.numCol_);
  solution_.row_value.resize(model_.lp_.numRow_);
  solution_.col_dual.resize(model_.lp_.numCol_);
  solution_.row_dual.resize(model_.lp_.numRow_);
  // Ensure that the HiGHS basis vectors are the right size,
  // invalidating the basis if they aren't
  if ((HighsInt) basis_.col_status.size() != model_.lp_.numCol_) {
    basis_.col_status.resize(model_.lp_.numCol_);
    basis_.valid = false;
  }
  if ((HighsInt) basis_.row_status.size() != model_.lp_.numRow_) {
    basis_.row_status.resize(model_.lp_.numRow_);
    basis_.valid = false;
  }
}

void Primal::setHighsModelStatusBasisSolutionAndInfo() {
//  assert(haveHmo("setHighsModelStatusBasisSolutionAndInfo"));
//  clearUserSolverData();

  model_status_ = unscaled_model_status_;
  scaled_model_status_ = scaled_model_status_;

  basis_ = basis_;
  solution_ = solution_;

  info_.simplex_iteration_count = iteration_counts_.simplex;
  info_.ipm_iteration_count = iteration_counts_.ipm;
  info_.crossover_iteration_count = iteration_counts_.crossover;
  info_.qp_iteration_count = iteration_counts_.qp;

  HighsSolutionParams &solution_params = solution_params_;
  info_.primal_solution_status = solution_params.primal_solution_status;
  info_.dual_solution_status = solution_params.dual_solution_status;
  if (basis_.valid) {
    info_.basis_validity = kBasisValidityValid;
  } else {
    info_.basis_validity = kBasisValidityInvalid;
  }
  info_.objective_function_value = solution_params.objective_function_value;
  info_.num_primal_infeasibilities = solution_params.num_primal_infeasibility;
  info_.max_primal_infeasibility = solution_params.max_primal_infeasibility;
  info_.sum_primal_infeasibilities = solution_params.sum_primal_infeasibility;
  info_.num_dual_infeasibilities = solution_params.num_dual_infeasibility;
  info_.max_dual_infeasibility = solution_params.max_dual_infeasibility;
  info_.sum_dual_infeasibilities = solution_params.sum_dual_infeasibility;
  info_.valid = true;
}

// Applies checks before returning from run()
HighsStatus Primal::returnFromRun(const HighsStatus run_return_status) {
  assert(!called_return_from_run);
  HighsStatus return_status =
          highsStatusFromHighsModelStatus(scaled_model_status_);
  assert(return_status == run_return_status);
  //  return_status = run_return_status;
  // Make sure that the unscaled status, solution, basis and info
  // are consistent with the scaled status

  // ToDo: Outcome of Run() should be driven by model_status_, not
  // scaled_model_status_. This is currently done because latter may
  // be optimal but tolerances not satisfied for unscaled model.
  switch (scaled_model_status_) {
    // First consider the error returns
    case HighsModelStatus::kNotset:
    case HighsModelStatus::kLoadError:
    case HighsModelStatus::kModelError:
    case HighsModelStatus::kPresolveError:
    case HighsModelStatus::kSolveError:
    case HighsModelStatus::kPostsolveError:
      clearUserSolverData();
      assert(return_status == HighsStatus::kError);
      break;

      // Then consider the OK returns
    case HighsModelStatus::kModelEmpty:
      clearInfo();
      clearSolution();
      clearBasis();
      assert(model_status_ == scaled_model_status_);
      assert(return_status == HighsStatus::kOk);
      break;

    case HighsModelStatus::kOptimal:
      // The following is an aspiration
      //
      // assert(info_.primal_solution_status == kSolutionStatusFeasible);
      //
      // assert(info_.dual_solution_status == kSolutionStatusFeasible);
      assert(model_status_ == HighsModelStatus::kNotset ||
             model_status_ == HighsModelStatus::kOptimal);
      assert(return_status == HighsStatus::kOk);
      break;

    case HighsModelStatus::kInfeasible:
    case HighsModelStatus::kUnbounded:
    case HighsModelStatus::kObjectiveBound:
    case HighsModelStatus::kObjectiveTarget:
      // For kInfeasible, will not have a basis, if infeasibility was
      // detected in presolve or by IPX without crossover
      assert(model_status_ == scaled_model_status_);
      assert(return_status == HighsStatus::kOk);
      break;

    case HighsModelStatus::kUnboundedOrInfeasible:
      if (options_.allow_unbounded_or_infeasible ||
          (options_.solver == kIpmString && options_.run_crossover)) {
        assert(model_status_ == scaled_model_status_);
        assert(return_status == HighsStatus::kOk);
      } else {
        // This model status is not permitted unless IPM is run without
        // crossover
        highsLogUser(
                options_.log_options, HighsLogType::kError,
                "returnFromHighs: HighsModelStatus::kUnboundedOrInfeasible is not "
                "permitted\n");
        assert(options_.allow_unbounded_or_infeasible);
        return_status = HighsStatus::kError;
      }
      break;

      // Finally consider the warning returns
    case HighsModelStatus::kTimeLimit:
    case HighsModelStatus::kIterationLimit:
    case HighsModelStatus::kUnknown:
      assert(model_status_ == scaled_model_status_);
      assert(return_status == HighsStatus::kWarning);
      break;
    default:
      // All cases should have been considered so assert on reaching here
      assert(1 == 0);
  }
  // Now to check what's available with each model status
  //
  const bool have_info = info_.valid;
  const bool have_primal_solution = solution_.value_valid;
  const bool have_dual_solution = solution_.dual_valid;
  // Can't have a dual solution without a primal solution
  assert(have_primal_solution || !have_dual_solution);
  //  const bool have_solution = have_primal_solution && have_dual_solution;
  const bool have_basis = basis_.valid;
  switch (scaled_model_status_) {
    case HighsModelStatus::kNotset:
    case HighsModelStatus::kLoadError:
    case HighsModelStatus::kModelError:
    case HighsModelStatus::kPresolveError:
    case HighsModelStatus::kSolveError:
    case HighsModelStatus::kPostsolveError:
    case HighsModelStatus::kModelEmpty:
      // No info, primal solution or basis
      assert(have_info == false);
      assert(have_primal_solution == false);
      assert(have_basis == false);
      break;
    case HighsModelStatus::kOptimal:
    case HighsModelStatus::kInfeasible:
    case HighsModelStatus::kUnbounded:
    case HighsModelStatus::kObjectiveBound:
    case HighsModelStatus::kObjectiveTarget:
    case HighsModelStatus::kUnboundedOrInfeasible:
    case HighsModelStatus::kTimeLimit:
    case HighsModelStatus::kIterationLimit:
    case HighsModelStatus::kUnknown:
      // Have info and primal solution (unless infeasible). No primal solution
      // in some other case, too!
      assert(have_info == true);
      //      if (have_primal_solution == true || scaled_model_status_ ==
      //      HighsModelStatus::kInfeasible);
      break;
    default:
      // All cases should have been considered so assert on reaching here
      assert(1 == 0);
  }
  if (have_primal_solution) {
    if (debugPrimalSolutionRightSize(options_, model_.lp_, solution_) ==
        HighsDebugStatus::kLogicalError)
      return_status = HighsStatus::kError;
  }
  if (have_dual_solution) {
    if (debugDualSolutionRightSize(options_, model_.lp_, solution_) ==
        HighsDebugStatus::kLogicalError)
      return_status = HighsStatus::kError;
  }
  if (have_basis) {
    if (debugBasisRightSize(options_, model_.lp_, basis_) ==
        HighsDebugStatus::kLogicalError)
      return_status = HighsStatus::kError;
  }
  if (debugHighsSolution("Return from run()", options_, model_.lp_, solution_,
                         basis_, model_status_,
                         info_) == HighsDebugStatus::kLogicalError)
    return_status = HighsStatus::kError;
  //  getReportKktFailures(options_, model_.lp_, solution_, basis_);
  if (debugInfo(options_, model_.lp_, basis_, solution_, info_,
                scaled_model_status_) == HighsDebugStatus::kLogicalError)
    return_status = HighsStatus::kError;
  // Record that returnFromRun() has been called, and stop the Highs
  // run clock
  called_return_from_run = true;

  return returnFromHighs(return_status);
}

// Applies checks before returning from HiGHS
HighsStatus Primal::returnFromHighs(HighsStatus highs_return_status) {
  HighsStatus return_status = highs_return_status;

  forceHighsSolutionBasisSize();

  const bool consistent = debugBasisConsistent(options_, model_.lp_, basis_) !=
                          HighsDebugStatus::kLogicalError;
  if (!consistent) {
    highsLogUser(
            options_.log_options, HighsLogType::kError,
            "returnFromHighs: Supposed to be a HiGHS basis, but not consistent\n");
    assert(consistent);
    return_status = HighsStatus::kError;
  }
  bool simplex_lp_ok = 1;
//            ekkDebugSimplexLp(hmos_[0]) != HighsDebugStatus::kLogicalError;
  if (!simplex_lp_ok) {
    highsLogUser(options_.log_options, HighsLogType::kError,
                 "returnFromHighs: Simplex LP not OK\n");
    assert(simplex_lp_ok);
    return_status = HighsStatus::kError;
  }
  // Check that returnFromRun() has been called
  if (!called_return_from_run) {
    highsLogDev(
            options_.log_options, HighsLogType::kError,
            "Primal::returnFromHighs() called with called_return_from_run false\n");
    assert(called_return_from_run);
  }
  // Stop the HiGHS run clock if it is running
  if (timer_.runningRunHighsClock()) timer_.stopRunHighsClock();
  return return_status;
}

HighsStatus Primal::returnFromSolve(const HighsStatus return_status) {
  // Always called before returning from HEkkPrimal/Dual::solve()
  if (solve_bailout_) {
    // If bailout has already been decided: check that it's for one of
    // these reasons
    assert(model_status_ == HighsModelStatus::kTimeLimit ||
           model_status_ == HighsModelStatus::kIterationLimit ||
           model_status_ == HighsModelStatus::kObjectiveBound ||
           model_status_ == HighsModelStatus::kObjectiveTarget);
  }
  // Check that returnFromSolve has not already been called: it should
  // be called exactly once per solve
  assert(!called_return_from_solve_);
  called_return_from_solve_ = true;
  simplexinfo_.valid_backtracking_basis_ = false;

  // Initialise the status of the primal and dual solutions
  return_primal_solution_status = kSolutionStatusNone;
  return_dual_solution_status = kSolutionStatusNone;
  // Nothing more is known about the solve after an error return
  if (return_status == HighsStatus::kError) return return_status;

  // Check that an invert exists
  assert(status_.has_invert);

  // Determine a primal and dual solution, removing the effects of
  // perturbations and shifts
  //
  // Unless the solution is optimal, invalidate the infeasibility data
  if (model_status_ != HighsModelStatus::kOptimal) {
    simplexinfo_.max_primal_infeasibility = kHighsIllegalInfeasibilityMeasure;
    simplexinfo_.sum_primal_infeasibility = kHighsIllegalInfeasibilityMeasure;

    simplexinfo_.num_primal_infeasibility = kHighsIllegalInfeasibilityCount;
    simplexinfo_.max_dual_infeasibility = kHighsIllegalInfeasibilityMeasure;
    simplexinfo_.sum_dual_infeasibility = kHighsIllegalInfeasibilityMeasure;
    simplexinfo_.num_dual_infeasibility = kHighsIllegalInfeasibilityCount;
  }
  switch (model_status_) {
    case HighsModelStatus::kOptimal: {
      assert(simplexinfo_.num_primal_infeasibility == 0);
      assert(simplexinfo_.num_dual_infeasibility == 0);
      return_primal_solution_status = kSolutionStatusFeasible;
      return_dual_solution_status = kSolutionStatusFeasible;
      break;
    }
    case HighsModelStatus::kInfeasible: {
      // Primal simplex has identified primal infeasibility in phase 1, or
      // dual simplex has identified dual unboundedness in phase 2. In
      // both cases there should be no primal or dual perturbations
      assert(!simplexinfo_.costs_perturbed && !simplexinfo_.bounds_perturbed);
      if (exit_algorithm == SimplexAlgorithm::kPrimal) {
        // Reset the simplex costs and recompute duals after primal
        // phase 1
        initialiseCost(SimplexAlgorithm::kDual, kSolvePhase2);
        computeDual();
      }
      computeSimplexPrimalInfeasible();
      computeSimplexDualInfeasible();
      // Primal solution shouldn't be feasible
      assert(simplexinfo_.num_primal_infeasibility > 0);
      break;
    }
    case HighsModelStatus::kUnboundedOrInfeasible: {
      // Dual simplex has identified dual infeasibility in phase
      // 1. There should be no dual perturbations
      assert(exit_algorithm == SimplexAlgorithm::kDual);
      assert(!simplexinfo_.costs_perturbed);
      // Reset the simplex bounds and recompute primals
      initialiseBound(SimplexAlgorithm::kDual, kSolvePhase2);
      computePrimal();
      computeSimplexPrimalInfeasible();
      computeSimplexDualInfeasible();
      // Dual solution shouldn't be feasible
      assert(simplexinfo_.num_dual_infeasibility > 0);
      break;
    }
    case HighsModelStatus::kUnbounded: {
      // Primal simplex has identified unboundedness in phase 2. There
      // should be no primal or dual perturbations
      assert(exit_algorithm == SimplexAlgorithm::kPrimal);
      assert(!simplexinfo_.costs_perturbed && !simplexinfo_.bounds_perturbed);
      computeSimplexPrimalInfeasible();
      computeSimplexDualInfeasible();
      // Primal solution should be feasible
      assert(simplexinfo_.num_primal_infeasibility == 0);
      break;
    }
    case HighsModelStatus::kObjectiveBound:
    case HighsModelStatus::kObjectiveTarget:
    case HighsModelStatus::kTimeLimit:
    case HighsModelStatus::kIterationLimit: {
      // Simplex has bailed out due to reaching the objecive cut-off,
      // time or iteration limit. Could happen anywhere (other than
      // the fist implying dual simplex)
      //
      // Reset the simplex bounds and recompute primals
      initialiseBound(SimplexAlgorithm::kDual, kSolvePhase2);
      initialiseNonbasicValueAndMove();
      computePrimal();
      // Reset the simplex costs and recompute duals
      initialiseCost(SimplexAlgorithm::kDual, kSolvePhase2);
      computeDual();
      computeSimplexPrimalInfeasible();
      computeSimplexDualInfeasible();
      break;
    }
    default: {
      std::string algorithm_name = "primal";
      if (exit_algorithm == SimplexAlgorithm::kDual) algorithm_name = "dual";
      highsLogDev(options_.log_options, HighsLogType::kError,
                  "EKK %s simplex solver returns status %s\n",
                  algorithm_name.c_str(),
                  utilModelStatusToString(model_status_).c_str());
      return HighsStatus::kError;
      break;
    }
  }
  assert(simplexinfo_.num_primal_infeasibility >= 0);
  assert(simplexinfo_.num_dual_infeasibility >= 0);
  if (simplexinfo_.num_primal_infeasibility == 0) {
    return_primal_solution_status = kSolutionStatusFeasible;
  } else {
    return_primal_solution_status = kSolutionStatusInfeasible;
  }
  if (simplexinfo_.num_dual_infeasibility == 0) {
    return_dual_solution_status = kSolutionStatusFeasible;
  } else {
    return_dual_solution_status = kSolutionStatusInfeasible;
  }
  computePrimalObjectiveValue();

  return return_status;
}

HighsSolution Primal::getSolution() {
  HighsSolution solution;
  // Scatter the basic primal values
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++)
    simplexinfo_.workValue_[simplexbasis_.basicIndex_[iRow]] = simplexinfo_.baseValue_[iRow];
  // Zero the basic dual values
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++)
    simplexinfo_.workDual_[simplexbasis_.basicIndex_[iRow]] = 0;

  // Now we can get the solution
  solution.col_value.resize(lp_.numCol_);
  solution.col_dual.resize(lp_.numCol_);
  solution.row_value.resize(lp_.numRow_);
  solution.row_dual.resize(lp_.numRow_);

  for (HighsInt iCol = 0; iCol < lp_.numCol_; iCol++) {
    solution.col_value[iCol] = simplexinfo_.workValue_[iCol];
    solution.col_dual[iCol] = (HighsInt) lp_.sense_ * simplexinfo_.workDual_[iCol];
  }
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++) {
    solution.row_value[iRow] = -simplexinfo_.workValue_[lp_.numCol_ + iRow];
    // @FlipRowDual negate RHS
    solution.row_dual[iRow] = -(HighsInt) lp_.sense_ * simplexinfo_.workDual_[lp_.numCol_ + iRow];
  }
  solution.value_valid = true;
  solution.dual_valid = true;
  return solution;
}

HighsBasis Primal::getHighsBasis() {
  HighsInt num_col = lp_.numCol_;
  HighsInt num_row = lp_.numRow_;
  HighsBasis highs_basis;
  highs_basis.col_status.resize(num_col);
  highs_basis.row_status.resize(num_row);
  assert(status_.has_basis);
  highs_basis.valid = false;
  for (HighsInt iCol = 0; iCol < num_col; iCol++) {
    HighsInt iVar = iCol;
    const double lower = lp_.colLower_[iCol];
    const double upper = lp_.colUpper_[iCol];
    HighsBasisStatus basis_status = HighsBasisStatus::kNonbasic;
    if (!simplexbasis_.nonbasicFlag_[iVar]) {
      basis_status = HighsBasisStatus::kBasic;
    } else if (simplexbasis_.nonbasicMove_[iVar] == kNonbasicMoveUp) {
      basis_status = HighsBasisStatus::kLower;
    } else if (simplexbasis_.nonbasicMove_[iVar] == kNonbasicMoveDn) {
      basis_status = HighsBasisStatus::kUpper;
    } else if (simplexbasis_.nonbasicMove_[iVar] == kNonbasicMoveZe) {
      if (lower == upper) {
        basis_status = HighsBasisStatus::kLower;
      } else {
        basis_status = HighsBasisStatus::kZero;
      }
    }
    highs_basis.col_status[iCol] = basis_status;
  }
  for (HighsInt iRow = 0; iRow < num_row; iRow++) {
    HighsInt iVar = num_col + iRow;
    const double lower = lp_.rowLower_[iRow];
    const double upper = lp_.rowUpper_[iRow];
    HighsBasisStatus basis_status = HighsBasisStatus::kNonbasic;
    if (!simplexbasis_.nonbasicFlag_[iVar]) {
      basis_status = HighsBasisStatus::kBasic;
    } else if (simplexbasis_.nonbasicMove_[iVar] == kNonbasicMoveUp) {
      basis_status = HighsBasisStatus::kUpper;
    } else if (simplexbasis_.nonbasicMove_[iVar] == kNonbasicMoveDn) {
      basis_status = HighsBasisStatus::kLower;
    } else if (simplexbasis_.nonbasicMove_[iVar] == kNonbasicMoveZe) {
      if (lower == upper) {
        basis_status = HighsBasisStatus::kLower;
      } else {
        basis_status = HighsBasisStatus::kZero;
      }
    }
    highs_basis.row_status[iRow] = basis_status;
  }
  highs_basis.valid = true;
  return highs_basis;
}

HighsStatus Primal::Solve() {
  HighsStatus return_status = HighsStatus::kOk;
  //  HighsStatus call_status;
  HighsOptions &options = options_;
  HighsSimplexStatus &status = status_;

  // Reset the model status and solution parameters for the unscaled
  // LP in case of premature return
  resetModelStatusAndSolutionParams(unscaled_model_status_, solution_params_, options_);

  // Assumes that the LP has a positive number of rows, since
  // unconstrained LPs should be solved in solveLp
  bool positive_num_row = lp_.numRow_ > 0;
  assert(positive_num_row);
  if (!positive_num_row) {
    highsLogUser(options.log_options, HighsLogType::kError,
                 "solveLpSimplex called for LP with non-positive (%" HIGHSINT_FORMAT
                 ") number of constraints\n", lp_.numRow_);
    return HighsStatus::kError;
  }

  // If the simplex LP isn't initialised, scale and pass the current LP
  if (!status.initialised) {
    HighsOptions &options = options_;
    // Possibly scale the LP
    bool scale_lp = options.simplex_scale_strategy != kSimplexScaleStrategyOff && lp_.numCol_ > 0;
    const bool force_no_scaling = true;  // false;  //
    if (force_no_scaling) {
      highsLogUser(options.log_options, HighsLogType::kWarning,
                   "Forcing no scaling\n");
      scale_lp = false;
    }
    const bool analyse_lp_data =
            kHighsAnalysisLevelModelData & options.highs_analysis_level;
    if (analyse_lp_data)
      analyseLp(options.log_options, lp_, "Unscaled");
    // Possibly scale the LP. At least set the scaling factors to 1
    HighsScale &scale = scale_;
    if (scale_lp) {
      HighsLp scaled_lp = lp_;
      // Perform scaling - if it's worth it.
      scaleSimplexLp(options, scaled_lp, scale);
      if (analyse_lp_data) analyseScaledLp(options.log_options, scale, scaled_lp);
    } else {
      // Initialise unit scaling factors
      initialiseScale(lp_, scale);
    }
  }

  // Solve the LP!
  return_status = SolveInternal();
  if (return_status == HighsStatus::kError) return HighsStatus::kError;

  // Copy solution data into the HMO
  HighsSolutionParams &solution_params = solution_params_;
  scaled_model_status_ = model_status_;
  solution_params.objective_function_value = simplexinfo_.primal_objective_value;
  iteration_counts_.simplex += iteration_count_;
  solution_ = getSolution();
  if (scale_.is_scaled)
    unscaleSolution(solution_, scale_);
  basis_ = getHighsBasis();

  // Determine whether the unscaled LP has been solved
  double new_primal_feasibility_tolerance;
  double new_dual_feasibility_tolerance;
  //  HighsSolutionParams solution_;
  getUnscaledInfeasibilitiesAndNewTolerances(
          options_, lp_, model_status_, simplexbasis_, simplexinfo_, scale_, solution_params,
          new_primal_feasibility_tolerance, new_dual_feasibility_tolerance);

  HighsInt num_unscaled_primal_infeasibility = solution_params.num_primal_infeasibility;
  HighsInt num_unscaled_dual_infeasibility = solution_params.num_dual_infeasibility;

  if (num_unscaled_primal_infeasibility > 0) {
    solution_params.primal_solution_status = kSolutionStatusInfeasible;
  } else {
    solution_params.primal_solution_status = kSolutionStatusFeasible;
  }
  if (num_unscaled_dual_infeasibility > 0) {
    solution_params.dual_solution_status = kSolutionStatusInfeasible;
  } else {
    solution_params.dual_solution_status = kSolutionStatusFeasible;
  }

  // Handle non-optimal status
  if (model_status_ != HighsModelStatus::kOptimal) {
    unscaled_model_status_ = model_status_;
    return_status = highsStatusFromHighsModelStatus(model_status_);
    return return_status;
  }

  // Now interpret the status of the unscaled solution when the scaled
  // LP is solved to optimailty
  assert(model_status_ == HighsModelStatus::kOptimal);

  // Set the model and solution status according to the unscaled solution
  // parameters
  if (num_unscaled_primal_infeasibility == 0 &&
      num_unscaled_dual_infeasibility == 0) {
    // Optimal
    unscaled_model_status_ = HighsModelStatus::kOptimal;
  } else {
    // Not optimal - should try refinement
    assert(num_unscaled_primal_infeasibility > 0 ||
           num_unscaled_dual_infeasibility > 0);
    unscaled_model_status_ = HighsModelStatus::kNotset;
    highsLogUser(options_.log_options, HighsLogType::kInfo,
                 "Have num/max/sum primal (%" HIGHSINT_FORMAT
                 "/%g/%g) and dual (%" HIGHSINT_FORMAT
                 "/%g/%g) "
                 "unscaled infeasibilities\n",
                 num_unscaled_primal_infeasibility,
                 solution_params.max_primal_infeasibility,
                 solution_params.sum_primal_infeasibility,
                 num_unscaled_dual_infeasibility,
                 solution_params.max_dual_infeasibility,
                 solution_params.sum_dual_infeasibility);
    if (model_status_ == HighsModelStatus::kOptimal)
      highsLogUser(options_.log_options, HighsLogType::kInfo,
                   "Possibly re-solve with feasibility tolerances of %g "
                   "primal and %g dual\n",
                   new_primal_feasibility_tolerance,
                   new_dual_feasibility_tolerance);
    solution_ = getSolution();
    if (scale_.is_scaled)
      unscaleSolution(solution_, scale_);
    basis_ = getHighsBasis();
  }
  return_status = interpretCallStatus(return_status, return_status, "solveLp");
  return return_status;
}

HighsStatus Primal::SolveInternal() {
  analysis_.setup(lp_name_, lp_, options_, iteration_count_);
  analysis_.simplexTimerStart(SimplexTotalClock);

  if (initialiseForSolve() == HighsStatus::kError) return HighsStatus::kError;

  assert(status_.has_basis);
  assert(status_.has_invert);
  assert(status_.valid);
  if (model_status_ == HighsModelStatus::kOptimal) return HighsStatus::kOk;

  HighsStatus return_status = HighsStatus::kOk;
  HighsStatus call_status;

  // Allow primal and dual perturbations in case a block on them is
  // hanging over from a previous call
  simplexinfo_.allow_cost_perturbation = true;
  simplexinfo_.allow_bound_perturbation = true;

  // Initial solve according to strategy
  reportSimplexPhaseIterations(options_.log_options, iteration_count_, simplexinfo_, true);
  highsLogUser(options_.log_options, HighsLogType::kInfo, "Using EKK primal simplex solver\n");
  workEdWt_ = NULL;
  workEdWtFull_ = NULL;

#define DG_ENABLE
#ifndef DG_ENABLE
  PrimalSimplex primal_solver(*this);
  call_status = primal_solver.solve();
#else
  DGPrimal primal_solver(*this, OP_PRIMAL_ENTER);
  call_status = primal_solver.solve();
#endif
  assert(called_return_from_solve_);
  return_status = interpretCallStatus(call_status, return_status, "HEkkPrimal::solve");
  reportSimplexPhaseIterations(options_.log_options, iteration_count_, simplexinfo_);
  if (return_status == HighsStatus::kError) return return_status;
  highsLogDev(options_.log_options, HighsLogType::kInfo,
              "EKK %s simplex solver returns %" HIGHSINT_FORMAT
              " primal and %" HIGHSINT_FORMAT
              " dual infeasibilities: "
              "Status %s\n",
              "primal", simplexinfo_.num_primal_infeasibility,
              simplexinfo_.num_dual_infeasibility,
              utilModelStatusToString(model_status_).c_str());
  // Can model_status_ = HighsModelStatus::kNotset be returned?
  assert(model_status_ != HighsModelStatus::kNotset);

  return return_status;
}

HighsStatus Primal::initialiseForSolve() {
  allocateWorkAndBaseArrays();
  initialiseForNewLp();
  const HighsInt error_return = initialiseSimplexLpBasisAndFactor();
  assert(!error_return);
  if (error_return) return HighsStatus::kError;
  assert(status_.has_basis);

  initialiseMatrix();  // Timed
  initialiseCost(SimplexAlgorithm::kPrimal, kSolvePhaseUnknown, false);
  initialiseBound(SimplexAlgorithm::kPrimal, kSolvePhaseUnknown, false);
  computePrimal();                // Timed
  computeDual();                  // Timed
  computeSimplexPrimalInfeasible();
  computeSimplexDualInfeasible();
  computeDualObjectiveValue();    // Timed
  computePrimalObjectiveValue();  // Timed
  status_.valid = true;

  bool primal_feasible = simplexinfo_.num_primal_infeasibility == 0;
  bool dual_feasible = simplexinfo_.num_dual_infeasibility == 0;
  model_status_ = HighsModelStatus::kNotset;
  if (primal_feasible && dual_feasible)
    model_status_ = HighsModelStatus::kOptimal;
  return HighsStatus::kOk;
}

void Primal::initialiseForNewLp() {
  setSimplexOptions();
  initialiseControl();
  initialiseSimplexLpRandomVectors();
  status_.initialised = true;
}

void Primal::setSimplexOptions() {
  // Copy values of HighsOptions for the simplex solver
  // Currently most of these options are straight copies, but they
  // will become valuable when "choose" becomes a HiGHS strategy value
  // that will need converting into a specific simplex strategy value.
  //
  // NB simplex_strategy is set by chooseSimplexStrategyThreads in each call
  //
  simplexinfo_.dual_edge_weight_strategy = options_.simplex_dual_edge_weight_strategy;
  simplexinfo_.price_strategy = options_.simplex_price_strategy;
  simplexinfo_.dual_simplex_cost_perturbation_multiplier = options_.dual_simplex_cost_perturbation_multiplier;
  simplexinfo_.primal_simplex_bound_perturbation_multiplier = options_.primal_simplex_bound_perturbation_multiplier;
  simplexinfo_.factor_pivot_threshold = options_.factor_pivot_threshold;
  simplexinfo_.update_limit = options_.simplex_update_limit;
  random_.initialise(options_.highs_random_seed);

  // Set values of internal options
  simplexinfo_.store_squared_primal_infeasibility = true;
}

void Primal::initialiseControl() {
  // Copy tolerances from options
  simplexinfo_.allow_dual_steepest_edge_to_devex_switch =
          options_.simplex_dual_edge_weight_strategy == kSimplexDualEdgeWeightStrategyChoose;
  simplexinfo_.dual_steepest_edge_weight_log_error_threshold = options_.dual_steepest_edge_weight_log_error_threshold;
  // Initialise the iteration count when control started. Need to
  // consider what to do if this isn't zero
  assert(iteration_count_ == 0);
  simplexinfo_.control_iteration_count0 = iteration_count_;
  // Initialise the densities
  simplexinfo_.col_aq_density = 0;
  simplexinfo_.row_ep_density = 0;
  simplexinfo_.row_ap_density = 0;
  simplexinfo_.row_DSE_density = 0;
  simplexinfo_.col_basic_feasibility_change_density = 0;
  simplexinfo_.row_basic_feasibility_change_density = 0;
  simplexinfo_.col_BFRT_density = 0;
  simplexinfo_.primal_col_density = 0;
  // Set the row_dual_density to 1 since it's assumed all costs are at
  // least perturbed from zero, if not initially nonzero
  simplexinfo_.dual_col_density = 1;
  // Initialise the data used to determine the switch from DSE to
  // Devex
  simplexinfo_.costly_DSE_frequency = 0;
  simplexinfo_.num_costly_DSE_iteration = 0;
  simplexinfo_.average_log_low_DSE_weight_error = 0;
  simplexinfo_.average_log_high_DSE_weight_error = 0;
}

void Primal::initialiseSimplexLpRandomVectors() {
  const HighsInt num_col = lp_.numCol_;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  simplexinfo_.numTotRandomValue_.resize(num_tot);
  return;
  if (!num_tot) return;
  // Instantiate and (re-)initialise the random number generator
  //  HighsRandom random;
  HighsRandom &random = random_;
  //  random.initialise();

  // Generate a vector of random reals
  simplexinfo_.numTotRandomValue_.resize(num_tot);
  vector<double> &numTotRandomValue = simplexinfo_.numTotRandomValue_;
  for (HighsInt i = 0; i < num_tot; i++) {
    numTotRandomValue[i] = random.fraction();
  }
}

HighsInt Primal::initialiseSimplexLpBasisAndFactor(const bool only_from_known_basis) {
  // If there's no basis, return error if the basis has to be known,
  // otherwise set a logical basis
  if (!status_.has_basis) {
    if (only_from_known_basis) {
      highsLogUser(options_.log_options, HighsLogType::kError, "Simplex basis should be known but isn't\n");
      return -(HighsInt) HighsStatus::kError;
    }
    setBasis();
  }
  const HighsInt rank_deficiency = computeFactor();
  assert(status_.has_invert);
  return 0;
}

void Primal::updateSimplexOptions() {
  // Update some simplex option values from HighsOptions when
  // (re-)solving an LP. Others aren't changed because better values
  // may have been learned due to solving this LP (possibly with some
  // modification) before.
  //
  // NB simplex_strategy is set by chooseSimplexStrategyThreads in each call
  //
  simplexinfo_.dual_simplex_cost_perturbation_multiplier = options_.dual_simplex_cost_perturbation_multiplier;
  simplexinfo_.primal_simplex_bound_perturbation_multiplier = options_.primal_simplex_bound_perturbation_multiplier;
}

void Primal::initialiseMatrix() {
  if (!status_.has_matrix) {
    analysis_.simplexTimerStart(matrixSetupClock);
    matrix_.setup(lp_.numCol_, lp_.numRow_, &lp_.Astart_[0], &lp_.Aindex_[0],
                  &lp_.Avalue_[0], &simplexbasis_.nonbasicFlag_[0]);
    status_.has_matrix = true;
    analysis_.simplexTimerStop(matrixSetupClock);
  }
}

void Primal::allocateWorkAndBaseArrays() {
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  simplexinfo_.workCost_.resize(num_tot);
  simplexinfo_.workDual_.resize(num_tot);
  simplexinfo_.workShift_.resize(num_tot);

  simplexinfo_.workLower_.resize(num_tot);
  simplexinfo_.workUpper_.resize(num_tot);
  simplexinfo_.workRange_.resize(num_tot);
  simplexinfo_.workValue_.resize(num_tot);
  simplexinfo_.workLowerShift_.resize(num_tot);
  simplexinfo_.workUpperShift_.resize(num_tot);

  // Feel that it should be possible to resize this with in dual
  // solver, and only if Devex is being used, but a pointer to it
  // needs to be set up when constructing HDual
  simplexinfo_.devex_index_.resize(num_tot);

  simplexinfo_.baseLower_.resize(lp_.numRow_);
  simplexinfo_.baseUpper_.resize(lp_.numRow_);
  simplexinfo_.baseValue_.resize(lp_.numRow_);
}

void Primal::initialiseCost(const SimplexAlgorithm algorithm, const HighsInt solve_phase, const bool perturb) {
  // Copy the cost
  initialiseLpColCost();
  initialiseLpRowCost();
  simplexinfo_.costs_perturbed = 0;
  // Primal simplex costs are either from the LP or set specially in phase 1
  if (algorithm == SimplexAlgorithm::kPrimal) return;
  // Dual simplex costs are either from the LP or perturbed
  if (!perturb || simplexinfo_.dual_simplex_cost_perturbation_multiplier == 0) return;
  // Perturb the original costs, scale down if is too big
  HighsInt num_original_nonzero_cost = 0;
  if (analysis_.analyse_simplex_data)
    printf("grep_DuPtrb: Cost perturbation for %s\n", lp_.model_name_.c_str());
  double bigc = 0;
  for (HighsInt i = 0; i < lp_.numCol_; i++) {
    const double abs_cost = fabs(simplexinfo_.workCost_[i]);
    bigc = max(bigc, abs_cost);
    if (analysis_.analyse_simplex_data && abs_cost) num_original_nonzero_cost++;
  }
  const HighsInt pct0 = (100 * num_original_nonzero_cost) / lp_.numCol_;
  double average_cost = 0;
  if (analysis_.analyse_simplex_data) {
    if (num_original_nonzero_cost) {
      average_cost = bigc / num_original_nonzero_cost;
    } else {
      printf("grep_DuPtrb:    STRANGE initial workCost has non nonzeros\n");
    }
    printf("grep_DuPtrb:    Initially have %" HIGHSINT_FORMAT
           " nonzero costs (%3" HIGHSINT_FORMAT
           "%%) with bigc = "
           "%g "
           "and average = %g\n",
           num_original_nonzero_cost, pct0, bigc, average_cost);
  }
  if (bigc > 100) {
    bigc = sqrt(sqrt(bigc));
    if (analysis_.analyse_simplex_data)
      printf("grep_DuPtrb:    Large so set bigc = sqrt(bigc) = %g\n", bigc);
  }

  // If there are few boxed variables, we will just use simple perturbation
  double boxedRate = 0;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  for (HighsInt i = 0; i < num_tot; i++)
    boxedRate += (simplexinfo_.workRange_[i] < 1e30);
  boxedRate /= num_tot;
  if (boxedRate < 0.01) {
    bigc = min(bigc, 1.0);
    if (analysis_.analyse_simplex_data)
      printf("grep_DuPtrb:    small boxedRate (%g) so set bigc = min(bigc, 1.0) = %g\n", boxedRate, bigc);
  }
  // Determine the perturbation base
  double base = 5e-7 * bigc;
  if (analysis_.analyse_simplex_data)
    printf("grep_DuPtrb:    Perturbation base = %g\n", base);

  // Now do the perturbation
  for (HighsInt i = 0; i < lp_.numCol_; i++) {
    double lower = lp_.colLower_[i];
    double upper = lp_.colUpper_[i];
    double xpert = (fabs(simplexinfo_.workCost_[i]) + 1) * base *
                   simplexinfo_.dual_simplex_cost_perturbation_multiplier *
                   (1 + simplexinfo_.numTotRandomValue_[i]);
    const double previous_cost = simplexinfo_.workCost_[i];
    if (lower <= -kHighsInf && upper >= kHighsInf) {
      // Free - no perturb
    } else if (upper >= kHighsInf) {  // Lower
      simplexinfo_.workCost_[i] += xpert;
    } else if (lower <= -kHighsInf) {  // Upper
      simplexinfo_.workCost_[i] += -xpert;
    } else if (lower != upper) {  // Boxed
      simplexinfo_.workCost_[i] += (simplexinfo_.workCost_[i] >= 0) ? xpert : -xpert;
    } else {
      // Fixed - no perturb
    }
    if (analysis_.analyse_simplex_data) {
      const double perturbation1 = fabs(simplexinfo_.workCost_[i] - previous_cost);
      if (perturbation1)
        updateValueDistribution(perturbation1,
                                analysis_.cost_perturbation1_distribution);
    }
  }
  for (HighsInt i = lp_.numCol_; i < num_tot; i++) {
    double perturbation2 = (0.5 - simplexinfo_.numTotRandomValue_[i]) *
                           simplexinfo_.dual_simplex_cost_perturbation_multiplier *
                           1e-12;
    simplexinfo_.workCost_[i] += perturbation2;
    if (analysis_.analyse_simplex_data) {
      perturbation2 = fabs(perturbation2);
      updateValueDistribution(perturbation2,
                              analysis_.cost_perturbation2_distribution);
    }
  }
  simplexinfo_.costs_perturbed = 1;
}

void Primal::initialiseBound(const SimplexAlgorithm algorithm, const HighsInt solve_phase, const bool perturb) {
  initialiseLpColBound();
  initialiseLpRowBound();
  simplexinfo_.bounds_perturbed = 0;
  // Primal simplex bounds are either from the LP or perturbed
  if (algorithm == SimplexAlgorithm::kPrimal) {
    if (!perturb || simplexinfo_.primal_simplex_bound_perturbation_multiplier == 0)
      return;
    // Perturb the bounds
    // Determine the smallest and largest finite lower/upper bounds
    HighsInt num_col = lp_.numCol_;
    HighsInt num_row = lp_.numRow_;
    HighsInt num_tot = num_col + num_row;
    double min_abs_lower = kHighsInf;
    double max_abs_lower = -1;
    double min_abs_upper = kHighsInf;
    double max_abs_upper = -1;
    for (HighsInt iVar = 0; iVar < num_tot; iVar++) {
      double abs_lower = fabs(simplexinfo_.workLower_[iVar]);
      double abs_upper = fabs(simplexinfo_.workUpper_[iVar]);
      if (abs_lower && abs_lower < kHighsInf) {
        min_abs_lower = min(abs_lower, min_abs_lower);
        max_abs_lower = max(abs_lower, max_abs_lower);
      }
      if (abs_upper && abs_upper < kHighsInf) {
        min_abs_upper = min(abs_upper, min_abs_upper);
        max_abs_upper = max(abs_upper, max_abs_upper);
      }
    }
    // printf(
    //     "Nonzero finite lower bounds in [%9.4g, %9.4g]; upper bounds in "
    //     "[%9.4g, %9.4g]\n",
    //     min_abs_lower, max_abs_lower, min_abs_upper, max_abs_upper);

    const double base =
            simplexinfo_.primal_simplex_bound_perturbation_multiplier * 5e-7;
    for (HighsInt iVar = 0; iVar < num_tot; iVar++) {
      double lower = simplexinfo_.workLower_[iVar];
      double upper = simplexinfo_.workUpper_[iVar];
      const bool fixed = lower == upper;
      // Don't perturb bounds of nonbasic fixed variables as they stay nonbasic
      if (simplexbasis_.nonbasicFlag_[iVar] == kNonbasicFlagTrue && fixed) continue;
      double random_value = simplexinfo_.numTotRandomValue_[iVar];
      if (lower > -kHighsInf) {
        if (lower < -1) {
          lower -= random_value * base * (-lower);
        } else if (lower < 1) {
          lower -= random_value * base;
        } else {
          lower -= random_value * base * lower;
        }
        simplexinfo_.workLower_[iVar] = lower;
      }
      if (upper < kHighsInf) {
        if (upper < -1) {
          upper += random_value * base * (-upper);
        } else if (upper < 1) {
          upper += random_value * base;
        } else {
          upper += random_value * base * upper;
        }
        simplexinfo_.workUpper_[iVar] = upper;
      }
      simplexinfo_.workRange_[iVar] = simplexinfo_.workUpper_[iVar] - simplexinfo_.workLower_[iVar];
      if (simplexbasis_.nonbasicFlag_[iVar] == kNonbasicFlagFalse) continue;
      // Set values of nonbasic variables
      if (simplexbasis_.nonbasicMove_[iVar] > 0) {
        simplexinfo_.workValue_[iVar] = lower;
      } else if (simplexbasis_.nonbasicMove_[iVar] < 0) {
        simplexinfo_.workValue_[iVar] = upper;
      }
    }
    for (HighsInt iRow = 0; iRow < num_row; iRow++) {
      HighsInt iVar = simplexbasis_.basicIndex_[iRow];
      simplexinfo_.baseLower_[iRow] = simplexinfo_.workLower_[iVar];
      simplexinfo_.baseUpper_[iRow] = simplexinfo_.workUpper_[iVar];
    }
    simplexinfo_.bounds_perturbed = 1;
    return;
  }
  // Dual simplex bounds are either from the LP or set to special values in
  // phase
  // 1
  assert(algorithm == SimplexAlgorithm::kDual);
  if (solve_phase == kSolvePhase2) return;

  // The dual objective is the sum of products of primal and dual
  // values for nonbasic variables. For dual simplex phase 1, the
  // primal bounds are set so that when the dual value is feasible, the
  // primal value is set to zero. Otherwise the value is +1/-1
  // according to the required sign of the dual, except for free
  // variables, where the bounds are [-1000, 1000]. Hence the dual
  // objective is the negation of the sum of infeasibilities, unless there are
  // free In Phase 1: change to dual phase 1 bound.
  const double inf = kHighsInf;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  for (HighsInt iCol = 0; iCol < num_tot; iCol++) {
    if (simplexinfo_.workLower_[iCol] == -inf && simplexinfo_.workUpper_[iCol] == inf) {
      // Don't change for row variables: they should never become
      // nonbasic when starting from a logical basis, and no crash
      // should make a free row nonbasic, but could an advanced basis
      // make a free row nonbasic.
      // But what it it happened?
      if (iCol >= lp_.numCol_) continue;
      simplexinfo_.workLower_[iCol] = -1000,
              simplexinfo_.workUpper_[iCol] = 1000;  // FREE
    } else if (simplexinfo_.workLower_[iCol] == -inf) {
      simplexinfo_.workLower_[iCol] = -1,
              simplexinfo_.workUpper_[iCol] = 0;  // UPPER
    } else if (simplexinfo_.workUpper_[iCol] == inf) {
      simplexinfo_.workLower_[iCol] = 0,
              simplexinfo_.workUpper_[iCol] = 1;  // LOWER
    } else {
      simplexinfo_.workLower_[iCol] = 0,
              simplexinfo_.workUpper_[iCol] = 0;  // BOXED or FIXED
    }
    simplexinfo_.workRange_[iCol] = simplexinfo_.workUpper_[iCol] - simplexinfo_.workLower_[iCol];
  }
}

void Primal::initialiseLpColBound() {
  for (HighsInt iCol = 0; iCol < lp_.numCol_; iCol++) {
    simplexinfo_.workLower_[iCol] = lp_.colLower_[iCol];
    simplexinfo_.workUpper_[iCol] = lp_.colUpper_[iCol];
    simplexinfo_.workRange_[iCol] = simplexinfo_.workUpper_[iCol] - simplexinfo_.workLower_[iCol];
    simplexinfo_.workLowerShift_[iCol] = 0;
    simplexinfo_.workUpperShift_[iCol] = 0;
  }
}

void Primal::initialiseLpRowBound() {
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++) {
    HighsInt iCol = lp_.numCol_ + iRow;
    simplexinfo_.workLower_[iCol] = -lp_.rowUpper_[iRow];
    simplexinfo_.workUpper_[iCol] = -lp_.rowLower_[iRow];
    simplexinfo_.workRange_[iCol] = simplexinfo_.workUpper_[iCol] - simplexinfo_.workLower_[iCol];
    simplexinfo_.workLowerShift_[iCol] = 0;
    simplexinfo_.workUpperShift_[iCol] = 0;
  }
}

void Primal::initialiseLpColCost() {
  for (HighsInt iCol = 0; iCol < lp_.numCol_; iCol++) {
    simplexinfo_.workCost_[iCol] = (HighsInt) lp_.sense_ * lp_.colCost_[iCol];
    simplexinfo_.workShift_[iCol] = 0;
  }
}

void Primal::initialiseLpRowCost() {
  for (HighsInt iCol = lp_.numCol_; iCol < lp_.numCol_ + lp_.numRow_; iCol++) {
    simplexinfo_.workCost_[iCol] = 0;
    simplexinfo_.workShift_[iCol] = 0;
  }
}

void Primal::initialiseNonbasicValueAndMove() {
  // Initialise workValue and nonbasicMove from nonbasicFlag and
  // bounds, except for boxed variables when nonbasicMove is used to
  // set workValue=workLower/workUpper
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  for (HighsInt iVar = 0; iVar < num_tot; iVar++) {
    if (!simplexbasis_.nonbasicFlag_[iVar]) {
      // Basic variable
      simplexbasis_.nonbasicMove_[iVar] = kNonbasicMoveZe;
      continue;
    }
    // Nonbasic variable
    const double lower = simplexinfo_.workLower_[iVar];
    const double upper = simplexinfo_.workUpper_[iVar];
    const HighsInt original_move = simplexbasis_.nonbasicMove_[iVar];
    double value;
    HighsInt move = kIllegalMoveValue;
    if (lower == upper) {
      // Fixed
      value = lower;
      move = kNonbasicMoveZe;
    } else if (!highs_isInfinity(-lower)) {
      // Finite lower bound so boxed or lower
      if (!highs_isInfinity(upper)) {
        // Finite upper bound so boxed
        if (original_move == kNonbasicMoveUp) {
          // Set at lower
          value = lower;
          move = kNonbasicMoveUp;
        } else if (original_move == kNonbasicMoveDn) {
          // Set at upper
          value = upper;
          move = kNonbasicMoveDn;
        } else {
          // Invalid nonbasicMove: correct and set value at lower
          value = lower;
          move = kNonbasicMoveUp;
        }
      } else {
        // Lower
        value = lower;
        move = kNonbasicMoveUp;
      }
    } else if (!highs_isInfinity(upper)) {
      // Upper
      value = upper;
      move = kNonbasicMoveDn;
    } else {
      // FREE
      value = 0;
      move = kNonbasicMoveZe;
    }
    assert(move != kIllegalMoveValue);
    simplexbasis_.nonbasicMove_[iVar] = move;
    simplexinfo_.workValue_[iVar] = value;
  }
}

void Primal::computePrimal() {
  analysis_.simplexTimerStart(ComputePrimalClock);
  const HighsInt num_row = lp_.numRow_;
  const HighsInt num_col = lp_.numCol_;
  // Setup a local buffer for the values of basic variables
  HVector primal_col;
  primal_col.setup(num_row);
  primal_col.clear();
  for (HighsInt i = 0; i < num_col + num_row; i++) {
    if (simplexbasis_.nonbasicFlag_[i] && simplexinfo_.workValue_[i] != 0) {
      matrix_.collect_aj(primal_col, i, simplexinfo_.workValue_[i]);
    }
  }
  // It's possible that the buffer has no nonzeros, so performing
  // FTRAN is unnecessary. Not much of a saving, but the zero density
  // looks odd in the analysis!
  if (primal_col.count) {
    factor_.ftran(primal_col, analysis_.primal_col_density, analysis_.pointer_serial_factor_clocks);
    const double local_primal_col_density = (double) primal_col.count / num_row;
    analysis_.updateOperationResultDensity(local_primal_col_density,
                                           analysis_.primal_col_density);
//    updateOperationResultDensity(local_primal_col_density,
//                                 simplexinfo_.primal_col_density);
  }
  for (HighsInt i = 0; i < num_row; i++) {
    HighsInt iCol = simplexbasis_.basicIndex_[i];
    simplexinfo_.baseValue_[i] = -primal_col.array[i];
    simplexinfo_.baseLower_[i] = simplexinfo_.workLower_[iCol];
    simplexinfo_.baseUpper_[i] = simplexinfo_.workUpper_[iCol];
  }
  // Indicate that the primal infeasiblility information isn't known
  simplexinfo_.num_primal_infeasibility = kHighsIllegalInfeasibilityCount;
  simplexinfo_.max_primal_infeasibility = kHighsIllegalInfeasibilityMeasure;
  simplexinfo_.sum_primal_infeasibility = kHighsIllegalInfeasibilityMeasure;

  // Now have basic primals
  status_.has_basic_primal_values = true;
  analysis_.simplexTimerStop(ComputePrimalClock);
}

void Primal::computeDual() {
  analysis_.simplexTimerStart(ComputeDualClock);
  // Create a local buffer for the pi vector
  HVector dual_col;
  dual_col.setup(lp_.numRow_);
  dual_col.clear();
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++) {
    const double value = simplexinfo_.workCost_[simplexbasis_.basicIndex_[iRow]] +
                         simplexinfo_.workShift_[simplexbasis_.basicIndex_[iRow]];
    if (value) {
      dual_col.index[dual_col.count++] = iRow;
      dual_col.array[iRow] = value;
    }
  }
  // Copy the costs in case the basic costs are all zero
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  for (HighsInt i = 0; i < num_tot; i++)
    simplexinfo_.workDual_[i] = simplexinfo_.workCost_[i];

  if (dual_col.count) {
    fullBtran(dual_col);
    // Create a local buffer for the values of reduced costs
    HVector dual_row;
    dual_row.setup(lp_.numCol_);
    fullPrice(dual_col, dual_row);
    for (HighsInt i = 0; i < lp_.numCol_; i++)
      simplexinfo_.workDual_[i] -= dual_row.array[i];
    for (HighsInt i = lp_.numCol_; i < num_tot; i++)
      simplexinfo_.workDual_[i] -= dual_col.array[i - lp_.numCol_];
  }
  // Indicate that the dual infeasiblility information isn't known
  simplexinfo_.num_dual_infeasibility = kHighsIllegalInfeasibilityCount;
  simplexinfo_.max_dual_infeasibility = kHighsIllegalInfeasibilityMeasure;
  simplexinfo_.sum_dual_infeasibility = kHighsIllegalInfeasibilityMeasure;

  // Now have nonbasic duals
  status_.has_nonbasic_dual_values = true;
  analysis_.simplexTimerStop(ComputeDualClock);
}

void Primal::computeSimplexPrimalInfeasible() {
  // Computes num/max/sum of primal infeasibliities according to the
  // simplex bounds. This is used to determine optimality in dual
  // phase 1 and dual phase 2, albeit using different bounds in
  // workLower/Upper.
  analysis_.simplexTimerStart(ComputePrIfsClock);
  const double scaled_primal_feasibility_tolerance = options_.primal_feasibility_tolerance;
  HighsInt &num_primal_infeasibility = simplexinfo_.num_primal_infeasibility;
  double &max_primal_infeasibility = simplexinfo_.max_primal_infeasibility;
  double &sum_primal_infeasibility = simplexinfo_.sum_primal_infeasibility;
  num_primal_infeasibility = 0;
  max_primal_infeasibility = 0;
  sum_primal_infeasibility = 0;

  for (HighsInt i = 0; i < lp_.numCol_ + lp_.numRow_; i++) {
    if (simplexbasis_.nonbasicFlag_[i]) {
      // Nonbasic column
      double value = simplexinfo_.workValue_[i];
      double lower = simplexinfo_.workLower_[i];
      double upper = simplexinfo_.workUpper_[i];
      // @primal_infeasibility calculation
      double primal_infeasibility = 0;
      if (value < lower - scaled_primal_feasibility_tolerance) {
        primal_infeasibility = lower - value;
      } else if (value > upper + scaled_primal_feasibility_tolerance) {
        primal_infeasibility = value - upper;
      }
      if (primal_infeasibility > 0) {
        if (primal_infeasibility > scaled_primal_feasibility_tolerance)
          num_primal_infeasibility++;
        max_primal_infeasibility = std::max(primal_infeasibility, max_primal_infeasibility);
        sum_primal_infeasibility += primal_infeasibility;
      }
    }
  }
  for (HighsInt i = 0; i < lp_.numRow_; i++) {
    // Basic variable
    double value = simplexinfo_.baseValue_[i];
    double lower = simplexinfo_.baseLower_[i];
    double upper = simplexinfo_.baseUpper_[i];
    // @primal_infeasibility calculation
    double primal_infeasibility = 0;
    if (value < lower - scaled_primal_feasibility_tolerance) {
      primal_infeasibility = lower - value;
    } else if (value > upper + scaled_primal_feasibility_tolerance) {
      primal_infeasibility = value - upper;
    }
    if (primal_infeasibility > 0) {
      if (primal_infeasibility > scaled_primal_feasibility_tolerance)
        num_primal_infeasibility++;
      max_primal_infeasibility = std::max(primal_infeasibility, max_primal_infeasibility);
      sum_primal_infeasibility += primal_infeasibility;
    }
  }
  analysis_.simplexTimerStop(ComputePrIfsClock);
}

void Primal::computeSimplexDualInfeasible() {
  analysis_.simplexTimerStart(ComputeDuIfsClock);
  // Computes num/max/sum of dual infeasibilities in phase 1 and phase
  // 2 according to nonbasicMove. The bounds are only used to identify
  // free variables. Fixed variables are assumed to have
  // nonbasicMove=0 so that no dual infeasibility is counted for them.
  const double scaled_dual_feasibility_tolerance = options_.dual_feasibility_tolerance;
  HighsInt &num_dual_infeasibility = simplexinfo_.num_dual_infeasibility;
  double &max_dual_infeasibility = simplexinfo_.max_dual_infeasibility;
  double &sum_dual_infeasibility = simplexinfo_.sum_dual_infeasibility;
  num_dual_infeasibility = 0;
  max_dual_infeasibility = 0;
  sum_dual_infeasibility = 0;

  for (HighsInt iCol = 0; iCol < lp_.numCol_ + lp_.numRow_; iCol++) {
    if (!simplexbasis_.nonbasicFlag_[iCol]) continue;
    // Nonbasic column
    const double dual = simplexinfo_.workDual_[iCol];
    const double lower = simplexinfo_.workLower_[iCol];
    const double upper = simplexinfo_.workUpper_[iCol];
    double dual_infeasibility = 0;
    if (highs_isInfinity(-lower) && highs_isInfinity(upper)) {
      // Free: any nonzero dual value is infeasible
      dual_infeasibility = fabs(dual);
    } else {
      // Not free: any dual infeasibility is given by the dual value
      // signed by nonbasicMove
      dual_infeasibility = -simplexbasis_.nonbasicMove_[iCol] * dual;
    }
    if (dual_infeasibility > 0) {
      if (dual_infeasibility >= scaled_dual_feasibility_tolerance)
        num_dual_infeasibility++;
      max_dual_infeasibility = std::max(dual_infeasibility, max_dual_infeasibility);
      sum_dual_infeasibility += dual_infeasibility;
    }
  }
  analysis_.simplexTimerStop(ComputeDuIfsClock);
}

void Primal::computePrimalObjectiveValue() {
  analysis_.simplexTimerStart(ComputePrObjClock);
  simplexinfo_.primal_objective_value = 0;
  for (HighsInt iRow = 0; iRow < lp_.numRow_; iRow++) {
    HighsInt iVar = simplexbasis_.basicIndex_[iRow];
    if (iVar < lp_.numCol_) {
      simplexinfo_.primal_objective_value += simplexinfo_.baseValue_[iRow] * lp_.colCost_[iVar];
    }
  }
  for (HighsInt iCol = 0; iCol < lp_.numCol_; iCol++) {
    if (simplexbasis_.nonbasicFlag_[iCol])
      simplexinfo_.primal_objective_value += simplexinfo_.workValue_[iCol] * lp_.colCost_[iCol];
  }
  simplexinfo_.primal_objective_value *= cost_scale_;
  // Objective value calculation is done using primal values and
  // original costs so offset is vanilla
  simplexinfo_.primal_objective_value += lp_.offset_;
  // Now have primal objective value
  status_.has_primal_objective_value = true;
  analysis_.simplexTimerStop(ComputePrObjClock);
}

void Primal::computeDualObjectiveValue(const HighsInt phase) {
  analysis_.simplexTimerStart(ComputeDuObjClock);
  simplexinfo_.dual_objective_value = 0;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  for (HighsInt iCol = 0; iCol < num_tot; iCol++) {
    if (simplexbasis_.nonbasicFlag_[iCol]) {
      const double term = simplexinfo_.workValue_[iCol] * simplexinfo_.workDual_[iCol];
      if (term) {
        simplexinfo_.dual_objective_value += simplexinfo_.workValue_[iCol] * simplexinfo_.workDual_[iCol];
      }
    }
  }
  simplexinfo_.dual_objective_value *= cost_scale_;
  if (phase != 1) {
    // In phase 1 the dual objective has no objective
    // shift. Otherwise, if minimizing the shift is added. If
    // maximizing, workCost (and hence workDual) are negated, so the
    // shift is subtracted. Hence the shift is added according to the
    // sign implied by sense_
    simplexinfo_.dual_objective_value += ((HighsInt) lp_.sense_) * lp_.offset_;
  }
  // Now have dual objective value
  status_.has_dual_objective_value = true;
  analysis_.simplexTimerStop(ComputeDuObjClock);
}

HighsInt Primal::computeFactor() {
  if (!status_.has_factor_arrays) {
    // todo @ Julian: this fails on glass4
    assert(simplexinfo_.factor_pivot_threshold >= options_.factor_pivot_threshold);
    factor_.setup(lp_.numCol_, lp_.numRow_, &lp_.Astart_[0], &lp_.Aindex_[0],
                  &lp_.Avalue_[0], &simplexbasis_.basicIndex_[0],
                  simplexinfo_.factor_pivot_threshold, options_.factor_pivot_tolerance,
                  options_.highs_debug_level, options_.output_flag,
                  options_.log_file_stream, options_.log_to_console,
                  options_.log_dev_level);
    status_.has_factor_arrays = true;
  }
  analysis_.simplexTimerStart(InvertClock);
  HighsTimerClock *factor_timer_clock_pointer = NULL;
  if (analysis_.analyse_factor_time) {
    HighsInt thread_id = 0;
    factor_timer_clock_pointer = analysis_.getThreadFactorTimerClockPtr(thread_id);
  }
  const HighsInt rank_deficiency = factor_.build(factor_timer_clock_pointer);
  if (analysis_.analyse_factor_data) analysis_.updateInvertFormData(factor_);

  // Now have a representation of B^{-1}, and it is fresh!
  status_.has_invert = true;
  status_.has_fresh_invert = true;
  // Set the update count to zero since the corrected invertible
  // representation may be used for an initial basis. In any case the
  // number of updates shouldn't be positive
  simplexinfo_.update_count = 0;

  analysis_.simplexTimerStop(InvertClock);
  return rank_deficiency;
}

HighsStatus Primal::setBasis() {
  // Set up nonbasicFlag and basicIndex for a logical basis
  const HighsInt num_col = lp_.numCol_;
  const HighsInt num_row = lp_.numRow_;
  const HighsInt num_tot = num_col + num_row;
  simplexbasis_.nonbasicFlag_.resize(num_tot);
  simplexbasis_.nonbasicMove_.resize(num_tot);
  simplexbasis_.basicIndex_.resize(num_row);
  for (HighsInt iCol = 0; iCol < num_col; iCol++) {
    simplexbasis_.nonbasicFlag_[iCol] = kNonbasicFlagTrue;
    double lower = lp_.colLower_[iCol];
    double upper = lp_.colUpper_[iCol];
    HighsInt move = kIllegalMoveValue;
    double value;
    if (lower == upper) {
      // Fixed
      value = lower;
      move = kNonbasicMoveZe;
    } else if (!highs_isInfinity(-lower)) {
      // Finite lower bound so boxed or lower
      if (!highs_isInfinity(upper)) {
        // Finite upper bound so boxed. Set to bound of LP that is closer to
        // zero
        if (move == kIllegalMoveValue) {
          if (fabs(lower) < fabs(upper)) {
            value = lower;
            move = kNonbasicMoveUp;
          } else {
            value = upper;
            move = kNonbasicMoveDn;
          }
        }
      } else {
        // Lower (since upper bound is infinite)
        value = lower;
        move = kNonbasicMoveUp;
      }
    } else if (!highs_isInfinity(upper)) {
      // Upper
      value = upper;
      move = kNonbasicMoveDn;
    } else {
      // FREE
      value = 0;
      move = kNonbasicMoveZe;
    }
    assert(move != kIllegalMoveValue);
    simplexbasis_.nonbasicMove_[iCol] = move;
    simplexinfo_.workValue_[iCol] = value;
  }
  for (HighsInt iRow = 0; iRow < num_row; iRow++) {
    HighsInt iVar = num_col + iRow;
    simplexbasis_.nonbasicFlag_[iVar] = kNonbasicFlagFalse;
    simplexbasis_.basicIndex_[iRow] = iVar;
    simplexbasis_.nonbasicMove_[iVar] = kNonbasicMoveZe;
  }
  simplexinfo_.num_basic_logicals = num_row;
  status_.has_basis = true;
  return HighsStatus::kOk;
}

void Primal::setNonbasicMove() {
  const bool have_solution = false;
  // Don't have a simplex basis since nonbasicMove is not set up.

  // Assign nonbasicMove using as much information as is available
  double lower;
  double upper;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  simplexbasis_.nonbasicMove_.resize(num_tot);

  for (HighsInt iVar = 0; iVar < num_tot; iVar++) {
    if (!simplexbasis_.nonbasicFlag_[iVar]) {
      // Basic variable
      simplexbasis_.nonbasicMove_[iVar] = kNonbasicMoveZe;
      continue;
    }
    // Nonbasic variable
    if (iVar < lp_.numCol_) {
      lower = lp_.colLower_[iVar];
      upper = lp_.colUpper_[iVar];
    } else {
      HighsInt iRow = iVar - lp_.numCol_;
      lower = -lp_.rowUpper_[iRow];
      upper = -lp_.rowLower_[iRow];
    }
    HighsInt move = kIllegalMoveValue;
    if (lower == upper) {
      // Fixed
      move = kNonbasicMoveZe;
    } else if (!highs_isInfinity(-lower)) {
      // Finite lower bound so boxed or lower
      if (!highs_isInfinity(upper)) {
        // Finite upper bound so boxed
        //
        // Determine the bound to set the value to according to, in order of
        // priority
        //
        // 1. Any solution value
        if (have_solution) {
          double midpoint = 0.5 * (lower + upper);
          double value = simplexinfo_.workValue_[iVar];
          if (value < midpoint) {
            move = kNonbasicMoveUp;
          } else {
            move = kNonbasicMoveDn;
          }
        }
        // 2. Bound of original LP that is closer to zero
        if (move == kIllegalMoveValue) {
          if (fabs(lower) < fabs(upper)) {
            move = kNonbasicMoveUp;
          } else {
            move = kNonbasicMoveDn;
          }
        }
      } else {
        // Lower (since upper bound is infinite)
        move = kNonbasicMoveUp;
      }
    } else if (!highs_isInfinity(upper)) {
      // Upper
      move = kNonbasicMoveDn;
    } else {
      // FREE
      move = kNonbasicMoveZe;
    }
    assert(move != kIllegalMoveValue);
    simplexbasis_.nonbasicMove_[iVar] = move;
  }
}

void Primal::fullBtran(HVector &buffer) {
  // Performs BTRAN on the buffer supplied. Make sure that
  // buffer.count is large (>lp_.numRow_ to be sure) rather
  // than 0 if the indices of the RHS (and true value of buffer.count)
  // isn't known.
  analysis_.simplexTimerStart(BtranFullClock);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordBefore(ANALYSIS_OPERATION_TYPE_BTRAN_FULL, buffer,
                                    analysis_.dual_col_density);
  factor_.btran(buffer, analysis_.dual_col_density,
                analysis_.pointer_serial_factor_clocks);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordAfter(ANALYSIS_OPERATION_TYPE_BTRAN_FULL, buffer);
  const double local_dual_col_density = (double) buffer.count / lp_.numRow_;
  analysis_.updateOperationResultDensity(local_dual_col_density,
                                         analysis_.dual_col_density);
//  updateOperationResultDensity(local_dual_col_density, simplexinfo_.dual_col_density);
  analysis_.simplexTimerStop(BtranFullClock);
}

void Primal::fullPrice(const HVector &full_col, HVector &full_row) {
  analysis_.simplexTimerStart(PriceFullClock);
  full_row.clear();
  if (analysis_.analyse_simplex_data) {
    const double historical_density_for_non_hypersparse_operation = 1;
    analysis_.operationRecordBefore(
            ANALYSIS_OPERATION_TYPE_PRICE_FULL, full_col,
            historical_density_for_non_hypersparse_operation);
  }
  matrix_.priceByColumn(full_row, full_col);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordAfter(ANALYSIS_OPERATION_TYPE_PRICE_FULL,
                                   full_row);
  analysis_.simplexTimerStop(PriceFullClock);
}

bool Primal::isUnconstrainedLp() {
  bool is_unconstrained_lp = lp_.numRow_ <= 0;
  if (is_unconstrained_lp)
    highsLogUser(
            options_.log_options, HighsLogType::kError,
            "HEkkDual::solve called for LP with non-positive (%" HIGHSINT_FORMAT
            ") number of constraints\n",
            lp_.numRow_);
  assert(!is_unconstrained_lp);
  return is_unconstrained_lp;
}

bool Primal::bailoutOnTimeIterations() {
  if (solve_bailout_) {
    // Bailout has already been decided: check that it's for one of these
    // reasons
    assert(model_status_ == HighsModelStatus::kTimeLimit ||
           model_status_ == HighsModelStatus::kIterationLimit ||
           model_status_ == HighsModelStatus::kObjectiveBound ||
           model_status_ == HighsModelStatus::kObjectiveTarget);
  } else if (timer_.readRunHighsClock() > options_.time_limit) {
    solve_bailout_ = true;
    model_status_ = HighsModelStatus::kTimeLimit;
  } else if (iteration_count_ >= options_.simplex_iteration_limit) {
    solve_bailout_ = true;
    model_status_ = HighsModelStatus::kIterationLimit;
  }
  return solve_bailout_;
}

void Primal::pivotColumnFtran(const HighsInt iCol, HVector &col_aq) {
  analysis_.simplexTimerStart(FtranClock);
  col_aq.clear();
  col_aq.packFlag = true;
  matrix_.collect_aj(col_aq, iCol, 1);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordBefore(ANALYSIS_OPERATION_TYPE_FTRAN, col_aq,
                                    analysis_.col_aq_density);
  factor_.ftran(col_aq, analysis_.col_aq_density, analysis_.pointer_serial_factor_clocks);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordAfter(ANALYSIS_OPERATION_TYPE_FTRAN, col_aq);
  HighsInt num_row = lp_.numRow_;
  const double local_col_aq_density = (double) col_aq.count / num_row;
  analysis_.updateOperationResultDensity(local_col_aq_density, analysis_.col_aq_density);
//  updateOperationResultDensity(local_col_aq_density, simplexinfo_.col_aq_density);
  analysis_.simplexTimerStop(FtranClock);
}

double Primal::computeDualForTableauColumn(const HighsInt iVar, const HVector &tableau_column) {
  const vector<double> &workCost = simplexinfo_.workCost_;
  const vector<HighsInt> &basicIndex = simplexbasis_.basicIndex_;

  double dual = simplexinfo_.workCost_[iVar];
  for (HighsInt i = 0; i < tableau_column.count; i++) {
    HighsInt iRow = tableau_column.index[i];
    dual -= tableau_column.array[iRow] * workCost[basicIndex[iRow]];
  }
  return dual;
}

void Primal::unitBtran(const HighsInt iRow, HVector &row_ep) {
  analysis_.simplexTimerStart(BtranClock);
  row_ep.clear();
  row_ep.count = 1;
  row_ep.index[0] = iRow;
  row_ep.array[iRow] = 1;
  row_ep.packFlag = true;
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordBefore(ANALYSIS_OPERATION_TYPE_BTRAN_EP, row_ep,
                                    analysis_.row_ep_density);
  factor_.btran(row_ep, analysis_.row_ep_density,
                analysis_.pointer_serial_factor_clocks);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordAfter(ANALYSIS_OPERATION_TYPE_BTRAN_EP, row_ep);
  HighsInt num_row = lp_.numRow_;
  const double local_row_ep_density = (double) row_ep.count / num_row;
  analysis_.updateOperationResultDensity(local_row_ep_density,
                                         analysis_.row_ep_density);
//  updateOperationResultDensity(local_row_ep_density, simplexinfo_.row_ep_density);
  analysis_.simplexTimerStop(BtranClock);
}

void Primal::tableauRowPrice(const HVector &row_ep, HVector &row_ap) {
  analysis_.simplexTimerStart(PriceClock);
  const HighsInt solver_num_row = lp_.numRow_;
  const HighsInt solver_num_col = lp_.numCol_;
  const double local_density = 1.0 * row_ep.count / solver_num_row;
  bool use_col_price;
  bool use_row_price_w_switch;
  choosePriceTechnique(simplexinfo_.price_strategy, local_density, use_col_price,
                       use_row_price_w_switch);
  if (analysis_.analyse_simplex_data) {
    if (use_col_price) {
      const double historical_density_for_non_hypersparse_operation = 1;
      analysis_.operationRecordBefore(
              ANALYSIS_OPERATION_TYPE_PRICE_AP, row_ep,
              historical_density_for_non_hypersparse_operation);
      analysis_.num_col_price++;
    } else if (use_row_price_w_switch) {
      analysis_.operationRecordBefore(ANALYSIS_OPERATION_TYPE_PRICE_AP, row_ep,
                                      analysis_.row_ep_density);
      analysis_.num_row_price_with_switch++;
    } else {
      analysis_.operationRecordBefore(ANALYSIS_OPERATION_TYPE_PRICE_AP, row_ep,
                                      analysis_.row_ep_density);
      analysis_.num_row_price++;
    }
  }
  row_ap.clear();
  if (use_col_price) {
    // Perform column-wise PRICE
    matrix_.priceByColumn(row_ap, row_ep);
  } else if (use_row_price_w_switch) {
    // Perform hyper-sparse row-wise PRICE, but switch if the density of row_ap
    // becomes extreme
    const double switch_density = matrix_.hyperPRICE;
    matrix_.priceByRowSparseResultWithSwitch(
            row_ap, row_ep, analysis_.row_ap_density, 0, switch_density);
  } else {
    // Perform hyper-sparse row-wise PRICE
    matrix_.priceByRowSparseResult(row_ap, row_ep);
  }
  if (use_col_price) {
    // Column-wise PRICE computes components corresponding to basic
    // variables, so zero these by exploiting the fact that, for basic
    // variables, nonbasicFlag[*]=0
    const int8_t *nonbasicFlag = &simplexbasis_.nonbasicFlag_[0];
    for (HighsInt iCol = 0; iCol < solver_num_col; iCol++)
      row_ap.array[iCol] *= nonbasicFlag[iCol];
  }
  // Update the record of average row_ap density
  const double local_row_ap_density = (double) row_ap.count / solver_num_col;
  analysis_.updateOperationResultDensity(local_row_ap_density,
                                         analysis_.row_ap_density);
//  updateOperationResultDensity(local_row_ap_density, simplexinfo_.row_ap_density);
  if (analysis_.analyse_simplex_data)
    analysis_.operationRecordAfter(ANALYSIS_OPERATION_TYPE_PRICE_AP, row_ap);
  analysis_.simplexTimerStop(PriceClock);
}

void Primal::invalidatePrimalMaxSumInfeasibilityRecord() {
  simplexinfo_.max_primal_infeasibility = kHighsIllegalInfeasibilityMeasure;
  simplexinfo_.sum_primal_infeasibility = kHighsIllegalInfeasibilityMeasure;
}

void Primal::invalidatePrimalInfeasibilityRecord() {
  simplexinfo_.num_primal_infeasibility = kHighsIllegalInfeasibilityCount;
  invalidatePrimalMaxSumInfeasibilityRecord();
}

void Primal::invalidateDualMaxSumInfeasibilityRecord() {
  simplexinfo_.max_dual_infeasibility = kHighsIllegalInfeasibilityMeasure;
  simplexinfo_.sum_dual_infeasibility = kHighsIllegalInfeasibilityMeasure;
}

void Primal::invalidateDualInfeasibilityRecord() {
  simplexinfo_.num_dual_infeasibility = kHighsIllegalInfeasibilityCount;
  invalidateDualMaxSumInfeasibilityRecord();
}

bool Primal::sparseLoopStyle(const HighsInt count, const HighsInt dim, HighsInt &to_entry) {
  // Parameter to decide whether to use just the values in a HVector, or
  // use the indices of their nonzeros
  const double density_for_indexing = 0.4;
  const bool use_indices = count >= 0 && count < density_for_indexing * dim;
  if (use_indices) {
    to_entry = count;
  } else {
    to_entry = dim;
  }
  return use_indices;
}

// The major model updates. Factor calls factor_.update; Matrix
// calls matrix_.update; updatePivots does everything---and is
// called from the likes of HDual::updatePivots
void Primal::updateFactor(HVector *column, HVector *row_ep, HighsInt *iRow, HighsInt *hint) {
  analysis_.simplexTimerStart(UpdateFactorClock);
  factor_.update(column, row_ep, iRow, hint);
  // Now have a representation of B^{-1}, but it is not fresh
  status_.has_invert = true;
  if (simplexinfo_.update_count >= simplexinfo_.update_limit)
    *hint = kRebuildReasonUpdateLimitReached;

  // Determine whether to reinvert based on the synthetic clock
  bool reinvert_syntheticClock = total_synthetic_tick_ >= build_synthetic_tick_;
  const bool performed_min_updates = simplexinfo_.update_count >= kSyntheticTickReinversionMinUpdateCount;
//  if (reinvert_syntheticClock && performed_min_updates)
//    *hint = kRebuildReasonSyntheticClockSaysInvert;

  analysis_.simplexTimerStop(UpdateFactorClock);
}

void Primal::updatePivots(const HighsInt variable_in, const HighsInt row_out, const HighsInt move_out) {
  analysis_.simplexTimerStart(UpdatePivotsClock);
  HighsInt variable_out = simplexbasis_.basicIndex_[row_out];

  // Incoming variable
  simplexbasis_.basicIndex_[row_out] = variable_in;
  simplexbasis_.nonbasicFlag_[variable_in] = 0;
  simplexbasis_.nonbasicMove_[variable_in] = 0;
  simplexinfo_.baseLower_[row_out] = simplexinfo_.workLower_[variable_in];
  simplexinfo_.baseUpper_[row_out] = simplexinfo_.workUpper_[variable_in];

  // Outgoing variable
  simplexbasis_.nonbasicFlag_[variable_out] = 1;
  if (simplexinfo_.workLower_[variable_out] == simplexinfo_.workUpper_[variable_out]) {
    simplexinfo_.workValue_[variable_out] = simplexinfo_.workLower_[variable_out];
    simplexbasis_.nonbasicMove_[variable_out] = 0;
  } else if (move_out == -1) {
    simplexinfo_.workValue_[variable_out] = simplexinfo_.workLower_[variable_out];
    simplexbasis_.nonbasicMove_[variable_out] = 1;
  } else {
    simplexinfo_.workValue_[variable_out] = simplexinfo_.workUpper_[variable_out];
    simplexbasis_.nonbasicMove_[variable_out] = -1;
  }
  // Update the dual objective value
  double nwValue = simplexinfo_.workValue_[variable_out];
  double vrDual = simplexinfo_.workDual_[variable_out];
  double dl_dual_objective_value = nwValue * vrDual;
  simplexinfo_.updated_dual_objective_value += dl_dual_objective_value;
  simplexinfo_.update_count++;
  // Update the number of basic logicals
  if (variable_out < lp_.numCol_) simplexinfo_.num_basic_logicals++;
  if (variable_in < lp_.numCol_) simplexinfo_.num_basic_logicals--;
  // No longer have a representation of B^{-1}, and certainly not
  // fresh!
  status_.has_invert = false;
  status_.has_fresh_invert = false;
  // Data are no longer fresh from rebuild
  status_.has_fresh_rebuild = false;
  analysis_.simplexTimerStop(UpdatePivotsClock);
}

void Primal::updateMatrix(const HighsInt variable_in, const HighsInt variable_out) {
  analysis_.simplexTimerStart(UpdateMatrixClock);
  matrix_.update(variable_in, variable_out);
  analysis_.simplexTimerStop(UpdateMatrixClock);
}

void Primal::choosePriceTechnique(const HighsInt price_strategy, const double row_ep_density, bool &use_col_price,
                                  bool &use_row_price_w_switch) {
  // By default switch to column PRICE when pi_p has at least this
  // density
  const double density_for_column_price_switch = 0.75;
  use_col_price = (price_strategy == kSimplexPriceStrategyCol) ||
                  (price_strategy == kSimplexPriceStrategyRowSwitchColSwitch &&
                   row_ep_density > density_for_column_price_switch);
  use_row_price_w_switch =
          price_strategy == kSimplexPriceStrategyRowSwitch ||
          price_strategy == kSimplexPriceStrategyRowSwitchColSwitch;
}

void
Primal::putBacktrackingBasis(const vector<HighsInt> &basicIndex_before_compute_factor, double *scattered_edge_weights) {
  simplexinfo_.valid_backtracking_basis_ = true;
  simplexinfo_.backtracking_basis_ = simplexbasis_;
  simplexinfo_.backtracking_basis_.basicIndex_ = basicIndex_before_compute_factor;
  simplexinfo_.backtracking_basis_costs_perturbed_ = simplexinfo_.costs_perturbed;
  simplexinfo_.backtracking_basis_workShift_ = simplexinfo_.workShift_;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  const bool handle_edge_weights = scattered_edge_weights != NULL;
  if (handle_edge_weights) {
    for (HighsInt iVar = 0; iVar < num_tot; iVar++)
      simplexinfo_.backtracking_basis_edge_weights_[iVar] =
              scattered_edge_weights[iVar];
  }
}

void Primal::putBacktrackingBasis() {
  const vector<HighsInt> &basicIndex = simplexbasis_.basicIndex_;
  const bool handle_edge_weights = workEdWt_ != NULL;
  if (handle_edge_weights) {
    analysis_.simplexTimerStart(PermWtClock);
    for (HighsInt i = 0; i < lp_.numRow_; i++)
      workEdWtFull_[basicIndex[i]] = workEdWt_[i];
    analysis_.simplexTimerStop(PermWtClock);
  }
  putBacktrackingBasis(basicIndex, workEdWtFull_);
}

bool Primal::getNonsingularInverse(const HighsInt solve_phase) {
  assert(status_.has_basis);
  const vector<HighsInt> &basicIndex = simplexbasis_.basicIndex_;
  // Take a copy of basicIndex from before INVERT to be used as the
  // saved ordering of basic variables - so reinvert will run
  // identically.
  const vector<HighsInt> basicIndex_before_compute_factor = basicIndex;
  // Save the number of updates performed in case it has to be used to determine
  // a limit
  const HighsInt simplex_update_count = simplexinfo_.update_count;
  // Dual simplex edge weights are identified with rows, so must be
  // permuted according to INVERT. This must be done if workEdWt_ is
  // not NULL.
  const bool handle_edge_weights = workEdWt_ != NULL;
  // Scatter the edge weights so that, after INVERT, they can be
  // gathered according to the new permutation of basicIndex
  if (handle_edge_weights) {
    analysis_.simplexTimerStart(PermWtClock);
    for (HighsInt i = 0; i < lp_.numRow_; i++)
      workEdWtFull_[basicIndex[i]] = workEdWt_[i];
    analysis_.simplexTimerStop(PermWtClock);
  }

  // Call computeFactor to perform INVERT
  HighsInt rank_deficiency = computeFactor();
  const bool artificial_rank_deficiency = false;  //  true;//
  if (artificial_rank_deficiency) {
    if (!simplexinfo_.phase1_backtracking_test_done && solve_phase == kSolvePhase1) {
      // Claim rank deficiency to test backtracking
      printf("Phase1 (Iter %" HIGHSINT_FORMAT             ") Claiming rank deficiency to test backtracking\n",
             iteration_count_);
      rank_deficiency = 1;
      simplexinfo_.phase1_backtracking_test_done = true;
    } else if (!simplexinfo_.phase2_backtracking_test_done && solve_phase == kSolvePhase2) {
      // Claim rank deficiency to test backtracking
      printf("Phase2 (Iter %" HIGHSINT_FORMAT
             ") Claiming rank deficiency to test backtracking\n", iteration_count_);
      rank_deficiency = 1;
      simplexinfo_.phase2_backtracking_test_done = true;
    }
  }
  if (rank_deficiency) {
    // Rank deficient basis, so backtrack to last full rank basis
    //
    // Get the last nonsingular basis - so long as there is one
    if (!getBacktrackingBasis(workEdWtFull_)) return false;
    // Record that backtracking is taking place
    simplexinfo_.backtracking_ = true;
    updateSimplexLpStatus(status_, LpAction::kBacktracking);
    HighsInt backtrack_rank_deficiency = computeFactor();
    // This basis has previously been inverted successfully, so it shouldn't be
    // singular
    if (backtrack_rank_deficiency) return false;
    // simplex update limit will be half of the number of updates
    // performed, so make sure that at least one update was performed
    if (simplex_update_count <= 1) return false;
    HighsInt use_simplex_update_limit = simplexinfo_.update_limit;
    HighsInt new_simplex_update_limit = simplex_update_count / 2;
    simplexinfo_.update_limit = new_simplex_update_limit;
    highsLogUser(options_.log_options, HighsLogType::kWarning,
                 "Rank deficiency of %" HIGHSINT_FORMAT
                 " after %" HIGHSINT_FORMAT
                 " simplex updates, so "
                 "backtracking: max updates reduced from %" HIGHSINT_FORMAT
                 " to %" HIGHSINT_FORMAT "\n",
                 rank_deficiency, simplex_update_count,
                 use_simplex_update_limit, new_simplex_update_limit);
  } else {
    // Current basis is full rank so save it
    putBacktrackingBasis(basicIndex_before_compute_factor, workEdWtFull_);
    // Indicate that backtracking is not taking place
    simplexinfo_.backtracking_ = false;
    // Reset the update limit in case this is the first successful
    // inversion after backtracking
    simplexinfo_.update_limit = options_.simplex_update_limit;
  }
  if (handle_edge_weights) {
    // Gather the edge weights according to the permutation of
    // basicIndex after INVERT
    analysis_.simplexTimerStart(PermWtClock);
    for (HighsInt i = 0; i < lp_.numRow_; i++)
      workEdWt_[i] = workEdWtFull_[basicIndex[i]];
    analysis_.simplexTimerStop(PermWtClock);
  }
  return true;
}

bool Primal::getBacktrackingBasis(double *scattered_edge_weights) {
  if (!simplexinfo_.valid_backtracking_basis_) return false;
  simplexbasis_ = simplexinfo_.backtracking_basis_;
  simplexinfo_.costs_perturbed = simplexinfo_.backtracking_basis_costs_perturbed_;
  simplexinfo_.workShift_ = simplexinfo_.backtracking_basis_workShift_;
  const HighsInt num_tot = lp_.numCol_ + lp_.numRow_;
  const bool handle_edge_weights = scattered_edge_weights != NULL;
  if (handle_edge_weights) {
    for (HighsInt iVar = 0; iVar < num_tot; iVar++)
      scattered_edge_weights[iVar] =
              simplexinfo_.backtracking_basis_edge_weights_[iVar];
  }
  return true;
}

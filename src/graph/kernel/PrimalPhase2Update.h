//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE2_UPDATE_H
#define HIGHS_PRIMAL_PHASE2_UPDATE_H

#include "../KernelBase.h"

class PrimalPhase2Update : public KernelBase {
public:
    PrimalPhase2Update(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase2Update() = default;

    int Run() override;
};


#endif //HIGHS_PRIMAL_PHASE2_UPDATE_H

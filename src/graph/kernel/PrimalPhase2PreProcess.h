//
// Created by root on 8/12/21.
//

#ifndef HIGHS_PRIMAL_PHASE2_PREPROCESS_H
#define HIGHS_PRIMAL_PHASE2_PREPROCESS_H

#include "../KernelBase.h"

class PrimalPhase2PreProcess : public KernelBase {
public:
    PrimalPhase2PreProcess(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase2PreProcess() = default;

    int Run() override;

};


#endif //HIGHS_PRIMAL_PHASE2_PREPROCESS_H

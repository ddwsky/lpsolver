//
// Created by root on 8/9/21.
//

#include "DGPrimal.h"
#include "simplex/HEkkDebug.h"
#include "util/HighsSort.h"

DGPrimal::DGPrimal(Primal &simplex, OpType initOpid) : DynamicGraph<Primal, DGInfo>(simplex, initOpid) {
}

HighsStatus DGPrimal::solve() {

  // The major solving loop
  Run();

  // Should only have these cases
  assert(dgInfo_.solve_phase ==
         kSolvePhaseExit ||  // solve_phase == kSolvePhaseUnknown ||
         dgInfo_.solve_phase == kSolvePhaseOptimal ||  // solve_phase == kSolvePhase1 ||
         dgInfo_.solve_phase == kSolvePhaseCleanup);
  if (dgInfo_.solve_phase == kSolvePhaseOptimal)
    ekk_instance_.model_status_ = HighsModelStatus::kOptimal;
  return ekk_instance_.returnFromSolve(HighsStatus::kOk);
}


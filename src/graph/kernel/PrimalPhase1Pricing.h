//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE1_PRICING_H
#define HIGHS_PRIMAL_PHASE1_PRICING_H

#include "../KernelBase.h"

class PrimalPhase1Pricing : public KernelBase {
public:
    PrimalPhase1Pricing(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase1Pricing() = default;

    int Run() override;

};


#endif //HIGHS_PRIMAL_PHASE1_PRICING_H

#ifndef DG_PRIMAL_SIMPLEX_H_
#define DG_PRIMAL_SIMPLEX_H_

#include <utility>

#include "Primal.h"
#include "KernelBase.h"
#include "util/HSet.h"
#include "kernel_types.h"
#include "DynamicGraph.h"

class DGPrimal : DynamicGraph<Primal, DGInfo> {
public:
    explicit DGPrimal(Primal &simplex, OpType initOpid);

    ~DGPrimal() = default;

    HighsStatus solve() override;

};

#endif /* DG_PRIMAL_SIMPLEX_H_ */

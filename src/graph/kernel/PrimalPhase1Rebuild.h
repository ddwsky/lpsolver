//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE1_REBUILD_H
#define HIGHS_PRIMAL_PHASE1_REBUILD_H

#include "../KernelBase.h"

class PrimalPhase1Rebuild : public KernelBase {
public:
    PrimalPhase1Rebuild(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase1Rebuild() = default;

    int Run() override;
};


#endif //HIGHS_PRIMAL_PHASE1_REBUILD_H

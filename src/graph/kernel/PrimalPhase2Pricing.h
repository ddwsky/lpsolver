//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE2_PRICING_H
#define HIGHS_PRIMAL_PHASE2_PRICING_H

#include "../KernelBase.h"

class PrimalPhase2Pricing : public KernelBase {
public:
    PrimalPhase2Pricing(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase2Pricing() = default;

    int Run() override;

};


#endif //HIGHS_PRIMAL_PHASE2_PRICING_H

//
// Created by root on 8/12/21.
//

#include "PrimalPhase2PostProcess.h"
#include "../KernelRegistry.h"

int PrimalPhase2PostProcess::Run() {
  HighsSimplexStatus &status = dgInfo_.ekk_instance_.status_;
  std::cout << "solve_phase: " << dgInfo_.solve_phase << ", primal objective: "
            << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value << ", " << dgInfo_.rebuild_reason << std::endl;

  if (!dgInfo_.rebuild_reason) {
    return OP_PRIMAL_PHASE2_PRICING;
  } else if (!(status.has_fresh_rebuild && dgInfo_.num_flip_since_rebuild == 0)) {
    return OP_PRIMAL_PHASE2_REBUILD;
  }
  if (dgInfo_.solve_phase == kSolvePhase1) {
    return OP_PRIMAL_PHASE1_REBUILD;
  } else if (dgInfo_.variable_in == -1) {
    // There is no candidate in CHUZC, even after rebuild so probably optimal
    // Remove any bound perturbations and see if basis is still primal feasible
    dgInfo_.cleanup();
    if (dgInfo_.ekk_instance_.simplexinfo_.num_primal_infeasibility > 0) {
      // There are primal infeasiblities, so consider performing dual
      // simplex iterations to get primal feasibility
      dgInfo_.solve_phase = kSolvePhaseCleanup;
    } else {
      // There are no primal infeasiblities so optimal!
      dgInfo_.solve_phase = kSolvePhaseOptimal;
      dgInfo_.model_status_ = HighsModelStatus::kOptimal;
      dgInfo_.ekk_instance_.computeDualObjectiveValue();  // Why?
    }
    return -1;
  } else {
    assert(dgInfo_.row_out < 0);

    // There is no candidate in CHUZR, so probably primal unbounded
    highsLogDev(dgInfo_.options_.log_options, HighsLogType::kInfo, "primal-phase-2-unbounded\n");
    if (dgInfo_.ekk_instance_.simplexinfo_.bounds_perturbed) {
      // If the bounds have been perturbed, clean up and return
      dgInfo_.cleanup();
      // If there are primal infeasiblities, go back to phase 1
      if (dgInfo_.ekk_instance_.simplexinfo_.num_primal_infeasibility > 0)
        dgInfo_.solve_phase = kSolvePhase1;
      return OP_PRIMAL_PHASE1_REBUILD;
    } else {
      // The bounds have not been perturbed, so primal unbounded
      dgInfo_.solve_phase = kSolvePhaseExit;
      // Model status should be unset
      assert(dgInfo_.model_status_ == HighsModelStatus::kNotset);
      highsLogDev(dgInfo_.options_.log_options, HighsLogType::kInfo, "problem-primal-unbounded\n");
      dgInfo_.model_status_ = HighsModelStatus::kUnbounded;
      return -1;
    }
  }
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE2_POSTPROCESS, KernelCreator<PrimalPhase2PostProcess>)
//
// Created by root on 8/9/21.
//

#include "FTran.h"
#include "../KernelRegistry.h"

int FTran::Run() {
  // rebuild_reason = kRebuildReasonPossiblySingularBasis is set if
  // numerical trouble is detected
  HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  vector<double> &workDual = info.workDual_;
  const vector<int8_t> &nonbasicMove = dgInfo_.ekk_instance_.simplexbasis_.nonbasicMove_;
  const double updated_theta_dual = workDual[dgInfo_.variable_in];
  // Determine the move direction - can't use nonbasicMove_[variable_in]
  // due to free columns
  dgInfo_.move_in = updated_theta_dual > 0 ? -1 : 1;
  // Unless the variable is free, nonbasicMove[variable_in] should be the same
  // as move_in
  if (nonbasicMove[dgInfo_.variable_in]) assert(nonbasicMove[dgInfo_.variable_in] == dgInfo_.move_in);
  //
  // FTRAN
  //
  // Compute pivot column
  dgInfo_.ekk_instance_.pivotColumnFtran(dgInfo_.variable_in, dgInfo_.col_aq);
  // Compute the dual for the pivot column and compare it with the
  // updated value
  double computed_theta_dual = dgInfo_.ekk_instance_.computeDualForTableauColumn(dgInfo_.variable_in, dgInfo_.col_aq);
//  ekkDebugUpdatedDual(dgInfo_.ekk_instance_.options_, updated_theta_dual, computed_theta_dual);

  // Feed in the computed dual value
  info.workDual_[dgInfo_.variable_in] = computed_theta_dual;
  // Reassign theta_dual to be the computed value
  dgInfo_.theta_dual = info.workDual_[dgInfo_.variable_in];
  // Determine whether theta_dual is too small or has changed sign
  const bool theta_dual_small = fabs(dgInfo_.theta_dual) <= dgInfo_.dual_feasibility_tolerance;
  const bool theta_dual_sign_error = updated_theta_dual * computed_theta_dual <= 0;

  if (theta_dual_small || theta_dual_sign_error) {
    // The computed dual is small or has a sign error, so don't use it
    // If a significant computed dual has sign error, consider reinverting
    if (!theta_dual_small && info.update_count > 0) {
      dgInfo_.rebuild_reason = kRebuildReasonPossiblySingularBasis;
    }
    return dgInfo_.solve_phase == kSolvePhase1 ? OP_PRIMAL_PHASE1_REBUILD : OP_PRIMAL_PHASE2_REBUILD;
  }
  return dgInfo_.solve_phase == kSolvePhase1 ? OP_PRIMAL_PHASE1_RATIO_TEST : OP_PRIMAL_PHASE2_RATIO_TEST;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_FTRAN, KernelCreator<FTran>)
REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE2_FTRAN, KernelCreator<FTran>)
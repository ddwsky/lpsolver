//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE1_RATIO_TEST_H
#define HIGHS_PRIMAL_PHASE1_RATIO_TEST_H

#include "../KernelBase.h"

class PrimalPhase1RatioTest : public KernelBase {
public:
    PrimalPhase1RatioTest(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase1RatioTest() = default;

    int Run() override;

    int BoundFlip();

    void RatioTest();

};


#endif //HIGHS_PRIMAL_PHASE1_RATIO_TEST_H

//
// Created by root on 8/12/21.
//

#ifndef HIGHS_PRIMAL_PHASE2_POSTPROCESS_H
#define HIGHS_PRIMAL_PHASE2_POSTPROCESS_H

#include "../KernelBase.h"

class PrimalPhase2PostProcess : public KernelBase {
public:
    PrimalPhase2PostProcess(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase2PostProcess() = default;

    int Run() override;

};


#endif //HIGHS_PRIMAL_PHASE2_POSTPROCESS_H

import numpy as np
from load import load_pro

class DualSimplex():

    def __init__(self):
        self._A = ""  # 系数矩阵
        self._b = ""  #
        self._c = ''  # 目标向量
        self._B = ''  # 基变量的下标集合
        self.var = 0  # var个数
        self.row = 0  # 约束个数

    def solve(self, filename):
        A_org, b_org, c_org, var, row = load_pro(filename)
        A_np = np.array(A_org, np.float)
        b_np = np.array(b_org, np.float)
        c_np = np.array(c_org, np.float)
        self.var, self.row = var, row

        # step1: 原问题化为标准型,行变换,使b>=0
        A, b = self.InitializeSimplex(A_np, b_np)

        # step2: 添加人工变量(松弛变量)，得到辅助问题。
        slacks = np.eye(row)
        A = np.concatenate((A, slacks), axis=1)
        c = np.concatenate((np.zeros(var), np.ones(row)))
        B = [i + var for i in range(row)]

        # step3 使用人工变量作为初始的基，构造辅助问题的初始单纯形表。在该表中同时也包含原问题的检验数行。

        # step4 使用单纯形算法求解辅助问题。（第一阶段）
        obj = np.sum(b)  # 辅助方程的目标函数值
        B, A, b, c, obj = self.Simplex(B, A, b, c, obj)

        # step5 若求得辅助问题最优解值 g* > 0，则原问题无可行解，结束。
        if isinstance(B, bool):
            return

        # step6 （否则g* = 0。）若某些人工变量为基变量，则调整，直到没有人工变量为基变量。
        self.ReplaceAuxVar(B, A, b, c, obj)

        # step7 去掉当前单纯形表上的辅助问题的检验数行和人工变量对应的列，得到原问题的单纯形表。此时已有一个初始的基可行解。
        A = A[:, 0:self.var]

        # step8 （第二阶段）运行单纯形算法，解原问题。最后或判断得原问题无界，或求到最优解
        obj = np.dot(c_np[B], b)  # 函数目标值
        B, A, b, c, obj = self.Simplex(B, A, b, c_np, obj)

        # finish
        x = self._CalculateX(B, A, b, c)

        ######print
        self.print(x, obj, A_org)

    # 添加人工变量得到一个初始解
    def InitializeSimplex(self, A, b):

        b_min, min_pos = (np.min(b), np.argmin(b))  # 得到最小bi

        # 将bi全部转化成正数
        if (b_min < 0):
            for i in range(self.row):
                if i != min_pos and b[i] < 0:
                    A[i] = A[i] - A[min_pos]
                    b[i] = b[i] - b[min_pos]
            A[min_pos] = A[min_pos] * -1
            b[min_pos] = b[min_pos] * -1
        return A, b

    def Simplex(self, B, A, b, c, obj):
        c = (c[B].reshape(1, -1).dot(A) - c).reshape(-1)  # 求检验数向量cbt*B-1*A-ct
        # 选校验数最大的为入基
        k = np.argmax(c)

        while c[k] > 0:
            theta = []
            for i in range(len(b)):
                if A[i][k] > 0:
                    theta.append(b[i] / A[i][k])
                else:
                    theta.append(float("inf"))

            # 选bi/aik最小的为出基
            r = np.argmin(np.array(theta))

            if theta[r] == float('inf'):
                print('unbounded')
                return [False] * 5

            (B, A, b, c, obj) = self._PIVOT(B, A, b, c, obj, r, k)

            k = np.argmax(c)

        return B, A, b, c, obj

    def ReplaceAuxVar(self, B, A, b, c, obj):
        for mb in B:
            if mb >= self.var:
                row = mb - self.var
                i = 0
                while A[row][i] == 0 and i < self.var:
                    i += 1
                (B, A, b, c, obj) = self._PIVOT(B, A, b, c, obj, B.index(mb), i)

    # 得到完整解
    def _CalculateX(self, B, A, b, c):

        x = np.zeros(self.var, dtype=float)
        x[B] = b
        return x

    # 基变换
    def _PIVOT(self, B, A, b, c, z, r, k):
        # c 为校验数
        # z 目标值
        # r represents leaving basis
        # k represents entering basis

        main_elem = A[r][k]
        # scaling the r-th line
        A[r] = A[r] / main_elem
        b[r] = b[r] / main_elem

        # change k-th column to unit array
        for i in range(self.row):
            if i != r:
                b[i] = b[i] - A[i][k] * b[r]
                A[i] = A[i] - A[i][k] * A[r]

        # update objective value
        z -= b[r] * c[k]

        c = c - c[k] * A[r]

        # change the basis
        B[r] = k

        return (B, A, b, c, z)

    def print(self, x, obj, A):
        px = ['x_%d = %f' % (i + 1, x[i]) for i in range(len(x))]
        print(','.join(px))
        print('objective value is : %f' % obj)
        print('------------------------------')
        for i in range(len(A)):
            print('%d-th line constraint value is : %f' % (i + 1, x.dot(A[i])))


s = DualSimplex()
s.solve('pro.txt')

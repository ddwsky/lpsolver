# load pro / mps / lp

def load_pro(filename):
    # 读取文件内容，文件结构前两行分别为 变量数 和 约束条件个数
    # 接下来是系数矩阵
    # 然后是b数组
    # 然后是约束条件c
    # 假设线性规划形式是标准形式（都是等式）

    A = []
    b = []
    c = []
    with open(filename, 'r') as f:
        var = int(f.readline())
        row = int(f.readline())

        for i in range(row):
            x = list(map(int, f.readline().strip().split()))
            A.append(x)
        b = list(map(int, f.readline().strip().split()))
        c = list(map(int, f.readline().strip().split()))
        return A, b, c, var, row

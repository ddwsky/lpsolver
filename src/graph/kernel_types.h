//
// Created by root on 8/9/21.
//

#ifndef HIGHS_KERNEL_TYPES_H
#define HIGHS_KERNEL_TYPES_H

#include <vector>
#include "util/HSet.h"
#include "lp_data/HighsLpUtils.h"
#include "lp_data/HighsRanging.h"
#include "lp_data/HighsInfo.h"
#include "model/HighsModel.h"
#include "Primal.h"


constexpr size_t MAX_KERNEL_NUM = 200;

enum KernelArch {
    ARCH_MIN = 0,
    ARCH_CPU = 0,
    ARCH_NV,
    ARCH_MAX,
};
enum DataType {
    DATA_TYPE_MIN = 0,
    DATA_TYPE_FLOAT32 = 0,
    DATA_TYPE_FLOAT64,
    DATA_TYPE_MAX,
};
enum OpType {
    OP_TYPE_MIN = 0,
    OP_PRIMAL_ENTER = OP_TYPE_MIN,
    OP_PRIMAL_INITIALIZE = OP_TYPE_MIN,
    OP_PRIMAL_PHASE1_REBUILD,
    OP_PRIMAL_PHASE1_PRICING,
    OP_PRIMAL_PHASE1_FTRAN,
    OP_PRIMAL_PHASE1_RATIO_TEST,
    OP_PRIMAL_PHASE1_BTRAN,
    OP_PRIMAL_PHASE1_UPDATE,
    OP_PRIMAL_PHASE1_POSTPROCESS,
    OP_PRIMAL_PHASE2_PREPROCESS,
    OP_PRIMAL_PHASE2_REBUILD,
    OP_PRIMAL_PHASE2_PRICING,
    OP_PRIMAL_PHASE2_FTRAN,
    OP_PRIMAL_PHASE2_RATIO_TEST,
    OP_PRIMAL_PHASE2_BTRAN,
    OP_PRIMAL_PHASE2_UPDATE,
    OP_PRIMAL_PHASE2_POSTPROCESS,
    OP_TYPE_MAX,
};
enum GraphType {
    GRAPH_TYPE_MIN = 0,
    GRAPH_PRIMAL = GRAPH_TYPE_MIN,
    GRAPH_DUAL,
    GRAPH_IPM,
    GRAPH_CRASH,
    GRAPH_PRESOLVE,
    GRAPH_POSTSOLVE,
    GRAPH_TYPE_MAX,
};
struct LPOptions{

};
struct RuntimeDesc {
    KernelArch arch;
    DataType dtype;
};
struct KernelKey {
    KernelArch arch;
    DataType dtype;
    OpType opid;
};

struct DGInfo {
    DGInfo(Primal &ekk_instance) : ekk_instance_(ekk_instance) {};
    // ekk
    Primal &ekk_instance_;
    HighsOptions &options_{ekk_instance_.options_};
    HighsBasis &basis_{ekk_instance_.basis_};
    HighsModelStatus &model_status_{ekk_instance_.model_status_};
    double &cost_scale_{ekk_instance_.cost_scale_};
    HighsInt &iteration_count_{ekk_instance_.iteration_count_};

    bool &solve_bailout_{ekk_instance_.solve_bailout_};
    bool &called_return_from_solve_{ekk_instance_.called_return_from_solve_};
    SimplexAlgorithm &exit_algorithm{ekk_instance_.exit_algorithm};
    HighsInt &return_primal_solution_status{ekk_instance_.return_primal_solution_status};
    HighsInt &return_dual_solution_status{ekk_instance_.return_dual_solution_status};

    HighsLp &lp_{ekk_instance_.lp_};
    std::string &lp_name_{ekk_instance_.lp_name_};
    HighsSimplexStatus &status_{ekk_instance_.status_};

    HighsSimplexInfo &simplexinfo_{ekk_instance_.simplexinfo_};
    SimplexBasis &simplexbasis_{ekk_instance_.simplexbasis_};
    HighsRandom &random_{ekk_instance_.random_};

    double *&workEdWt_{ekk_instance_.workEdWt_};      //!< DSE or Dvx weight
    double *&workEdWtFull_{ekk_instance_.workEdWtFull_};  //!< Full-length std::vector where weights

    HMatrix &matrix_{ekk_instance_.matrix_};
    HFactor &factor_{ekk_instance_.factor_};

    double &build_synthetic_tick_{ekk_instance_.build_synthetic_tick_};
    double &total_synthetic_tick_{ekk_instance_.total_synthetic_tick_};

    HighsSolutionParams &solution_params_{ekk_instance_.solution_params_};
    HighsScale &scale_{ekk_instance_.scale_};

    //DGPrimal
    // References:
    vector<double> devex_weight;
    HighsInt num_col;
    HighsInt num_row;
    HighsInt num_tot;
    HighsInt solve_phase;
    double primal_feasibility_tolerance;
    double dual_feasibility_tolerance;
    HSet nonbasic_free_col_set;
    double objective_target;
    HighsInt rebuild_reason;
    // Pivot related
    HighsInt variable_in;
    HighsInt move_in;
    HighsInt row_out;
    HighsInt variable_out;
    HighsInt move_out;
    double theta_dual;
    double theta_primal;
    double value_in;
    double alpha_col;
    double alpha_row;
    double numericalTrouble;
    HighsInt num_flip_since_rebuild;
    // Primal phase 1 tools
    vector<std::pair<double, int> > ph1SorterR;
    vector<std::pair<double, int> > ph1SorterT;
    // Devex weight
    HighsInt num_devex_iterations;
    HighsInt num_bad_devex_weight;
    vector<HighsInt> devex_index;
    const HighsInt allowed_num_bad_devex_weight = 3;
    const double bad_devex_weight_factor = 3;
    // Solve buffer
    HVector row_ep;
    HVector row_ap;
    HVector col_aq;
    HVector col_basic_feasibility_change;
    HVector row_basic_feasibility_change;
    const HighsInt primal_correction_strategy = kSimplexPrimalCorrectionStrategyAlways;
    HighsInt num_free_col;

    //runtime
    RuntimeDesc rtDesc_{ARCH_CPU, DATA_TYPE_FLOAT32};

    HighsSimplexAnalysis *analysis{&ekk_instance_.analysis_};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void getBasicPrimalInfeasibility() {
      // Gets the num/max/sum of basic primal infeasibliities,
      const double primal_feasibility_tolerance = ekk_instance_.options_.primal_feasibility_tolerance;
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      HighsInt &num_primal_infeasibility = info.num_primal_infeasibility;
      double &max_primal_infeasibility = info.max_primal_infeasibility;
      double &sum_primal_infeasibility = info.sum_primal_infeasibility;
      const HighsInt updated_num_primal_infeasibility = num_primal_infeasibility;
      num_primal_infeasibility = 0;
      max_primal_infeasibility = 0;
      sum_primal_infeasibility = 0;

      for (HighsInt iRow = 0; iRow < num_row; iRow++) {
        double value = info.baseValue_[iRow];
        double lower = info.baseLower_[iRow];
        double upper = info.baseUpper_[iRow];
        // @primal_infeasibility calculation
        double primal_infeasibility = 0;
        if (value < lower - primal_feasibility_tolerance) {
          primal_infeasibility = lower - value;
        } else if (value > upper + primal_feasibility_tolerance) {
          primal_infeasibility = value - upper;
        }
        if (primal_infeasibility > 0) {
          if (primal_infeasibility > primal_feasibility_tolerance)
            num_primal_infeasibility++;
          max_primal_infeasibility = std::max(primal_infeasibility, max_primal_infeasibility);
          sum_primal_infeasibility += primal_infeasibility;
        }
      }
    }

    void phase1ComputeDual() {
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      const vector<int8_t> &nonbasicFlag = ekk_instance_.simplexbasis_.nonbasicFlag_;
      const double primal_feasibility_tolerance = ekk_instance_.options_.primal_feasibility_tolerance;

      HVector buffer;
      buffer.setup(num_row);
      buffer.clear();
      buffer.count = 0;
      // Accumulate costs for checking
      info.workCost_.assign(num_tot, 0);
      // Zero the dual values
      info.workDual_.assign(num_tot, 0);
      // Determine the base value for cost perturbation
      const double base = info.primal_simplex_phase1_cost_perturbation_multiplier * 5e-7;
      for (HighsInt iRow = 0; iRow < num_row; iRow++) {
        const double value = info.baseValue_[iRow];
        const double lower = info.baseLower_[iRow];
        const double upper = info.baseUpper_[iRow];
        HighsInt bound_violated = 0;
        if (value < lower - primal_feasibility_tolerance) {
          bound_violated = -1;
        } else if (value > upper + primal_feasibility_tolerance) {
          bound_violated = 1;
        }
        if (!bound_violated) continue;
        double cost = bound_violated;
        if (base) cost *= 1 + base * info.numTotRandomValue_[iRow];
        buffer.array[iRow] = cost;
        buffer.index[buffer.count++] = iRow;
      }
      if (buffer.count <= 0) {
        // Strange, should be a non-trivial RHS
        assert(buffer.count > 0);
        return;
      }
      for (HighsInt iRow = 0; iRow < num_row; iRow++)
        info.workCost_[ekk_instance_.simplexbasis_.basicIndex_[iRow]] = buffer.array[iRow];
      //
      // Full BTRAN
      //
      ekk_instance_.fullBtran(buffer);
      //
      // Full PRICE
      //
      HVector bufferLong;
      bufferLong.setup(num_col);
      ekk_instance_.fullPrice(buffer, bufferLong);

      for (HighsInt iCol = 0; iCol < num_col; iCol++)
        info.workDual_[iCol] = -nonbasicFlag[iCol] * bufferLong.array[iCol];
      for (HighsInt iRow = 0, iCol = num_col; iRow < num_row; iRow++, iCol++)
        info.workDual_[iCol] = -nonbasicFlag[iCol] * buffer.array[iRow];
    }

    bool correctPrimal(const bool initialise = false) {
      if (primal_correction_strategy == kSimplexPrimalCorrectionStrategyNone)
        return true;
      static double max_max_primal_correction;
      if (initialise) {
        max_max_primal_correction = 0;
        return true;
      }
      assert(solve_phase == kSolvePhase2);
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      HighsInt num_primal_correction = 0;
      double max_primal_correction = 0;
      double sum_primal_correction = 0;
      HighsInt num_primal_correction_skipped = 0;
      for (HighsInt iRow = 0; iRow < num_row; iRow++) {
        double lower = info.baseLower_[iRow];
        double upper = info.baseUpper_[iRow];
        double value = info.baseValue_[iRow];
        HighsInt bound_violated = 0;
        if (value < lower - primal_feasibility_tolerance) {
          bound_violated = -1;
        } else if (value > upper + primal_feasibility_tolerance) {
          bound_violated = 1;
        }
        if (bound_violated) {
          if (info.allow_bound_perturbation) {
            HighsInt iCol = ekk_instance_.simplexbasis_.basicIndex_[iRow];
            double bound_shift;
            if (bound_violated > 0) {
              // Perturb the upper bound to accommodate the infeasiblilty
              shiftBound(false, iCol, info.baseValue_[iRow], info.numTotRandomValue_[iCol], info.workUpper_[iCol],
                         bound_shift, true);
              info.baseUpper_[iRow] = info.workUpper_[iCol];
              info.workUpperShift_[iCol] += bound_shift;
            } else {
              // Perturb the lower bound to accommodate the infeasiblilty
              shiftBound(true, iCol, info.baseValue_[iRow], info.numTotRandomValue_[iCol], info.workLower_[iCol],
                         bound_shift, true);
              info.baseLower_[iRow] = info.workLower_[iCol];
              info.workLowerShift_[iCol] += bound_shift;
            }
            assert(bound_shift > 0);
            num_primal_correction++;
            max_primal_correction = max(bound_shift, max_primal_correction);
            sum_primal_correction += bound_shift;
            info.bounds_perturbed = true;
          } else {
            // Bound perturbation is not permitted
            num_primal_correction_skipped++;
          }
        }
      }
      if (num_primal_correction_skipped) {
        highsLogDev(ekk_instance_.options_.log_options, HighsLogType::kError,
                    "correctPrimal: Missed %d bound shifts\n", num_primal_correction_skipped);
        assert(!num_primal_correction_skipped);
        return false;
      }
      if (max_primal_correction > 2 * max_max_primal_correction) {
        highsLogDev(ekk_instance_.options_.log_options, HighsLogType::kInfo,
                    "phase2CorrectPrimal: num / max / sum primal corrections = "
                    "%" HIGHSINT_FORMAT
                    " / %g / "
                    "%g\n", num_primal_correction, max_primal_correction, sum_primal_correction);
        max_max_primal_correction = max_primal_correction;
      }
      return true;
    }

    void shiftBound(const bool lower, const HighsInt iVar, const double value, const double random_value,
                    double &bound, double &shift, const bool report) {
      double feasibility = (1 + random_value) * primal_feasibility_tolerance;
      double old_bound = bound;
      std::string type;
      double infeasibility;
      double new_infeasibility;
      if (lower) {
        // Bound to shift is lower
        type = "lower";
        assert(value < bound - primal_feasibility_tolerance);
        infeasibility = bound - value;
        assert(infeasibility > 0);
        // Determine the amount by which value will be feasible - so that
        // it's not degenerate
        shift = infeasibility + feasibility;
        bound -= shift;
        new_infeasibility = bound - value;
        assert(new_infeasibility < 0);
      } else {
        // Bound to shift is upper
        type = "upper";
        assert(value > bound + primal_feasibility_tolerance);
        infeasibility = value - bound;
        assert(infeasibility > 0);
        // Determine the amount by which value will be feasible - so that
        // it's not degenerate
        shift = infeasibility + feasibility;
        bound += shift;
        new_infeasibility = value - bound;
        assert(new_infeasibility < 0);
      }
      double error = fabs(-new_infeasibility - feasibility);
      if (report)
        highsLogDev(ekk_instance_.options_.log_options, HighsLogType::kVerbose,
                    "Value(%4" HIGHSINT_FORMAT
                    ") = %10.4g exceeds %s = %10.4g by %9.4g, so shift bound by "
                    "%9.4g to %10.4g: infeasibility %10.4g with error %g\n",
                    iVar, value, type.c_str(), old_bound, infeasibility, shift, bound, new_infeasibility, error);
    }

    void updateVerify() {
      // updateVerify for primal
      const HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      const double numerical_trouble_tolerance = 1e-7;
      numericalTrouble = 0;
      double abs_alpha_from_col = fabs(alpha_col);
      std::string alpha_row_source;
      if (variable_in < num_col) {
        alpha_row = row_ap.array[variable_in];
        alpha_row_source = "Col";
      } else {
        alpha_row = row_ep.array[variable_in - num_col];
        alpha_row_source = "Row";
      }
      double abs_alpha_from_row = fabs(alpha_row);
      double abs_alpha_diff = fabs(abs_alpha_from_col - abs_alpha_from_row);
      double min_abs_alpha = min(abs_alpha_from_col, abs_alpha_from_row);
      numericalTrouble = abs_alpha_diff / min_abs_alpha;
      if (numericalTrouble > numerical_trouble_tolerance)
        printf("Numerical check: Iter %4" HIGHSINT_FORMAT
               ": alpha_col = %12g, (From %3s alpha_row = "
               "%12g), aDiff = %12g: measure = %12g\n",
               ekk_instance_.iteration_count_, alpha_col, alpha_row_source.c_str(), alpha_row, abs_alpha_diff,
               numericalTrouble);
//      assert(numericalTrouble < 1e-3);
      // Reinvert if the relative difference is large enough, and updates have been
      // performed
      //
      std::cout << "num trub:" << numericalTrouble << ", " << abs_alpha_diff << ", " <<  min_abs_alpha << ", " <<  alpha_col << ", " <<  alpha_row << ", " <<  variable_in << std::endl;
      if (numericalTrouble >= 1e-3 && info.update_count > 0)
        rebuild_reason = kRebuildReasonPossiblySingularBasis;
    }

    void adjustPerturbedEquationOut() {
      if (!ekk_instance_.simplexinfo_.bounds_perturbed) return;
      const HighsLp &lp = ekk_instance_.lp_;
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      double lp_lower;
      double lp_upper;
      if (variable_out < num_col) {
        lp_lower = lp.colLower_[variable_out];
        lp_upper = lp.colUpper_[variable_out];
      } else {
        lp_lower = -lp.rowUpper_[variable_out - num_col];
        lp_upper = -lp.rowLower_[variable_out - num_col];
      }
      if (lp_lower < lp_upper) return;
      // Leaving variable is fixed
      //  double save_theta_primal = theta_primal;
      double true_fixed_value = lp_lower;
      // Modify theta_primal so that variable leaves at true fixed value
      theta_primal = (info.baseValue_[row_out] - true_fixed_value) / alpha_col;
      /*
        printf("For equation %4" HIGHSINT_FORMAT " to be nonbasic at RHS %10.4g
        requires theta_primal to change by %10.4g from %10.4g to %10.4g\n",
        variable_out, true_fixed_value, theta_primal-save_theta_primal,
        save_theta_primal, theta_primal);
      */
      info.workLower_[variable_out] = true_fixed_value;
      info.workUpper_[variable_out] = true_fixed_value;
      info.workRange_[variable_out] = 0;
      value_in = info.workValue_[variable_in] + theta_primal;
    }

    void phase1UpdatePrimal() {
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      col_basic_feasibility_change.clear();
      //
      // Update basic primal values, identifying all the feasibility
      // changes giving a value to col_basic_feasibility_change so that the duals
      // can be updated.
      //
      // Determine the base value for cost perturbation
      const double base = info.primal_simplex_phase1_cost_perturbation_multiplier * 5e-7;
      //  if (ekk_instance_.sparseLoopStyle(col_aq.count, num_row, to_entry)) {
      for (HighsInt iEl = 0; iEl < col_aq.count; iEl++) {
        HighsInt iRow = col_aq.index[iEl];
        info.baseValue_[iRow] -= theta_primal * col_aq.array[iRow];
        HighsInt iCol = ekk_instance_.simplexbasis_.basicIndex_[iRow];
        double was_cost = info.workCost_[iCol];
        const double value = info.baseValue_[iRow];
        const double lower = info.baseLower_[iRow];
        const double upper = info.baseUpper_[iRow];
        HighsInt bound_violated = 0;
        if (value < lower - primal_feasibility_tolerance) {
          bound_violated = -1.0;
        } else if (value > upper + primal_feasibility_tolerance) {
          bound_violated = 1.0;
        }
        double cost = bound_violated;
        if (base) cost *= 1 + base * info.numTotRandomValue_[iRow];
        info.workCost_[iCol] = cost;
        if (was_cost) {
          if (!cost) info.num_primal_infeasibility--;
        } else {
          if (cost) info.num_primal_infeasibility++;
        }
        double delta_cost = cost - was_cost;
        if (delta_cost) {
          col_basic_feasibility_change.array[iRow] = delta_cost;
          col_basic_feasibility_change.index[col_basic_feasibility_change.count++] = iRow;
          if (iCol >= num_col) info.workDual_[iCol] += delta_cost;
        }
      }
      // Don't set baseValue[row_out] yet so that dual update due to
      // feasibility changes is done correctly
      ekk_instance_.invalidatePrimalMaxSumInfeasibilityRecord();
    }

    void basicFeasibilityChangeUpdateDual() {
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      // For basic logicals, the change in the basic cost will be a
      // component in col_basic_feasibility_change. This will lead to it being
      // subtracted from workDual in the loop below over the
      // nonzeros in col_basic_feasibility_change, so add it in now. For basic
      // structurals, there will be no corresponding component in
      // row_basic_feasibility_change, since only the nonbasic components are
      // computed (avoided using row pricing, and basic components
      // zeroed after column pricing). Hence there will be no
      // subtraction in the loop below over the nonzeros in
      // row_basic_feasibility_change. Hence, only add in the basic cost change
      // for logicals.
      //
      // Assumes that row_basic_feasibility_change has been set up in
      // phase1UpdatePrimal()

      basicFeasibilityChangeBtran();
      basicFeasibilityChangePrice();
      HighsInt to_entry;
      const bool use_row_indices = ekk_instance_.sparseLoopStyle(row_basic_feasibility_change.count, num_col, to_entry);
      for (HighsInt iEntry = 0; iEntry < to_entry; iEntry++) {
        HighsInt iCol;
        if (use_row_indices) {
          iCol = row_basic_feasibility_change.index[iEntry];
        } else {
          iCol = iEntry;
        }
        info.workDual_[iCol] -= row_basic_feasibility_change.array[iCol];
      }
      const bool use_col_indices = ekk_instance_.sparseLoopStyle(col_basic_feasibility_change.count, num_row, to_entry);
      for (HighsInt iEntry = 0; iEntry < to_entry; iEntry++) {
        HighsInt iRow;
        if (use_col_indices) {
          iRow = col_basic_feasibility_change.index[iEntry];
        } else {
          iRow = iEntry;
        }
        HighsInt iCol = num_col + iRow;
        info.workDual_[iCol] -= col_basic_feasibility_change.array[iRow];
      }
      ekk_instance_.invalidateDualInfeasibilityRecord();
    }

    void basicFeasibilityChangeBtran() {
      // Performs BTRAN on col_basic_feasibility_change. Make sure that
      // col_basic_feasibility_change.count is large (>lp_.numRow_ to be
      // sure) rather than 0 if the indices of the RHS (and true value of
      // col_basic_feasibility_change.count) isn't known.
      const HighsInt solver_num_row = ekk_instance_.lp_.numRow_;
      ekk_instance_.factor_.btran(col_basic_feasibility_change, 0,
                                  0);
      const double local_col_basic_feasibility_change_density =
              (double) col_basic_feasibility_change.count / solver_num_row;
    }

    void basicFeasibilityChangePrice() {
      const HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      const double local_density = 1.0 * col_basic_feasibility_change.count / num_row;
      bool use_col_price;
      bool use_row_price_w_switch;
      ekk_instance_.choosePriceTechnique(info.price_strategy, local_density, use_col_price, use_row_price_w_switch);
      row_basic_feasibility_change.clear();
      if (use_col_price) {
        // Perform column-wise PRICE
        ekk_instance_.matrix_.priceByColumn(row_basic_feasibility_change, col_basic_feasibility_change);
      } else if (use_row_price_w_switch) {
        // Perform hyper-sparse row-wise PRICE, but switch if the density of
        // row_basic_feasibility_change becomes extreme
        const double switch_density = ekk_instance_.matrix_.hyperPRICE;
        ekk_instance_.matrix_.priceByRowSparseResultWithSwitch(row_basic_feasibility_change,
                                                               col_basic_feasibility_change,
                                                               0, 0,
                                                               switch_density);
      } else {
        // Perform hyper-sparse row-wise PRICE
        ekk_instance_.matrix_.priceByRowSparseResult(row_basic_feasibility_change, col_basic_feasibility_change);
      }
      if (use_col_price) {
        // Column-wise PRICE computes components corresponding to basic
        // variables, so zero these by exploiting the fact that, for basic
        // variables, nonbasicFlag[*]=0
        const int8_t *nonbasicFlag = &ekk_instance_.simplexbasis_.nonbasicFlag_[0];
        for (HighsInt iCol = 0; iCol < num_col; iCol++)
          row_basic_feasibility_change.array[iCol] *= nonbasicFlag[iCol];
      }
      // Update the record of average row_basic_feasibility_change density
      const double local_row_basic_feasibility_change_density = (double) row_basic_feasibility_change.count / num_col;
    }

    void phase2UpdatePrimal(const bool initialise = false) {
      static double max_max_local_primal_infeasibility;
      static double max_max_ignored_violation;
      if (initialise) {
        max_max_local_primal_infeasibility = 0;
        max_max_ignored_violation = 0;
        return;
      }
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      bool primal_infeasible = false;
      double max_local_primal_infeasibility = 0;
      double max_ignored_violation = 0;
      // If shifts are only identified in rebuild() the bounds can be
      // ignored. If they aren't ignored, then violations lead to either
      // identification of infeasiblilities (and return to Phase 1) or
      // shifting of bounds to accommodate them.
      const bool ignore_bounds = primal_correction_strategy == kSimplexPrimalCorrectionStrategyInRebuild;
      HighsInt to_entry;
      const bool use_col_indices = ekk_instance_.sparseLoopStyle(col_aq.count, num_row, to_entry);
      for (HighsInt iEntry = 0; iEntry < to_entry; iEntry++) {
        HighsInt iRow;
        if (use_col_indices) {
          iRow = col_aq.index[iEntry];
        } else {
          iRow = iEntry;
        }
        info.baseValue_[iRow] -= theta_primal * col_aq.array[iRow];
        //    if (ignore_bounds) continue;
        // Determine whether a bound is violated and take action
        double lower = info.baseLower_[iRow];
        double upper = info.baseUpper_[iRow];
        double value = info.baseValue_[iRow];
        HighsInt bound_violated = 0;
        if (value < lower - primal_feasibility_tolerance) {
          bound_violated = -1;
        } else if (value > upper + primal_feasibility_tolerance) {
          bound_violated = 1;
        }
        if (!bound_violated) continue;
        // A bound is violated
        if (primal_correction_strategy == kSimplexPrimalCorrectionStrategyNone) {
          // @primal_infeasibility calculation
          double primal_infeasibility;
          if (bound_violated < 0) {
            primal_infeasibility = lower - value;
          } else {
            primal_infeasibility = value - upper;
          }
          max_local_primal_infeasibility = max(primal_infeasibility, max_local_primal_infeasibility);
          if (primal_infeasibility > primal_feasibility_tolerance) {
            info.num_primal_infeasibility++;
            primal_infeasible = true;
          }
        } else if (ignore_bounds) {
          double ignored_violation;
          if (bound_violated < 0) {
            ignored_violation = lower - value;
          } else {
            ignored_violation = value - upper;
          }
          max_ignored_violation = max(ignored_violation, max_ignored_violation);
        } else {
          HighsInt iCol = ekk_instance_.simplexbasis_.basicIndex_[iRow];
          double bound_shift;
          if (bound_violated > 0) {
            // Perturb the upper bound to accommodate the infeasiblilty
            shiftBound(false, iCol, info.baseValue_[iRow], info.numTotRandomValue_[iCol], info.workUpper_[iCol],
                       bound_shift, true);
            info.baseUpper_[iRow] = info.workUpper_[iCol];
            info.workUpperShift_[iCol] += bound_shift;
          } else {
            // Perturb the lower bound to accommodate the infeasiblilty
            shiftBound(true, iCol, info.baseValue_[iRow], info.numTotRandomValue_[iCol], info.workLower_[iCol],
                       bound_shift, true);
            info.baseLower_[iRow] = info.workLower_[iCol];
            info.workLowerShift_[iCol] += bound_shift;
          }
          assert(bound_shift > 0);
        }
      }
      if (primal_infeasible) {
        rebuild_reason = kRebuildReasonPrimalInfeasibleInPrimalSimplex;
        if (max_local_primal_infeasibility > max_max_local_primal_infeasibility * 2) {
          max_max_local_primal_infeasibility = max_local_primal_infeasibility;
          printf("phase2UpdatePrimal: max_local_primal_infeasibility = %g\n", max_local_primal_infeasibility);
        }
        ekk_instance_.invalidatePrimalMaxSumInfeasibilityRecord();
      }
      if (max_ignored_violation > max_max_ignored_violation * 2) {
        max_max_ignored_violation = max_ignored_violation;
        printf("phase2UpdatePrimal: max_ignored_violation = %g\n", max_ignored_violation);
      }
      info.updated_primal_objective_value += info.workDual_[variable_in] * theta_primal;

    }

    void considerInfeasibleValueIn() {
      assert(row_out >= 0);
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      // Determine the base value for cost perturbation
      const double base = info.primal_simplex_phase1_cost_perturbation_multiplier * 5e-7;
      const double lower = info.workLower_[variable_in];
      const double upper = info.workUpper_[variable_in];
      HighsInt bound_violated = 0;
      if (value_in < lower - primal_feasibility_tolerance) {
        bound_violated = -1;
      } else if (value_in > upper + primal_feasibility_tolerance) {
        bound_violated = 1;
      }
      if (!bound_violated) return;
      // The primal value of the entering variable is not feasible
      if (solve_phase == kSolvePhase1) {
        info.num_primal_infeasibility++;
        double cost = bound_violated;
        if (base) cost *= 1 + base * info.numTotRandomValue_[row_out];
        info.workCost_[variable_in] = cost;
        info.workDual_[variable_in] += cost;
      } else if (primal_correction_strategy == kSimplexPrimalCorrectionStrategyNone) {
        // @primal_infeasibility calculation
        double primal_infeasibility;
        if (bound_violated < 0) {
          primal_infeasibility = lower - value_in;
        } else {
          primal_infeasibility = value_in - upper;
        }
        info.num_primal_infeasibility++;
        printf("Entering variable has primal infeasibility of %g for [%g, %g, %g]\n",
               primal_infeasibility, lower, value_in, upper);
        rebuild_reason = kRebuildReasonPrimalInfeasibleInPrimalSimplex;
      } else {
        double bound_shift;
        if (bound_violated > 0) {
          // Perturb the upper bound to accommodate the infeasiblilty
          shiftBound(false, variable_in, value_in, info.numTotRandomValue_[variable_in],
                     info.workUpper_[variable_in], bound_shift, true);
          info.workUpperShift_[variable_in] += bound_shift;
        } else {
          // Perturb the lower bound to accommodate the infeasiblilty
          shiftBound(true, variable_in, value_in, info.numTotRandomValue_[variable_in],
                     info.workLower_[variable_in], bound_shift, true);
          info.workLowerShift_[variable_in] += bound_shift;
        }
        info.bounds_perturbed = true;
      }
      ekk_instance_.invalidatePrimalMaxSumInfeasibilityRecord();
    }

    void updateDual() {
      assert(alpha_col);
      assert(row_out >= 0);
      vector<double> &workDual = ekk_instance_.simplexinfo_.workDual_;
      //  const vector<HighsInt>& nonbasicMove =
      //  ekk_instance_.simplexbasis_.nonbasicMove_;
      // Update the duals
      theta_dual = workDual[variable_in] / alpha_col;
      for (HighsInt iEl = 0; iEl < row_ap.count; iEl++) {
        HighsInt iCol = row_ap.index[iEl];
        workDual[iCol] -= theta_dual * row_ap.array[iCol];
      }
      for (HighsInt iEl = 0; iEl < row_ep.count; iEl++) {
        HighsInt iRow = row_ep.index[iEl];
        HighsInt iCol = iRow + num_col;
        workDual[iCol] -= theta_dual * row_ep.array[iRow];
      }
      // Dual for the pivot
      workDual[variable_in] = 0;
      workDual[variable_out] = -theta_dual;

      ekk_instance_.invalidateDualInfeasibilityRecord();
      // After dual update in primal simplex the dual objective value is not known
      ekk_instance_.status_.has_dual_objective_value = false;
    }

    void removeNonbasicFreeColumn() {
      bool remove_nonbasic_free_column = ekk_instance_.simplexbasis_.nonbasicMove_[variable_in] == 0;
      if (remove_nonbasic_free_column) {
        bool removed_nonbasic_free_column = nonbasic_free_col_set.remove(variable_in);
        if (!removed_nonbasic_free_column) {
          highsLogUser(ekk_instance_.options_.log_options, HighsLogType::kError,
                       "PrimalSimplex::phase1update failed to remove nonbasic free "
                       "column %" HIGHSINT_FORMAT "\n", variable_in);
          assert(removed_nonbasic_free_column);
        }
      }
    }

    void updateDevex() {
      // Compute the pivot weight from the reference set
      double dPivotWeight = 0.0;
      HighsInt to_entry;
      const bool use_col_indices = ekk_instance_.sparseLoopStyle(col_aq.count, num_row, to_entry);
      for (HighsInt iEntry = 0; iEntry < to_entry; iEntry++) {
        HighsInt iRow;
        if (use_col_indices) {
          iRow = col_aq.index[iEntry];
        } else {
          iRow = iEntry;
        }
        HighsInt iCol = ekk_instance_.simplexbasis_.basicIndex_[iRow];
        double dAlpha = devex_index[iCol] * col_aq.array[iRow];
        dPivotWeight += dAlpha * dAlpha;
      }
      dPivotWeight += devex_index[variable_in] * 1.0;
      dPivotWeight = sqrt(dPivotWeight);

      // Check if the saved weight is too large
      if (devex_weight[variable_in] > bad_devex_weight_factor * dPivotWeight)
        num_bad_devex_weight++;

      // Update the devex weight for all
      double dPivot = col_aq.array[row_out];
      dPivotWeight /= fabs(dPivot);

      for (HighsInt iEl = 0; iEl < row_ap.count; iEl++) {
        HighsInt iCol = row_ap.index[iEl];
        double alpha = row_ap.array[iCol];
        double devex = dPivotWeight * fabs(alpha);
        devex += devex_index[iCol] * 1.0;
        if (devex_weight[iCol] < devex) {
          devex_weight[iCol] = devex;
        }
      }
      for (HighsInt iEl = 0; iEl < row_ep.count; iEl++) {
        HighsInt iRow = row_ep.index[iEl];
        HighsInt iCol = iRow + num_col;
        double alpha = row_ep.array[iRow];
        double devex = dPivotWeight * fabs(alpha);
        devex += devex_index[iCol] * 1.0;
        if (devex_weight[iCol] < devex) {
          devex_weight[iCol] = devex;
        }
      }

      // Update devex weight for the pivots
      devex_weight[variable_out] = max(1.0, dPivotWeight);
      devex_weight[variable_in] = 1.0;
      num_devex_iterations++;
    }

    void resetDevex() {
      devex_weight.assign(num_tot, 1.0);
      devex_index.assign(num_tot, 0);
      for (HighsInt iCol = 0; iCol < num_tot; iCol++) {
        const HighsInt nonbasicFlag = ekk_instance_.simplexbasis_.nonbasicFlag_[iCol];
        devex_index[iCol] = nonbasicFlag * nonbasicFlag;
      }
      num_devex_iterations = 0;
      num_bad_devex_weight = 0;
    }

    void getNonbasicFreeColumnSet() {
      if (!num_free_col) return;
      assert(num_free_col > 0);
      const HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      const SimplexBasis &basis = ekk_instance_.simplexbasis_;
      nonbasic_free_col_set.clear();
      for (HighsInt iCol = 0; iCol < num_tot; iCol++) {
        bool nonbasic_free = basis.nonbasicFlag_[iCol] == kNonbasicFlagTrue &&
                             info.workLower_[iCol] <= -kHighsInf &&
                             info.workUpper_[iCol] >= kHighsInf;
        if (nonbasic_free) nonbasic_free_col_set.add(iCol);
      }
    }

    void cleanup() {
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      if (!info.bounds_perturbed) return;
      highsLogDev(ekk_instance_.options_.log_options, HighsLogType::kDetailed,
                  "primal-cleanup-shift\n");
      // Remove perturbation and don't permit further perturbation
      ekk_instance_.initialiseBound(SimplexAlgorithm::kPrimal, solve_phase, false);
      ekk_instance_.initialiseNonbasicValueAndMove();
      info.allow_bound_perturbation = false;
      // Compute the primal values
      ekk_instance_.computePrimal();
      // Possibly analyse the change in duals
      /*  debugCleanup(ekk_instance_, original_baseValue); */
      // Compute the primal infeasibilities
      ekk_instance_.computeSimplexPrimalInfeasible();

      // Compute the primal objective value
      ekk_instance_.computePrimalObjectiveValue();
      // Now that there's a new primal_objective_value, reset the updated
      // value
      info.updated_primal_objective_value = info.primal_objective_value;

      //  if (!info.run_quiet) {
      // Report the dual infeasiblities
      ekk_instance_.computeSimplexDualInfeasible();
      // In phase 1, report the simplex LP dual infeasiblities
      // In phase 2, report the simplex dual infeasiblities (known)
      //    if (solve_phase == kSolvePhase1)
      //    computeSimplexLpDualInfeasible(ekk_instance_);
      std::cout << "Iteration: " << info.primal_phase1_iteration_count << ", " << info.primal_phase2_iteration_count << std::endl;
    }
    void reportRebuild(const HighsInt reason_for_rebuild) {
      iterationAnalysisData();
      analysis->rebuild_reason = reason_for_rebuild;
      analysis->invertReport();
    }
    void iterationAnalysisData() {
      HighsSimplexInfo &info = ekk_instance_.simplexinfo_;
      analysis->simplex_strategy = kSimplexStrategyPrimal;
      analysis->edge_weight_mode = DualEdgeWeightMode::kDevex;
      analysis->solve_phase = solve_phase;
      analysis->simplex_iteration_count = ekk_instance_.iteration_count_;
      analysis->devex_iteration_count = num_devex_iterations;
      analysis->pivotal_row_index = row_out;
      analysis->leaving_variable = variable_out;
      analysis->entering_variable = variable_in;
      analysis->rebuild_reason = rebuild_reason;
      analysis->reduced_rhs_value = 0;
      analysis->reduced_cost_value = 0;
      analysis->edge_weight = 0;
      analysis->primal_delta = 0;
      analysis->primal_step = theta_primal;
      analysis->dual_step = theta_dual;
      analysis->pivot_value_from_column = alpha_col;
      analysis->pivot_value_from_row = alpha_row;
      analysis->numerical_trouble = numericalTrouble;
      analysis->objective_value = info.updated_primal_objective_value;
      analysis->num_primal_infeasibility = info.num_primal_infeasibility;
      analysis->num_dual_infeasibility = info.num_dual_infeasibility;
      analysis->sum_primal_infeasibility = info.sum_primal_infeasibility;
      analysis->sum_dual_infeasibility = info.sum_dual_infeasibility;
      if ((analysis->edge_weight_mode == DualEdgeWeightMode::kDevex) && (num_devex_iterations == 0))
        analysis->num_devex_framework++;
    }
};

#endif //HIGHS_KERNEL_TYPES_H

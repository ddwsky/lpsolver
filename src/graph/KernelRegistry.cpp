//
// Created by root on 8/8/21.
//

#include "KernelRegistry.h"

KernelRegistry *KernelRegistry::GetInstance() {
  static KernelRegistry instance;
  return &instance;
}

KernelFunc KernelRegistry::GetKernel(int kernelId) {
  if (kernelId >= kernelList_.size()) {
    return nullptr;
  }
  auto it = kernelList_[kernelId];
  if (it != nullptr) {
    return it;
  }
  return nullptr;
}

KernelFunc KernelRegistry::GetKernel(KernelArch arch, DataType dtype, OpType opid) {
  KernelKey desc{arch, dtype, opid};
  return GetKernel(desc);
}

KernelFunc KernelRegistry::GetKernel(const KernelKey &desc) {
  int index = GetFuncIndex(desc);
  return GetKernel(index);
}

int KernelRegistry::GetFuncIndex(const KernelKey &desc) {
  int index;
  int deviceId = static_cast<int>(desc.arch);
  int dataTypeId = static_cast<int>(desc.dtype);
  int opId = static_cast<int>(desc.opid);
  index = deviceId * DATA_TYPE_MAX * OP_TYPE_MAX + dataTypeId * OP_TYPE_MAX + opId;
  return index;
}

void KernelRegistry::RegKernel(const KernelKey desc, const KernelFunc func) {
  int index = GetFuncIndex(desc);
  if (index >= kernelList_.size()) {
    return;
  }
  kernelList_[index] = func;
}

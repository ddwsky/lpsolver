/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                       */
/*    This file is part of the HiGHS linear optimization suite           */
/*                                                                       */
/*    Written and engineered 2008-2021 at the University of Edinburgh    */
/*                                                                       */
/*    Available as open-source under the MIT License                     */
/*                                                                       */
/*    Authors: Julian Hall, Ivet Galabova, Qi Huangfu, Leona Gottwald    */
/*    and Michael Feldmeier                                              */
/*                                                                       */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**@file ../app/RunHighs.cpp
 * @brief HiGHS main
 */
#include "Primal.h"
#include "lp_data/HighsRuntimeOptions.h"
#include "../../graph/kernel_types.h"
#include "../../graph/StaticGraph.h"

void ParserOptions(LPOptions options){}

int main(int argc, char** argv) {
  std::string model_file(argv[1]);

  LPOptions options;
  ParserOptions(options);

  StaticGraph graph(options, model_file);

  // Load the model from model_file
  graph.Initialise();

  // Solve the model
  graph.Solve();

  return 0;
}
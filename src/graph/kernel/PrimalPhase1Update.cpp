//
// Created by root on 8/9/21.
//

#include "PrimalPhase1Update.h"
#include "../KernelRegistry.h"

int PrimalPhase1Update::Run() {
  // Perform update operations that are independent of phase
  HighsSimplexInfo &info = dgInfo_.ekk_instance_.simplexinfo_;
  assert(!dgInfo_.rebuild_reason);
  bool flipped = dgInfo_.row_out < 0;
  if (flipped) {
    dgInfo_.variable_out = dgInfo_.variable_in;
    dgInfo_.alpha_col = 0;
    dgInfo_.numericalTrouble = 0;
    info.workValue_[dgInfo_.variable_in] = dgInfo_.value_in;
    assert(dgInfo_.ekk_instance_.simplexbasis_.nonbasicMove_[dgInfo_.variable_in] = dgInfo_.move_in);
    dgInfo_.ekk_instance_.simplexbasis_.nonbasicMove_[dgInfo_.variable_in] = -dgInfo_.move_in;
  } else {
    // Adjust perturbation if leaving equation
    dgInfo_.adjustPerturbedEquationOut();
  }

  // Update primal values
  dgInfo_.phase1UpdatePrimal();

  // Update the duals with respect to feasibility changes
  dgInfo_.basicFeasibilityChangeUpdateDual();


  assert(dgInfo_.rebuild_reason == kRebuildReasonNo ||
         dgInfo_.rebuild_reason == kRebuildReasonPrimalInfeasibleInPrimalSimplex);

  if (flipped) {
    info.primal_bound_swap++;
    dgInfo_.ekk_instance_.invalidateDualInfeasibilityRecord();
    dgInfo_.num_flip_since_rebuild++;
    // Update the synthetic clock for UPDATE
    dgInfo_.ekk_instance_.total_synthetic_tick_ += dgInfo_.col_aq.synthetic_tick;
    return OP_PRIMAL_PHASE1_PRICING;
  }

//  std::cout << "update : solve_phase: " << dgInfo_.solve_phase << ", primal objective: " << dgInfo_.ekk_instance_.simplexinfo_.primal_objective_value
//            << "," << info.updated_primal_objective_value << "," << info.update_count << "," << info.num_primal_infeasibility
//            << "," << info.dual_objective_value << "," << info.num_dual_infeasibility << "," << info.updated_primal_objective_value
//          << "," << dgInfo_.value_in << "," << dgInfo_.row_out << "," << dgInfo_.theta_primal << "," << dgInfo_.alpha_col
//          << "," << dgInfo_.alpha_row << "," << dgInfo_.move_in << "," << dgInfo_.move_out << "," << dgInfo_.theta_dual
//          << "," << dgInfo_.variable_in << "," << dgInfo_.variable_out << std::endl;
  assert(dgInfo_.row_out >= 0);
  // Now set the value of the entering variable
  info.baseValue_[dgInfo_.row_out] = dgInfo_.value_in;
  // Consider whether the entering value is feasible and, if not, take
  // action
  //
  // rebuild_reason =
  // kRebuildReasonPrimalInfeasibleInPrimalSimplex is set in
  // phase 2 if a primal infeasiblility is found
  dgInfo_.considerInfeasibleValueIn();

  // Update the dual values
  dgInfo_.theta_dual = info.workDual_[dgInfo_.variable_in];
  dgInfo_.updateDual();

  // Update the devex weight
  dgInfo_.updateDevex();

  // If entering column was nonbasic free, remove it from the set
  dgInfo_.removeNonbasicFreeColumn();

  // Perform pivoting
  dgInfo_.ekk_instance_.updatePivots(dgInfo_.variable_in, dgInfo_.row_out, dgInfo_.move_out);
  dgInfo_.ekk_instance_.updateFactor(&dgInfo_.col_aq, &dgInfo_.row_ep, &dgInfo_.row_out, &dgInfo_.rebuild_reason);
  dgInfo_.ekk_instance_.updateMatrix(dgInfo_.variable_in, dgInfo_.variable_out);
  if (info.update_count >= info.update_limit)
    dgInfo_.rebuild_reason = kRebuildReasonUpdateLimitReached;

  // Update the iteration count
  dgInfo_.ekk_instance_.iteration_count_++;

  // Reset the devex when there are too many errors
  if (dgInfo_.num_bad_devex_weight > dgInfo_.allowed_num_bad_devex_weight) dgInfo_.resetDevex();

  // Update the synthetic clock for UPDATE
  dgInfo_.ekk_instance_.total_synthetic_tick_ += dgInfo_.col_aq.synthetic_tick;
  dgInfo_.ekk_instance_.total_synthetic_tick_ += dgInfo_.row_ep.synthetic_tick;

  return OP_PRIMAL_PHASE1_POSTPROCESS;
}

REG_KERNEL(ARCH_CPU, DATA_TYPE_FLOAT32, OP_PRIMAL_PHASE1_UPDATE, KernelCreator<PrimalPhase1Update>)

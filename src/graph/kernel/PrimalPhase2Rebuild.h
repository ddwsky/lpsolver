//
// Created by root on 8/9/21.
//

#ifndef HIGHS_PRIMAL_PHASE2_REBUILD_H
#define HIGHS_PRIMAL_PHASE2_REBUILD_H

#include "../KernelBase.h"

class PrimalPhase2Rebuild : public KernelBase {
public:
    PrimalPhase2Rebuild(DGInfo &info, int id, int rval) : KernelBase(info, id, rval) {};

    ~PrimalPhase2Rebuild() = default;

    int Run() override;
};


#endif //HIGHS_PRIMAL_PHASE2_REBUILD_H
